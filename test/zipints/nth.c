// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#include "zipints/zipints.h"
#include "genutil/rng.h"


// Set the default test array length.
#ifndef TEST_LEN
  #define TEST_LEN 1000
#endif

// Set the default test repetition count.
#ifndef TEST_REP
  #define TEST_REP 100000
#endif

/// Compare two integers for the purposes of the `qsort` function.
static int cmp_ints(
  const void* fst, // First value.
  const void* snd) // Second value.
{
  ZIPINTS_INT val[2]; // Integer values.
  
  val[0] = *(const ZIPINTS_INT*)fst;
  val[1] = *(const ZIPINTS_INT*)snd;

  return (val[0] > val[1]) - (val[0] < val[1]);
}

/// Execute a single test.
static bool run_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  ZIPINTS_INT        *const restrict arr) // Values memory.
{
  struct zipints zip; // Compressed integers.
  ZIPINTS_INT    idx; // Array index.
  ZIPINTS_INT    val; // Value to search for.
  ZIPINTS_INT    len; // Test length.
  ZIPINTS_INT    exp; // Expected index.
  ZIPINTS_INT    act; // Actual index.
  bool           ret; // Return value.

  // Randomly reduce the input array.
  len = genutil_rng_int(rng, 1, TEST_LEN);

  // Fill the array with random integers. We do not handle the edge case where there are multiple
  // identical integers in the array.
  idx = 0;
  while (idx < len) {
    arr[idx] = genutil_rng_any(rng);
    idx     += 1;
  }
  qsort(arr, len, sizeof(ZIPINTS_INT), cmp_ints);

  // Encode the array.
  ret = zipints_put(&zip, arr, len);
  if (ret == false) {
    (void)printf("unable to encode the integers\n");
    return false;
  }

  // Pick a random index and the value within the array on that index.
  idx = genutil_rng_int(rng, 0, len - 1);
  exp = arr[idx];

  // Retrieve the element at the index.
  ret = zipints_nth(&zip, idx, &act); 
  if (ret == false) {
    (void)printf("index retrieval failed\n");
    return false;
  }
  
  // Verify that the values at indices match. This is to address the case
  if (act != exp) {
    (void)printf("mismatch of values exp=%" PRIuMAX " act=%" PRIuMAX "\n",
      (uintmax_t)exp, (uintmax_t)act);
    return false;
  }

  // Free up the compressed resources.
  zipints_del(&zip);
  
  return true;
}

/// Repeatedly execute the test of random input and verify that the `put` and `get` operations form
/// an identity under composition.
int main(void)
{
  struct genutil_rng rng; // Random number generator.
  ZIPINTS_INT*       arr; // Array of values.
  uintmax_t          rep; // Repetition counter.
  bool               res; // Test result.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Allocate memory used by the test.
  arr = calloc(sizeof(ZIPINTS_INT), TEST_LEN);
  if (arr == NULL) {
    (void)printf("unable to allocate memory\n");
    return EXIT_FAILURE;
  }

  // Repeatedly execute the test.
  rep = 0;
  while (rep < TEST_REP) {
    res = run_test(&rng, arr);
    if (res == false) {
      break;
    }
    rep += 1;
  }

  // Determine the exit code based on test results.
  if (res == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
