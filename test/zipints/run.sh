#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Compilation settings.
cc="gcc"
std="-std=c99"
flag="-Wall -Wextra -Werror -D_XOPEN_SOURCE -DTEST_SEED=7181"
inc="-I../ -I../../src"
lib="../../src/bittape/bittape.c ../../src/bitutil/bitutil.c ../../src/zipints/zipints.c ../../src/genutil/rng.c"

declare -A test_lens 
test_lens["-DTEST_LEN=10"]="1"
test_lens["-DTEST_LEN=100"]="2"
test_lens["-DTEST_LEN=1000"]="3"
test_lens["-DTEST_LEN=10000"]="4"
test_lens["-DTEST_LEN=100000"]="5"
test_lens["-DTEST_LEN=1000000"]="6"

declare -A test_opts
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

declare -A test_ints
test_ints["-DZIPINTS_INT_BIT=16 -DBITTAPE_INT_BIT=16 -DBITUTIL_INT_BIT=16 -DGENUTIL_INT_BIT=16 -DGENUTIL_FLT_EXC"]="16"
test_ints["-DZIPINTS_INT_BIT=32 -DBITTAPE_INT_BIT=32 -DBITUTIL_INT_BIT=32 -DGENUTIL_INT_BIT=32 -DGENUTIL_FLT_EXC"]="32"
test_ints["-DZIPINTS_INT_BIT=64 -DBITTAPE_INT_BIT=64 -DBITUTIL_INT_BIT=64 -DGENUTIL_INT_BIT=64 -DGENUTIL_FLT_EXC"]="64"

# Hardware acceleration settings.
declare -A test_accs
test_accs[" "]="0"
test_accs["-DGENUTIL_ACC"]="1"

# Ensure early termination and transparency.
set -e
set -x

# Clear the existing compilation and execution plans.
> make_plan
> test_plan

# Create the compilation and execution plans.
for test_opt in "${!test_opts[@]}"; do
  for test_acc in "${!test_accs[@]}"; do
    for test_len in "${!test_lens[@]}"; do
      for test_int in "${!test_ints[@]}"; do
        # Skip cases where the integer width is not sufficient.
        if [ ${test_ints[${test_int}]} -eq "16" ] && [ ${test_lens[${test_len}]} -ge "5" ]; then
          continue
        fi

        # Extract the identifiers.
        get_path="./bin/get_l${test_lens[$test_len]}_o${test_opts[$test_opt]}_a${test_accs[$test_acc]}_i${test_ints[${test_int}]}"
        idx_path="./bin/idx_l${test_lens[$test_len]}_o${test_opts[$test_opt]}_a${test_accs[$test_acc]}_i${test_ints[${test_int}]}"
        nth_path="./bin/nth_l${test_lens[$test_len]}_o${test_opts[$test_opt]}_a${test_accs[$test_acc]}_i${test_ints[${test_int}]}"
        dbg_path="./bin/dbg_o${test_opts[$test_opt]}_a${test_accs[$test_acc]}_i${test_ints[${test_int}]}"
      
        # Note the compilation and execution plans.
        echo ${cc} ${test_len} ${test_int} ${std} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} get.c -o ${get_path} >> ./make_plan
        echo ${cc} ${test_len} ${test_int} ${std} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} idx.c -o ${idx_path} >> ./make_plan
        echo ${cc} ${test_len} ${test_int} ${std} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} idx.c -o ${nth_path} >> ./make_plan
        echo ${cc}             ${test_int} ${std} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} dbg.c -o ${dbg_path} >> ./make_plan
        echo ${get_path} >> ./test_plan
        echo ${idx_path} >> ./test_plan
        echo ${nth_path} >> ./test_plan
      done
    done
  done
done

# Execute the compilation and execution of identity property tests in parallel.
parallel --tmpdir ./tmp/ -v         < ./make_plan
parallel --tmpdir ./tmp/ --joblog - < ./test_plan
