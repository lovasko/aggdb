// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#include "zipints/zipints.h"
#include "genutil/rng.h"


// Set the default test array length.
#ifndef TEST_LEN
  #define TEST_LEN 1000
#endif

// Set the default test repetition count.
#ifndef TEST_REP
  #define TEST_REP 100000
#endif

/// Compare two integers for the purposes of the `qsort` function.
static int cmp_ints(
  const void* fst, // First value.
  const void* snd) // Second value.
{
  ZIPINTS_INT val[2]; // Integer values.
  
  val[0] = *(const ZIPINTS_INT*)fst;
  val[1] = *(const ZIPINTS_INT*)snd;

  return (val[0] > val[1]) - (val[0] < val[1]);
}

/// Execute a single test.
static bool run_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  ZIPINTS_INT        *const restrict exp, // Expected values memory.
  ZIPINTS_INT        *const restrict act) // Actual values memory.
{
  struct zipints zip;    // Compressed integers.
  ZIPINTS_INT    idx;    // Array index.
  ZIPINTS_INT    len[2]; // Expected and actual lengths.
  bool           ret;    // Return value.

  // Randomly reduce the input array.
  len[0] = genutil_rng_int(rng, 1, TEST_LEN);

  // Fill the array with random integers. We ensure that the integers are unique.
  idx = 0;
  while (idx < len[0]) {
    exp[idx] = genutil_rng_any(rng);
    idx     += 1;
  }
  qsort(exp, len[0], sizeof(ZIPINTS_INT), cmp_ints);

  // Encode the array.
  ret = zipints_put(&zip, exp, len[0]);
  if (ret == false) {
    (void)printf("unable to encode the integers\n");
    return false;
  }

  // Decode the array.
  ret = zipints_get(&zip, act, &len[1]);
  if (ret == false) {
    (void)printf("unable to decode the integers\n");
    return false;
  }
  
  // Verify that the array lengths match.
  if (len[0] != len[1]) {
    (void)printf("mismatch of lengths exp=%" ZIPINTS_INT_FMT " act=%" ZIPINTS_INT_FMT "\n",
      len[0], len[1]);
    return false;
  }

  // Verify that the integers match.
  idx = 0;
  while (idx < len[0]) {
    if (exp[idx] != act[idx]) {
      (void)printf("mismatch of values exp=%" ZIPINTS_INT_FMT " act=%" ZIPINTS_INT_FMT "\n",
        exp[idx], act[idx]);
      return false;
    }
    idx += 1;
  }

  // Free up the compressed resources.
  zipints_del(&zip);
  
  return true;
}

/// Repeatedly execute the test of random input and verify that the `put` and `get` operations form
/// an identity under composition.
int main(void)
{
  struct genutil_rng rng; // Random number generator.
  ZIPINTS_INT*       exp; // Expected values.
  ZIPINTS_INT*       act; // Actual values.
  uintmax_t          rep; // Repetition counter.
  bool               res; // Test result.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Allocate memory used by the test.
  exp = calloc(sizeof(ZIPINTS_INT), TEST_LEN);
  if (exp == NULL) {
    (void)printf("unable to allocate memory\n");
    return EXIT_FAILURE;
  }

  act = calloc(sizeof(ZIPINTS_INT), TEST_LEN);
  if (act == NULL) {
    (void)printf("unable to allocate memory\n");
    return EXIT_FAILURE;
  }

  // Repeatedly execute the test.
  rep = 0;
  while (rep < TEST_REP) {
    res = run_test(&rng, exp, act);
    if (res == false) {
      break;
    }
    rep += 1;
  }

  // Determine the exit code based on test results.
  if (res == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
