#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "zipints/zipints.h"


/// Print a single bit from a bit tape to the standard output stream.
static bool show_bit(
  BITTAPE_INT     idx, // Index of the bit (unused).
  BITTAPE_INT     val, // Bit value.
  void*           ctx) // Context object (unused).
{
  // Explicitly ignore all but one of the arguments.
  (void)idx;
  (void)ctx;

  // Print the single bit.
  (void)printf("%c", val == 0 ? '0' : '1');

  // Do not terminate the traversal early.
  return true;
}

/// Encode and decode integers from the process argument vector along with detailed view of the
/// underlying bit tapes.
int main(
  int   argc,   // Argument count.
  char* argv[]) // Argument vector.
{
  struct zipints zip; // Compressed integers.
  ZIPINTS_INT*   inp; // Input array.
  ZIPINTS_INT*   out; // Output array.
  ZIPINTS_INT    len; // Length of the output array.
  ZIPINTS_INT    val; // Retrieved value.
  intmax_t       idx; // Argument index.
  bool           ret; // Return value.

  // Ensure that at least one integer was provided.
  if (argc < 2) {
    (void)printf("expected at least a single argument\n");
    return EXIT_FAILURE;
  }

  // Allocate required memory for the input and output arrays.
  inp = calloc(sizeof(ZIPINTS_INT), argc - 1);
  if (inp == false) {
    (void)printf("unable to allocate memory for input array\n");
    return EXIT_FAILURE;
  }

  out = calloc(sizeof(ZIPINTS_INT), argc -1);
  if (out == false) {
    (void)printf("unable to allocate memory for output array\n");
    return EXIT_FAILURE;
  }

  // Parse the integers from the argument vector.
  idx = 1;
  while (idx < (intmax_t)argc) {
    sscanf(argv[idx], "%" ZIPINTS_INT_SCN, &inp[idx - 1]);
    idx += 1;
  }

  // Encode the integers.
  ret = zipints_put(&zip, inp, (ZIPINTS_INT)argc - 1);
  if (ret == false) {
    (void)printf("unable to encode values\n");
    return EXIT_FAILURE;
  }

  // Print the underlying bit tapes for debugging.
  printf("low bits = ");
  bittape_map(&zip.zi_lbt, show_bit, NULL);
  printf("\n");

  printf("high bits = ");
  bittape_map(&zip.zi_hbt, show_bit, NULL);
  printf("\n");

  // Decode the integers.
  ret = zipints_get(&zip, out, &len);
  if (ret == false) {
    (void)printf("unable to decode values\n");
    return EXIT_FAILURE;
  }

  // Print the output array.
  (void)printf("out = ");
  idx = 0;
  while (idx < (intmax_t)len) {
    (void)printf("%" ZIPINTS_INT_FMT " ", out[idx]);
    idx += 1;
  }
  (void)printf("\n");

  // One by one select values at consecutive indices.
  idx = 0;
  while (idx < (argc - 1)) {
    ret = zipints_idx(&zip, inp[idx], &val);
    if (ret == false) {
      printf("failed retrival of value %" PRIuMAX "\n", (uintmax_t)inp[idx]);
    } else {
      printf("value %" PRIuMAX " is at index %" PRIuMAX "\n", (uintmax_t)inp[idx], (uintmax_t)val);
    }
    idx += 1;
  }
  
  return EXIT_SUCCESS;
}
