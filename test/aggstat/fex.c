// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <fenv.h>

#include "fex.h"


/// Initialise the floating-point environment exception tracker.
void aggtest_fex_new(
  struct aggtest_fex *const fex) // Floating-point exceptions.
{
  fex->af_ofe = 0;
  fex->af_ufe = 0;
  fex->af_iee = 0;
  fex->af_cnt = 0;
}

/// Obtain the current floating-point environment exceptions.
void aggtest_fex_put(
  struct aggtest_fex *const fex) // Floating-point exceptions.
{
  int set; // Set of exceptions to track.
  int exc; // Exceptions that took place.

  // Obtain the exceptions and clear them for further examination.
  set = FE_OVERFLOW | FE_UNDERFLOW | FE_INEXACT;
  exc = fetestexcept(set);
  feclearexcept(set);

  // Increment the appropriate exception counters.
  if (exc & FE_OVERFLOW) {
    fex->af_ofe += 1;
  }
  if (exc & FE_UNDERFLOW) {
    fex->af_ufe += 1;
  }
  if (exc & FE_INEXACT) {
    fex->af_iee += 1;
  }

  // Note the execution.
  fex->af_cnt += 1;
}

/// Retrieve the exception counts.
bool aggtest_fex_get(
  struct aggtest_fex *const restrict fex, // Floating-point exceptions.
  uintmax_t          *const restrict ofe, // Overflow exception count.
  uintmax_t          *const restrict ufe, // Underflow exception count.
  uintmax_t          *const restrict iee) // Inexact exception count.
{
  // Ensure that at least one measurement was made.
  if (fex->af_cnt == 0) {
    return false;
  }

  // Retrieve the exception counts.
  *ofe = fex->af_ofe;
  *ufe = fex->af_ufe;
  *iee = fex->af_iee;
  return true;
}
