// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include "aggstat/aggstat.h"


// Online algorithms.
bool aggstat_fst_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_lst_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_cnt_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_sum_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_min_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_max_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_avg_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_var_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_skw_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_krt_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_med_1_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_med_10_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_med_100_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_qnt_0_5_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool aggstat_qnt_0_9_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);

// Offline algorithms.
bool offline_fst_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_lst_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_cnt_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_sum_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_min_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_max_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_avg_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_var_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_skw_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_krt_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_qnt_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, const AGGSTAT_FLT pct, AGGSTAT_FLT* out);
bool offline_qnt_0_5_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
bool offline_qnt_0_9_run(AGGSTAT_FLT* inp, const AGGSTAT_INT len, AGGSTAT_FLT* out);
