// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include "run.h"


#ifndef TEST_CUT
  #error "must define TEST_CUT to select number of cut bits"
#endif

#if   AGGSTAT_FLT_BIT == 32
  #include <gsl/gsl_statistics_float.h>
  #include <gsl/gsl_sort_float.h> 
#elif AGGSTAT_FLT_BIT == 64
  #include <gsl/gsl_statistics_double.h>
  #include <gsl/gsl_sort_double.h>
#elif AGGSTAT_FLT_BIT == 80
  #include <gsl/gsl_statistics_long_double.h>
  #include <gsl/gsl_sort_long_double.h>
#elif AGGSTAT_FLT_BIT == 128 
  #include <gsl/gsl_statistics_long_double.h>
  #include <gsl/gsl_sort_long_double.h>
#else
  #error "invalid value for " AGGSTAT_FLT_BIT
#endif

// Add missing floating-point constants.
#define AGGSTAT_FLT_10_0  AGGSTAT_FLT_NUM(10,  0, +, 0)
#define AGGSTAT_FLT_100_0 AGGSTAT_FLT_NUM(100, 0, +, 0)

/// Compute the `first` of an array using an aggregate function.
bool aggstat_fst_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_fst agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_fst_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_fst_put(&agg, inp[idx]);
    aggstat_fst_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_fst_get(&agg, out);
}

/// Compute the `last` of an array using an aggregate function.
bool aggstat_lst_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_lst agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_lst_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_lst_put(&agg, inp[idx]);
    aggstat_lst_cut(&agg, TEST_CUT);
    idx += 1; 
  }

  // Retrieve the aggregate value.
  return aggstat_lst_get(&agg, out);
}

/// Compute the `count` of an array using an aggregate function.
bool aggstat_cnt_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_cnt agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_cnt_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_cnt_put(&agg, inp[idx]);
    aggstat_cnt_cut(&agg, TEST_CUT);
    idx += 1; 
  }

  // Retrieve the aggregate value.
  return aggstat_cnt_get(&agg, out);
}

/// Compute the `sum` of an array using an aggregate function.
bool aggstat_sum_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_sum agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_sum_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_sum_put(&agg, inp[idx]);
    aggstat_sum_cut(&agg, TEST_CUT);
    idx += 1; 
  }

  // Retrieve the aggregate value.
  return aggstat_sum_get(&agg, out);
}

/// Compute the `minimum` of an array using an aggregate function.
bool aggstat_min_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_min agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_min_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_min_put(&agg, inp[idx]);
    aggstat_min_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_min_get(&agg, out);
}

/// Compute the `maximum` of an array using an aggregate function.
bool aggstat_max_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_max agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.
  
  // Initialise the aggregate function.
  aggstat_max_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_max_put(&agg, inp[idx]);
    aggstat_max_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_max_get(&agg, out);
}

/// Compute the `average` of an array using an aggregate function.
bool aggstat_avg_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_avg agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_avg_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_avg_put(&agg, inp[idx]);
    aggstat_avg_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_avg_get(&agg, out);
}

/// Compute the `variance` of an array using an aggregate function.
bool aggstat_var_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_var agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_var_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_var_put(&agg, inp[idx]);
    aggstat_var_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_var_get(&agg, out);
}

/// Compute the `skewness` of an array using an aggregate function.
bool aggstat_skw_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_skw agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_skw_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_skw_put(&agg, inp[idx]);
    aggstat_skw_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_skw_get(&agg, out);
}

/// Compute the `kurtosis` of an array using an aggregate function.
bool aggstat_krt_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_krt agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_krt_new(&agg);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_krt_put(&agg, inp[idx]);
    aggstat_krt_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_krt_get(&agg, out);
}

/// Compute the `median with step of 1.0` of an array using an aggregate function.
bool aggstat_med_1_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_med agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_med_new(&agg, AGGSTAT_FLT_1_0);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_med_put(&agg, inp[idx]);
    aggstat_med_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_med_get(&agg, out);
}

/// Compute the `median with step of 10.0` of an array using an aggregate function.
bool aggstat_med_10_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_med agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_med_new(&agg, AGGSTAT_FLT_10_0);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_med_put(&agg, inp[idx]);
    aggstat_med_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_med_get(&agg, out);
}

/// Compute the `median with step of 100.0` of an array using an aggregate function.
bool aggstat_med_100_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_med agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_med_new(&agg, AGGSTAT_FLT_100_0);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_med_put(&agg, inp[idx]);
    aggstat_med_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_med_get(&agg, out);
}

/// Compute the `0.5-quantile` of an array using an aggregate function.
bool aggstat_qnt_0_5_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_qnt agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_qnt_new(&agg, AGGSTAT_FLT_0_5);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_qnt_put(&agg, inp[idx]);
    aggstat_qnt_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_qnt_get(&agg, out);
}

/// Compute the `0.9-quantile` of an array using an aggregate function.
bool aggstat_qnt_0_9_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  struct aggstat_qnt agg; // Aggregate function.
  AGGSTAT_INT        idx; // Array index.

  // Initialise the aggregate function.
  aggstat_qnt_new(&agg, AGGSTAT_FLT_0_9);

  // Push all values to the aggregate function.
  idx = 0;
  while (idx < len) {
    aggstat_qnt_put(&agg, inp[idx]);
    aggstat_qnt_cut(&agg, TEST_CUT);
    idx += 1;
  }

  // Retrieve the aggregate value.
  return aggstat_qnt_get(&agg, out);
}

/// Select the first element from an array (offline algorithm).
bool offline_fst_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  *out = inp[0];
  return true;
}

/// Select the last element from an array (offline algorithm).
bool offline_lst_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  *out = inp[len - 1];
  return true;
}

/// Compute the length of an array (offline algorithm).
bool offline_cnt_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  (void)inp;

  *out = (AGGSTAT_FLT)len;
  return true;
}

/// Compute the sum of an array (offline algorithm).
bool offline_sum_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  AGGSTAT_INT idx; // Array index.
  AGGSTAT_FLT sum; // Output value.

  sum = AGGSTAT_FLT_0_0;
  idx = 0;
  while (idx < len) {
    sum += inp[idx];
    idx += 1;
  }

  *out = sum;
  return true;
}

/// Compute the minimum of an array (offline algorithm).
bool offline_min_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    *out = (AGGSTAT_FLT)gsl_stats_float_min(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    *out = (AGGSTAT_FLT)gsl_stats_min(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    *out = (AGGSTAT_FLT)gsl_stats_long_double_min(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT min; // Output value.
    AGGSTAT_INT idx; // Array index.

    // Early exit.
    if (len == 0) {
      return false;
    }

    // Traverse the array.
    min = inp[0];
    idx = 1;
    while (idx < len) {
      // Continuously select lesser values.
      if (inp[idx] < min) {
        min = inp[idx];
      }

      idx += 1;
    }
    *out = min;
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the maximum of an array (offline algorithm).
bool offline_max_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    *out = (AGGSTAT_FLT)gsl_stats_float_max(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    *out = (AGGSTAT_FLT)gsl_stats_max(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    *out = (AGGSTAT_FLT)gsl_stats_long_double_max(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT max; // Maximal value.
    AGGSTAT_INT idx; // Array index.

    // Early exit.
    if (len == 0) {
      return false;
    }

    // Traverse the array.
    max = inp[0];
    idx = 1;
    while (idx < len) {
      // Continuously select greater values.
      if (inp[idx] < max) {
        max = inp[idx];
      }

      idx += 1;
    }
    *out = max;
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the average of an array (offline algorithm).
bool offline_avg_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    avg = (AGGSTAT_FLT)gsl_stats_float_mean(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    avg = (AGGSTAT_FLT)gsl_stats_mean(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    avg = (AGGSTAT_FLT)gsl_stats_long_double_mean(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT avg; // Output value.
    AGGSTAT_INT idx; // Array index.

    avg = AGGSTAT_FLT_0_0;
    idx = 0;
    while (idx < len) {
      avg += (inp[idx] - avg) / (idx + 1);
      idx += 1;
    }
    *out = avg;
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the variance of an array (offline algorithm).
bool offline_var_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    *out = (AGGSTAT_FLT)gsl_stats_float_variance(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    *out = (AGGSTAT_FLT)gsl_stats_variance(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    *out = (AGGSTAT_FLT)gsl_stats_long_double_variance(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT var; // Output value.
    AGGSTAT_INT idx; // Array index.
    AGGSTAT_FLT avg; // Average.
    bool        ret; // Return value.

    // Compute the average.
    ret = offline_avg_run(inp, len, &avg);
    if (ret == false) {
      return false;
    }

    // Traverse the array and compute the variance.
    var = AGGSTAT_FLT_0_0;
    idx = 0;
    while (idx < len) {
      var += ((inp[idx] - avg) * (inp[idx] - avg) - var) / (AGGSTAT_FLT)(idx + 1);
      idx += 1;
    }

    *out = var * ((AGGSTAT_FLT)idx / (AGGSTAT_FLT)(idx + 1));
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the skewness of an array (offline algorithm).
bool offline_skw_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    *out = (AGGSTAT_FLT)gsl_stats_float_skew(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    *out = (AGGSTAT_FLT)gsl_stats_skew(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    *out = (AGGSTAT_FLT)gsl_stats_long_double_skew(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT skw; // Output value.
    AGGSTAT_INT idx; // Array index.
    AGGSTAT_FLT dlt; // Delta.
    AGGSTAT_FLT avg; // Average.
    AGGSTAT_FLT var; // Variance.
    bool        ret; // Return value.

    ret = offline_avg_run(inp, len, &avg);
    if (ret == false) {
      return false;
    }

    ret = offline_var_run(inp, len, &var);
    if (ret == false) {
      return false;
    }

    idx = 0;
    skw = AGGSTAT_FLT_0_0;
    while (idx < len) {
      dlt  = (inp[idx] - avg) / AGGSTAT_FLT_SQRT(var);
      skw += (dlt * dlt * dlt - skw) / (AGGSTAT_FLT)(idx + 1);
      idx += 1;
    }

    *out = skw;
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the kurtosis of an array (offline algorithm).
bool offline_krt_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    *out = (AGGSTAT_FLT)gsl_stats_float_kurtosis(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 64
    *out = (AGGSTAT_FLT)gsl_stats_kurtosis(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 80
    *out = (AGGSTAT_FLT)gsl_stats_long_double_kurtosis(inp, 1, (size_t)len);
  #elif AGGSTAT_FLT_BIT == 128
    AGGSTAT_FLT krt; // Output value.
    AGGSTAT_INT idx; // Array index.
    AGGSTAT_FLT dlt; // Delta.
    AGGSTAT_FLT avg; // Average.
    AGGSTAT_FLT var; // Variance.
    bool        ret; // Return value.

    ret = offline_avg_run(inp, len, &avg);
    if (ret == false) {
      return false;
    }

    ret = offline_var_run(inp, len, &var);
    if (ret == false) {
      return false;
    }

    idx = 0;
    krt = AGGSTAT_FLT_0_0;
    while (idx < len) {
      dlt  = (inp[idx] - avg) / AGGSTAT_FLT_SQRT(var);
      krt += (dlt * dlt * dlt * dlt - krt) / (AGGSTAT_FLT)(idx + 1);
      idx += 1;
    }

    *out = krt - AGGSTAT_FLT_3_0;
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

static int offline_qnt_cmp(
  const void* fst,
  const void* snd)
{
  float val[2];

  val[0] = *(const float*)fst;
  val[1] = *(const float*)snd;

  return (val[0] > val[1]) - (val[0] < val[1]);
}
  
/// Compute a p-quantile of an array (offline algorithm).
bool offline_qnt_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
  const AGGSTAT_FLT                 pct, // Percentile.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  AGGSTAT_INT idx; // Array index.
  AGGSTAT_FLT dlt; // Delta.

  // Sort the input array in ascending order.
  qsort(inp, len, sizeof(AGGSTAT_FLT), offline_qnt_cmp);

  // Determine the index of the sought value.
  idx = (AGGSTAT_INT)(pct * (AGGSTAT_FLT)(len - 1));
  dlt = idx -        (pct * (AGGSTAT_FLT)(len - 1));

  // Select the appropriate element.
  if (idx == len - 1) {
    *out = inp[len - 1];
  } else {
    *out = (AGGSTAT_FLT_1_0 - dlt) * inp[idx]
         +                    dlt  * inp[idx + 1];
  }

  return true;
}

/// Compute the 0.5-quantile of an array (offline algorithm).
bool offline_qnt_0_5_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    gsl_sort_float(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_float_quantile_from_sorted_data(inp, 1, (size_t)len, 0.5);
  #elif AGGSTAT_FLT_BIT == 64
    gsl_sort(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_quantile_from_sorted_data(inp, 1, (size_t)len, 0.5);
  #elif AGGSTAT_FLT_BIT == 80
    gsl_sort_long_double(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_long_double_quantile_from_sorted_data(inp, 1, (size_t)len, 0.5);
  #elif AGGSTAT_FLT_BIT == 128
    return offline_qnt_run(inp, len, AGGSTAT_FLT_0_5, out);
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}

/// Compute the 0.9-quantile of an array (offline algorithm).
bool offline_qnt_0_9_run(
        AGGSTAT_FLT *const restrict inp, // Input array.
  const AGGSTAT_INT                 len, // Length of the input array.
        AGGSTAT_FLT *const restrict out) // Output value.
{
  // Fail upon an empty array.
  if (len == 0) {
    return false;
  }

  #if   AGGSTAT_FLT_BIT == 32
    gsl_sort_float(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_float_quantile_from_sorted_data(inp, 1, (size_t)len, 0.9);
  #elif AGGSTAT_FLT_BIT == 64
    gsl_sort(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_quantile_from_sorted_data(inp, 1, (size_t)len, 0.9);
  #elif AGGSTAT_FLT_BIT == 80
    gsl_sort_long_double(inp, 1, len);
    *out = (AGGSTAT_FLT)gsl_stats_long_double_quantile_from_sorted_data(inp, 1, (size_t)len, 0.9);
  #elif AGGSTAT_FLT_BIT == 128
    return offline_qnt_run(inp, len, AGGSTAT_FLT_0_9, out);
  #else
    #error "invalid value for " AGGSTAT_FLT_BIT
  #endif

  return true;
}
