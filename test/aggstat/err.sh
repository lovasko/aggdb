#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Ensure that any failure fails the whole testing process.
set -e
set -x

cc="gcc"
flags="-std=c99 -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=201912 -Wall -Wextra -Wno-maybe-uninitialized -Wno-format -Werror -DTEST_REP=1000 -DTEST_SEED=1234"
inc="-I../genutil -I../../src"
err_src="err.c gen.c fex.c run.c ../../src/genutil/rng.c ../../src/genutil/clk.c ../../src/aggstat/aggstat.c"
cap_src="cap.c ../../src/genutil/rng.c ../../src/aggstat/aggstat.c"

if [ $(uname -m) = "x86_64" ]
then
  err_lib="-lgsl -lgslcblas -lm -lquadmath"
  cap_lib="-lm -lquadmath"
else
  err_lib="-lgsl -lgslcblas -lm"
  cap_lib="-lm"
fi

# Test lengths.
declare -A test_lens 
test_lens["-DTEST_LEN=10"]="1"
test_lens["-DTEST_LEN=100"]="2"
test_lens["-DTEST_LEN=1000"]="3"
test_lens["-DTEST_LEN=10000"]="4"
test_lens["-DTEST_LEN=100000"]="5"
#test_lens["1000000"]="6"
#test_lens["10000000"]="7"
#test_lens["100000000"]="8"

# Aggregate functions.
declare -A test_funs 
test_funs["-DTEST_FUNC=1"]="fst"
test_funs["-DTEST_FUNC=2"]="lst"
test_funs["-DTEST_FUNC=3"]="cnt"
test_funs["-DTEST_FUNC=4"]="sum"
test_funs["-DTEST_FUNC=5"]="min"
test_funs["-DTEST_FUNC=6"]="max"
test_funs["-DTEST_FUNC=7"]="avg"
test_funs["-DTEST_FUNC=8"]="var"
test_funs["-DTEST_FUNC=9"]="skw"
test_funs["-DTEST_FUNC=10"]="krt"
test_funs["-DTEST_FUNC=11"]="med_1"
test_funs["-DTEST_FUNC=12"]="med_10"
test_funs["-DTEST_FUNC=13"]="med_100"
test_funs["-DTEST_FUNC=14"]="qnt_0_5"
test_funs["-DTEST_FUNC=15"]="qnt_0_9"

# Optimisation levels. 
declare -A test_opts 
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

# Time series generator parameters.
declare -A test_gens 
test_gens["-DTEST_TWL=0    -DTEST_TSC=0.0   -DTEST_NSC=1.0   -DTEST_SLN=0   -DTEST_SSC=0.0" ]="1r"
test_gens["-DTEST_TWL=0    -DTEST_TSC=0.0   -DTEST_NSC=10.0  -DTEST_SLN=0   -DTEST_SSC=0.0" ]="2r"
test_gens["-DTEST_TWL=0    -DTEST_TSC=0.0   -DTEST_NSC=100.0 -DTEST_SLN=0   -DTEST_SSC=0.0" ]="3r"
test_gens["-DTEST_TWL=100  -DTEST_TSC=1.0   -DTEST_NSC=0.0   -DTEST_SLN=0   -DTEST_SSC=0.0" ]="1c"
test_gens["-DTEST_TWL=100  -DTEST_TSC=10.0  -DTEST_NSC=0.0   -DTEST_SLN=0   -DTEST_SSC=0.0" ]="2c"
test_gens["-DTEST_TWL=100  -DTEST_TSC=100.0 -DTEST_NSC=0.0   -DTEST_SLN=0   -DTEST_SSC=0.0" ]="3c"
test_gens["-DTEST_TWL=1000 -DTEST_TSC=0.62  -DTEST_NSC=0.01  -DTEST_SLN=200 -DTEST_SSC=0.37"]="1a"
test_gens["-DTEST_TWL=1000 -DTEST_TSC=6.2   -DTEST_NSC=0.1   -DTEST_SLN=200 -DTEST_SSC=3.7" ]="2a"
test_gens["-DTEST_TWL=1000 -DTEST_TSC=62.0  -DTEST_NSC=1.0   -DTEST_SLN=200 -DTEST_SSC=37.0"]="3a"

# Integer bit widths.
declare -A test_ints
test_ints["-DAGGSTAT_INT_BIT=16 -DGENUTIL_INT_BIT=16"]="16"
test_ints["-DAGGSTAT_INT_BIT=32 -DGENUTIL_INT_BIT=32"]="32"
test_ints["-DAGGSTAT_INT_BIT=64 -DGENUTIL_INT_BIT=64"]="64"

# Floating-point bit widths.
declare -A test_flts
test_flts["-DAGGSTAT_FLT_BIT=32 -DGENUTIL_FLT_BIT=32"]="32"
test_flts["-DAGGSTAT_FLT_BIT=64 -DGENUTIL_FLT_BIT=64"]="64"
test_flts["-DAGGSTAT_FLT_BIT=80 -DGENUTIL_FLT_BIT=80"]="80"
test_flts["-DAGGSTAT_STD=0 -DAGGSTAT_FLT_BIT=128 -DGENUTIL_FLT_BIT=128"]="128"

# Floating-point cuts.
declare -A test_cuts
test_cuts["-DTEST_CUT=0"]="0"
test_cuts["-DTEST_CUT=8"]="1"
test_cuts["-DTEST_CUT=16"]="2"

# Herbie rewrites.
declare -A test_hrbs
test_hrbs[" "]="0"
test_hrbs["-DAGGSTAT_HERB"]="1"

# Clear the existing compilation and execution plans.
> make_plan
> test_plan

# Create the compilation and execution plans.
for test_len in "${!test_lens[@]}"; do
  for test_fun in "${!test_funs[@]}"; do
    for test_opt in "${!test_opts[@]}"; do
      for test_gen in "${!test_gens[@]}"; do
        for test_int in "${!test_ints[@]}"; do
          for test_flt in "${!test_flts[@]}"; do
            for test_cut in "${!test_cuts[@]}"; do
              for test_hrb in "${!test_hrbs[@]}"; do
                # Skip cases where the integer width is not sufficient.
                if [ ${test_ints[${test_int}]} -eq "16" ] && [ ${test_lens[${test_len}]} -ge "5" ]; then
                  continue
                fi

                # Skip cases where the float type is not supported on the platform.
                if [ ${test_flts[${test_flt}]} = "128"] && [$(uname -a) != "x86_64" ]; then
                  continue
                fi

                # Extract the identifiers.
                cap_path="./bin/cap_${test_funs[$test_fun]}_h${test_hrbs[$test_hrb]}_c${test_cuts[$test_cut]}_l${test_lens[$test_len]}_g${test_gens[$test_gen]}_o${test_opts[$test_opt]}_f${test_flts[$test_flt]}_i${test_ints[$test_int]}"
                err_path="./bin/err_${test_funs[$test_fun]}_h${test_hrbs[$test_hrb]}_c${test_cuts[$test_cut]}_l${test_lens[$test_len]}_g${test_gens[$test_gen]}_o${test_opts[$test_opt]}_f${test_flts[$test_flt]}_i${test_ints[$test_int]}"

                # Note the compilation and execution plans.
                #echo ${cc} -DTEST_LEN=${test_len} ${test_gen} -DTEST_FUNC=${test_fun} ${test_cut} ${test_flt} ${test_int} ${test_opt} ${flags} ${inc} ${cap_src} -o ${cap_path} ${cap_lib} >> make_plan
                echo ${cc} ${test_len} ${test_gen} ${test_fun} ${test_hrb} ${test_cut} ${test_flt} ${test_int} ${test_opt} ${flags} ${inc} ${err_src} -o ${err_path} ${err_lib} >> make_plan
                #echo ${cap_path} >> test_plan
                echo ${err_path} >> test_plan
              done
            done
          done
        done
      done
    done
  done
done

# Execute the compilation and execution of identity property tests in parallel.
cat  make_plan | parallel --halt-on-error 2 --tmpdir ./tmp/ -v
shuf test_plan | parallel --halt-on-error 2 --tmpdir ./tmp/ --joblog -  --results err.csv
