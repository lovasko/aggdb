// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#if AGGSTAT_FLT_BIT == 128
  #include <quadmath.h>
#endif

#include <inttypes.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <fenv.h>

#include <gsl/gsl_statistics_long_double.h>
#include <gsl/gsl_sort_long_double.h>

#include "aggstat/aggstat.h"
#include "fex.h"
#include "gen.h"
#include "run.h"
#include "genutil/clk.h"
#include "genutil/rng.h"



// Verify that all configuration variables have been set.
#ifndef TEST_LEN
  #error "must define TEST_LEN to set the length of the tested streams"
#endif

#ifndef TEST_REP
  #error "must define TEST_REP to set the number of test repetitions"
#endif

#ifndef TEST_SEED
  #error "must define TEST_SEED to select a seed for the random number generator"
#endif

#ifndef TEST_FUNC
  #error "must define TEST_FUNC to select the aggregate function"
#endif

#ifndef TEST_NSC
  #error "must define TEST_NSC to set the maximal noise scale"
#endif

#ifndef TEST_TWL
  #error "must define TEST_TWL to set the trend wave length"
#endif

#ifndef TEST_SSC
  #error "must define TEST_SSC to set the maximal season scale"
#endif

#ifndef TEST_SLN
  #error "must define TEST_SLN to set the maximal season length"
#endif

// Select the appropriate online and offline algorithms.
#if   TEST_FUNC == 1
  #define AGGSTAT_FUNC aggstat_fst_run
  #define OFFLINE_FUNC offline_fst_run
#elif TEST_FUNC == 2
  #define AGGSTAT_FUNC aggstat_lst_run
  #define OFFLINE_FUNC offline_lst_run
#elif TEST_FUNC == 3
  #define AGGSTAT_FUNC aggstat_cnt_run
  #define OFFLINE_FUNC offline_cnt_run
#elif TEST_FUNC == 4
  #define AGGSTAT_FUNC aggstat_sum_run
  #define OFFLINE_FUNC offline_sum_run
#elif TEST_FUNC == 5
  #define AGGSTAT_FUNC aggstat_min_run
  #define OFFLINE_FUNC offline_min_run
#elif TEST_FUNC == 6
  #define AGGSTAT_FUNC aggstat_max_run
  #define OFFLINE_FUNC offline_max_run
#elif TEST_FUNC == 7
  #define AGGSTAT_FUNC aggstat_avg_run
  #define OFFLINE_FUNC offline_avg_run
#elif TEST_FUNC == 8
  #define AGGSTAT_FUNC aggstat_var_run
  #define OFFLINE_FUNC offline_var_run
#elif TEST_FUNC == 9
  #define AGGSTAT_FUNC aggstat_skw_run
  #define OFFLINE_FUNC offline_skw_run
#elif TEST_FUNC == 10
  #define AGGSTAT_FUNC aggstat_krt_run
  #define OFFLINE_FUNC offline_krt_run
#elif TEST_FUNC == 11
  #define AGGSTAT_FUNC aggstat_med_1_run
  #define OFFLINE_FUNC offline_qnt_0_5_run
#elif TEST_FUNC == 12
  #define AGGSTAT_FUNC aggstat_med_10_run
  #define OFFLINE_FUNC offline_qnt_0_5_run
#elif TEST_FUNC == 13
  #define AGGSTAT_FUNC aggstat_med_100_run
  #define OFFLINE_FUNC offline_qnt_0_5_run
#elif TEST_FUNC == 14
  #define AGGSTAT_FUNC aggstat_qnt_0_5_run
  #define OFFLINE_FUNC offline_qnt_0_5_run
#elif TEST_FUNC == 15
  #define AGGSTAT_FUNC aggstat_qnt_0_9_run
  #define OFFLINE_FUNC offline_qnt_0_9_run
#else
  #error "invalid TEST_FUNC value, must be [1, 15] inclusive"
#endif

/// Perform a comparisons of streaming and standard algorithms on random data and find their
/// greatest difference.
static void run_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  AGGSTAT_FLT        *const restrict arr, // Memory for stream emulation.
  AGGSTAT_FLT        *const restrict err, // Maximal error found.
  struct genutil_clk *const restrict cag, // AGGSTAT_FUNC clock. 
  struct genutil_clk *const restrict cof, // OFFLINE_FUNC clock.
  struct aggtest_fex *const restrict fag, // AGGSTAT_FUNC floating-point exceptions.
  struct aggtest_fex *const restrict fof) // OFFLINE_FUNC floating-point exceptions.
{
  uintmax_t          idx;    // Array index.
  struct aggtest_fex dum;    // Floating-point exception dummy value.
  struct aggtest_gen gen;    // Time series generator.
  AGGSTAT_FLT        val[2]; // Computed aggregate values.
  bool               ret[2]; // Return values.

  // Prepare the array and clear any exceptions that might have been caused
  // by the random number generation.
  aggtest_fex_new(&dum);
  aggtest_gen_new(&gen, TEST_TWL, TEST_TSC, TEST_SLN, TEST_SSC, TEST_NSC, rng);
  idx = 0;
  while (idx < TEST_LEN) {
    arr[idx] = aggtest_gen_get(&gen, rng);
    idx     += 1;
  }
  aggtest_fex_put(&dum);

  // Execute the streaming computation.
  genutil_clk_put(cag);
  ret[0] = AGGSTAT_FUNC(arr, TEST_LEN, &val[0]);
  genutil_clk_put(cag);
  aggtest_fex_put(fag);
    
  // Verify the validity of the streaming computation.
  if (ret[0] == false || AGGSTAT_FLT_TNAN(val[0])) {
    printf("aggstat failed:\n");
    idx = 0;
    while (idx < TEST_LEN) {
      printf(" " AGGSTAT_FLT_FMT, arr[idx]);
      idx     += 1;
    }
    printf(".\n");
  }
   
  // Execute the standard computation. Has to happen second as the quantile computation mutates the
  // input array data.
  genutil_clk_put(cof);
  ret[1] = OFFLINE_FUNC(arr, TEST_LEN, &val[1]);
  genutil_clk_put(cof);
  aggtest_fex_put(fof);

  // Verify the validity of the standard computation.
  if (ret[1] == false || AGGSTAT_FLT_TNAN(val[1])) {
    printf("offline failed:\n");
    idx = 0;
    while (idx < TEST_LEN) {
      printf(" %" PRIuMAX, (uintmax_t)arr[idx]);
      idx     += 1;
    }
    printf(".\n");
  }

  // Update the error.
  *err = AGGSTAT_FLT_ABS(val[0] - val[1]);
}

static int offline_qnt_cmp(
  const void* fst,
  const void* snd)
{
  float val[2];

  val[0] = *(const float*)fst;
  val[1] = *(const float*)snd;

  return (val[0] > val[1]) - (val[0] < val[1]);
}

int main(void)
{
  struct genutil_rng rng; // Random number generator state.
  struct genutil_clk cag; // AGGSTAT_FUNC clock.
  struct genutil_clk cof; // OFFLINE_FUNC clock.
  struct aggtest_fex fag; // AGGSTAT_FUNC floating-point exceptions.
  struct aggtest_fex fof; // OFFLINE_FUNC floating-point exceptions.
  uintmax_t          min; // Minimal clock.
  uintmax_t          max; // Maximal clock.
  uintmax_t          avg; // Average clock.
  uintmax_t          ofe; // Overflow exception count.
  uintmax_t          ufe; // Underflow exception count.
  uintmax_t          iee; // Inexact exception count.
  AGGSTAT_FLT*       arr; // Array representing stream of values.
  AGGSTAT_FLT*       err; // Maximal error found.
  AGGSTAT_FLT        qnt[5]; // Error quantiles.
  uintmax_t          rep; // Repeat counter.

  // Initialise the random seed, clocks, and floating-point exceptions.
  genutil_rng_new(&rng, TEST_SEED);
  genutil_clk_new(&cag, 100);
  genutil_clk_new(&cof, 100);
  aggtest_fex_new(&fag);
  aggtest_fex_new(&fof);

  // Allocate the array of values.
  arr = calloc(sizeof(AGGSTAT_FLT), TEST_LEN);
  if (arr == NULL) {
    perror("calloc");
    return EXIT_FAILURE;
  }

  err = malloc(sizeof(AGGSTAT_FLT) * TEST_REP);
  if (err == NULL) {
    perror("calloc");
    return EXIT_FAILURE;
  }

  // Find the cap of the error value.
  rep = 0;
  while (rep < TEST_REP) {
    run_test(&rng, arr, &err[rep], &cag, &cof, &fag, &fof);
    rep += 1;
  }

  // Compute multiple quantiles of all the errors.
  qsort(err, TEST_REP, sizeof(AGGSTAT_FLT), offline_qnt_cmp);
  (void)offline_qnt_run(err, TEST_REP, AGGSTAT_FLT_0_75, &qnt[0]);
  (void)offline_qnt_run(err, TEST_REP, AGGSTAT_FLT_0_9,  &qnt[1]);
  (void)offline_qnt_run(err, TEST_REP, AGGSTAT_FLT_0_95, &qnt[2]);
  (void)offline_qnt_run(err, TEST_REP, AGGSTAT_FLT_0_99, &qnt[3]);
  (void)offline_qnt_run(err, TEST_REP, AGGSTAT_FLT_1_0,  &qnt[4]);

  // Report the error quantiles.
  //(void)printf("%Le,%Le,%Le,%Le,%Le", qnt[0], qnt[1], qnt[2], qnt[3], qnt[4]);
  (void)printf(AGGSTAT_FLT_FMT
           "," AGGSTAT_FLT_FMT
           "," AGGSTAT_FLT_FMT
           "," AGGSTAT_FLT_FMT
           "," AGGSTAT_FLT_FMT,
    qnt[0], qnt[1], qnt[2], qnt[3], qnt[4]);
  
  // Report the online algorithm runtime statistics and floating-point environment exceptions.
  genutil_clk_get(&cag, &min, &max, &avg);
  aggtest_fex_get(&fag, &ofe, &ufe, &iee);
  (void)printf(",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ,
    min, max, avg, ofe, ufe, iee);

  // Report the offline algorithm runtime statistics and floating-point environment exceptions.
  genutil_clk_get(&cof, &min, &max, &avg);
  aggtest_fex_get(&fof, &ofe, &ufe, &iee);
  (void)printf(",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX ",%" PRIuMAX,
    min, max, avg, ofe, ufe, iee);

  // Release the array of values.
  free(arr);

  return EXIT_SUCCESS;
}
