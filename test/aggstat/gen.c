// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.
//

#include <inttypes.h>
#include <stdio.h>

#include "genutil/rng.h"
#include "gen.h"


#define AGGSTAT_FLT_8_0 AGGSTAT_FLT_NUM(8, 0, +, 0)
#define AGGSTAT_FLT_0_0001 AGGSTAT_FLT_NUM(1, 0, -, 3)
#define AGGSTAT_FLT_3_1415 AGGSTAT_FLT_NUM(3, 1415, +, 0)

/// Linear interpolation.
static AGGSTAT_FLT aggtest_gen_lin(
  const AGGSTAT_FLT fst, // First value.
  const AGGSTAT_FLT snd, // Second value.
  const AGGSTAT_FLT par) // Interpolation parameter.
{
  return fst * (AGGSTAT_FLT_1_0 - par) + snd * par;
}

/// Cosine interpolation.
static AGGSTAT_FLT aggtest_gen_cos(
  const AGGSTAT_FLT fst, // First value.
  const AGGSTAT_FLT snd, // Second value.
  const AGGSTAT_FLT par) // Interpolation parameter.
{
  return aggtest_gen_lin(fst, snd, (AGGSTAT_FLT_1_0 - AGGSTAT_FLT_COS(par * AGGSTAT_FLT_3_1415)) / AGGSTAT_FLT_2_0);
}

/// Initialise a time series generator.
void aggtest_gen_new(
        struct aggtest_gen *const restrict gen, // Time series generator.
  const AGGSTAT_INT                        twl, // Maximal trend wave length.
  const AGGSTAT_FLT                        tsc, // Maximal trend scale.
  const AGGSTAT_INT                        sln, // Maximal season length.
  const AGGSTAT_FLT                        ssc, // Maximal season scale.
  const AGGSTAT_FLT                        nsc, // Maximal noise scale.
        struct genutil_rng *const restrict rng) // Random number generator.
{
  // Save the object boundaries.
  gen->ag_twl = twl;
  gen->ag_tsc = tsc;
  gen->ag_sln = sln;
  gen->ag_ssc = ssc;
  gen->ag_nsc = nsc;

  // Determine the first trend wave.
  gen->ag_tpx = 0;
  gen->ag_tpy = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, tsc);
  gen->ag_tnx = genutil_rng_int(rng, 0, twl);
  gen->ag_tny = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, tsc);

  // Determine the length of the season. The season can be greater than the trend wave.
  gen->ag_sln    = genutil_rng_int(rng, 5, sln + 5);

  // Based on the length of the season, pick a mean of the Gauss curve. The mean denotes the mode of
  // the distribution and should be located in the middle section of the season. To ensure this, the
  // mean is generated such that the first 1/8 and the last 1/8 are removed from the interval.

  // The heights are selected at random, with the exception of the first and the last, that are selected  // to be zero.
  gen->ag_spy[0] = AGGSTAT_FLT_0_0;
  gen->ag_spy[1] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, gen->ag_ssc);
  gen->ag_spy[2] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, gen->ag_ssc);
  gen->ag_spy[3] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, gen->ag_ssc);
  gen->ag_spy[4] = AGGSTAT_FLT_0_0;

  // The times are selected so that they are ordered chronologically, with the first and last denoting the start and end of the season.
  gen->ag_spx[0] = 0;
  gen->ag_spx[4] = gen->ag_sln - 1;
  gen->ag_spx[2] = genutil_rng_int(rng, 0,              gen->ag_sln - 1);
  gen->ag_spx[1] = genutil_rng_int(rng, 0,              gen->ag_spx[2]);
  gen->ag_spx[3] = genutil_rng_int(rng, gen->ag_spx[2], gen->ag_sln - 1);

  //gen->ag_smn[0] = genutil_rng_flt(rng, eig, ((AGGSTAT_FLT)gen->ag_sln) - eig);
  //gen->ag_smn[1] = genutil_rng_flt(rng, eig, ((AGGSTAT_FLT)gen->ag_sln) - eig);

  // The standard deviation of the distribution determines its width. As the seasonality component
  // ought to start and end at zero, the appropriate value should be selected. The width of the
  // Gauss curve is roughly 99.7% covered by 3 standard deviations. Similarly, the height of the
  // distribution is computed as 0.4 divided by standard deviation. We want to ensure that the
  // height does not go above the selected scale.
  //min = AGGSTAT_FLT_FMIN(gen->ag_smn[0], (AGGSTAT_FLT)gen->ag_sln - gen->ag_smn[0]);
  //gen->ag_sdv[0] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0001, min / AGGSTAT_FLT_3_0);

  //min = AGGSTAT_FLT_FMIN(gen->ag_smn[1], (AGGSTAT_FLT)gen->ag_sln - gen->ag_smn[1]);
 // gen->ag_sdv[1] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0001, min / AGGSTAT_FLT_3_0);

  //printf("%f %f %f %f\n",
  // (float)gen->ag_smn[0], (float)gen->ag_sdv[0],
  // (float)gen->ag_smn[1], (float)gen->ag_sdv[1]);

  // Reset the counter.
  gen->ag_ctr = 0;
}

// TODO simplify the time series generation
//  - make trend lines linear
//  - make the seasonality into 5 points
//  - first and last points are of value 0
//  - all other are random
//  - there are random times as well

/// Obtain the next value in the time series.
AGGSTAT_FLT aggtest_gen_get(
  struct aggtest_gen *const restrict gen, // Time series generator.
  struct genutil_rng *const restrict rng) // Random number generator.
{
  AGGSTAT_FLT val[3]; // Noise, trend, and seasonality.
  AGGSTAT_FLT ins;    // Time within a season.
  AGGSTAT_FLT par;    // Cosine interpolation parameter.

  // Generate a random noise.
  if (gen->ag_nsc > 0.0f) {
    val[0] = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, gen->ag_nsc);
  } else {
    val[0] = 0.0f;
  }

  if (gen->ag_twl > 0) {
    // Determine the new trend wave if the last one was finished.
    if (gen->ag_ctr == gen->ag_tnx) {
      gen->ag_tpx = gen->ag_tnx;
      gen->ag_tpy = gen->ag_tny;
      gen->ag_tnx = genutil_rng_int(rng, gen->ag_ctr + 1, gen->ag_ctr + gen->ag_twl);
      gen->ag_tny = genutil_rng_flt(rng, AGGSTAT_FLT_0_0, gen->ag_tsc);
    }

    // Compute the value of the trend wave.
    par    = AGGSTAT_FLT_1_0 - (AGGSTAT_FLT)(gen->ag_tnx - gen->ag_ctr) / (AGGSTAT_FLT)(gen->ag_tnx - gen->ag_tpx) ; 
    val[1] = aggtest_gen_lin(gen->ag_tpy, gen->ag_tny, par);
    //printf("par=%Lf ctr=%" PRIuMAX "\n", (long double)par, (uintmax_t)gen->ag_ctr);
    //printf("tnx=%" PRIuMAX " tpx=%" PRIuMAX "\n", (uintmax_t)gen->ag_tnx, (uintmax_t)gen->ag_tpx);
    //xhv    = (AGGSTAT_FLT_1_0 - cos(par * AGGSTAT_FLT_3_1415)) / AGGSTAT_FLT_2_0;
    //val[1] = gen->ag_tpy * (AGGSTAT_FLT_1_0 - xhv) + gen->ag_tny * xhv; 
  } else {
    val[1] = 0.0f;
  }

  // Compute the seasonality component.
  if (gen->ag_sln > 0) {
    //ins     = (AGGSTAT_FLT)(gen->ag_ctr % gen->ag_sln);
    //val[2]  = AGGSTAT_FLT_1_0 / (gen->ag_sdv[0] * AGGSTAT_FLT_SQRT(AGGSTAT_FLT_2_0 * AGGSTAT_FLT_3_1415))
   //         * AGGSTAT_FLT_EXP((-AGGSTAT_FLT_0_5) * AGGSTAT_FLT_POW((ins - gen->ag_smn[0]) / gen->ag_sdv[0], AGGSTAT_FLT_2_0));
   // val[2] += AGGSTAT_FLT_1_0 / (gen->ag_sdv[1] * AGGSTAT_FLT_SQRT(AGGSTAT_FLT_2_0 * AGGSTAT_FLT_3_1415))
   //         * AGGSTAT_FLT_EXP((-AGGSTAT_FLT_0_5) * AGGSTAT_FLT_POW((ins - gen->ag_smn[1]) / gen->ag_sdv[1], AGGSTAT_FLT_2_0));


    // Determine the position within the season.
    ins     = (AGGSTAT_FLT)(gen->ag_ctr % gen->ag_sln);
    val[2]  = AGGSTAT_FLT_0_0;

    // T
    if (ins > gen->ag_spx[3]) {
      par    = AGGSTAT_FLT_1_0
	     - (AGGSTAT_FLT)(gen->ag_spx[4] - ins)
	     / (AGGSTAT_FLT)(gen->ag_spx[4] - gen->ag_spx[3]); 
      val[2] = aggtest_gen_cos(gen->ag_spy[3], gen->ag_spy[4], par);
    }

    // 
    if (ins > gen->ag_spx[2] && ins <= gen->ag_spx[3]) {
      par    = AGGSTAT_FLT_1_0
	     - (AGGSTAT_FLT)(gen->ag_spx[3] - ins)
	     / (AGGSTAT_FLT)(gen->ag_spx[3] - gen->ag_spx[2]); 
      val[2] = aggtest_gen_cos(gen->ag_spy[2], gen->ag_spy[3], par);
    }

    //
    if (ins > gen->ag_spx[1] && ins <= gen->ag_spx[2]) {
      par    = AGGSTAT_FLT_1_0
	     - (AGGSTAT_FLT)(gen->ag_spx[2] - ins)
	     / (AGGSTAT_FLT)(gen->ag_spx[2] - gen->ag_spx[1]); 
      val[2] = aggtest_gen_cos(gen->ag_spy[1], gen->ag_spy[2], par);
    }

    //
    if (ins > gen->ag_spx[0] && ins <= gen->ag_spx[1]) {
      par    = AGGSTAT_FLT_1_0
	     - (AGGSTAT_FLT)(gen->ag_spx[1] - ins)
	     / (AGGSTAT_FLT)(gen->ag_spx[1] - gen->ag_spx[0]); 
      val[2] = aggtest_gen_cos(gen->ag_spy[0], gen->ag_spy[1], par);
    }

  } else {
    val[2] = 0.0f;
  }

  // Advance to the next value.
  gen->ag_ctr += 1;

  // Combine the three elements into the result.
  return val[0] + val[1] + val[2];
}
