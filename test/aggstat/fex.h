// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef AGGSTAT_TEST_FEX_H
#define AGGSTAT_TEST_FEX_H

#include <stdbool.h>
#include <stdint.h>


struct aggtest_fex {
  uintmax_t af_ofe; // Overflow exception.
  uintmax_t af_ufe; // Underflow exception.
  uintmax_t af_iee; // Inexact exception.
  uintmax_t af_cnt; // Number of observations.
};

void aggtest_fex_new(struct aggtest_fex *const fex);
void aggtest_fex_put(struct aggtest_fex *const fex);
bool aggtest_fex_get(
  struct aggtest_fex *const restrict fex,
  uintmax_t          *const restrict ofe,
  uintmax_t          *const restrict ufe,
  uintmax_t          *const restrict iee);

#endif
