// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef AGGSTAT_TEST_GEN_H
#define AGGSTAT_TEST_GEN_H

#include <stdbool.h>

#include "gen.h"
#include "genutil/rng.h"
#include "aggstat/aggstat.h"


// IDEA: probability that season is missed
//  * the scenario tested is that the regular pattern is broken.

/// Time series generator.
struct aggtest_gen {
  AGGSTAT_INT ag_ctr;    // Counter.
  AGGSTAT_INT ag_twl;    // Maximal trend wave length.
  AGGSTAT_INT ag_tpx;    // Trend previous time.
  AGGSTAT_INT ag_tnx;    // Trend next time.
  AGGSTAT_FLT ag_tpy;    // Trend previous height.
  AGGSTAT_FLT ag_tny;    // Trend next height.
  AGGSTAT_FLT ag_tsc;    // Maximal trend scale.
  AGGSTAT_INT ag_sln;    // Season length.
  AGGSTAT_FLT ag_ssc;    // Season scale.
  AGGSTAT_FLT ag_spy[5]; // Season peak heights.
  AGGSTAT_FLT ag_spx[5]; // Season peak times.
  AGGSTAT_FLT ag_nsc;    // Maximal noise scale.
};

/// Initialise a time series generator.
void aggtest_gen_new(
        struct aggtest_gen *const restrict gen,  // Time series generator.
  const AGGSTAT_INT                        twl, // Maximal trend wave length.
  const AGGSTAT_FLT                        tsc, // Maximal trend scale.
  const AGGSTAT_INT                        sln, // Maximal season length.
  const AGGSTAT_FLT                        ssc, // Maximal season scale.
  const AGGSTAT_FLT                        nsc, // Maximal noise scale.
        struct genutil_rng *const restrict rng); // Random number generator.

/// Obtain the next value in the time series.
AGGSTAT_FLT aggtest_gen_get(
  struct aggtest_gen *const restrict gen,  // Time series generator.
  struct genutil_rng *const restrict rng); // Random number generator.

#endif
