// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <inttypes.h>

#include <pthread.h>

#include "quempsc/quempsc.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Thread context.
struct thrdctx {
          struct quempsc* tc_que; // Queue.
          QUEMPSC_INT     tc_idx; // Thread index.
  _Atomic QUEMPSC_INT     tc_sum; // Sum of values.
  _Atomic QUEMPSC_INT     tc_cnt; // 
};

/// Producer thread that places random integers onto the queue.
static void* profunc(
  void* arg) // Thread arguments.
{
  QUEMPSC_INT        ctr; // Event counter.
  struct thrdctx*    ctx; // Thread context.
  struct genutil_rng rng; // Random number generator.
  QUEMPSC_INT        val; // Enqueued value.
  bool               ret; // Return value.

  // Unpack the tread arguments.
  ctx = (struct thrdctx*)arg;

  // Each thread has its own random number generator that is unique based
  // on its thread ID.
  genutil_rng_new(&rng, TEST_SEED + ctx->tc_idx);

  // Repeatedly push random integers to the queue.
  ctr = 0;
  while (ctr < TEST_LEN) {
    // Generate a non-zero integer and attempt to push it to the queue.
    val = genutil_rng_int(&rng, 1, 4);
    ret = quempsc_put(ctx->tc_que, val);

    // Update the global sum of integers upon a successful push.
    if (ret == true) {
      atomic_fetch_add(&ctx->tc_sum, val);
      ctr += 1;
    }

    // Allow other threads to manipulate the queue.
    sched_yield();
  }

  return NULL;
}

/// Consumer thread that reads integers from the queue.
static void* confunc(
  void* arg) // Thread arguments.
{
  QUEMPSC_INT     ctr; // Event counter.
  QUEMPSC_INT     val; // Retrieved value.
  struct thrdctx* ctx; // Thread context.
  bool            ret; // Return value.

  // Unpack the thread arguments.
  ctx = (struct thrdctx*)arg;

  // Repeatedly pop values from the queue.
  ctr = 0;
  while (ctr < TEST_LEN * 6) {
    // Retrieve a single value and update the global sum upon a successful pop.
    ret = quempsc_get(ctx->tc_que, &val);
    if (ret == true) {
      atomic_fetch_add(&ctx->tc_sum, val);
      ctr         += 1;
    }

    // Allow other threads to manipulate the queue.
    sched_yield();
  }

  return NULL;
}

/// Execute a single instance of the test.
static bool run_test(void)
{
  pthread_t       con;    // Consumer thread.
  pthread_t       pro[6]; // Producer threads.
  struct thrdctx  ctx[7]; // Thread contexts.
  struct quempsc  que;    // Queue.
  int             ret;    // Return value.
  QUEMPSC_INT     idx;    // Array index.
  QUEMPSC_INT     sum;    // Overall sum.

  // Create a common queue.
  ret = quempsc_new(&que, 128);
  if (ret == 0) {
    printf("unable to create a queue\n");
    return false;
  }

  // Start the consumer thread.
  ctx[0].tc_que = &que;
  ctx[0].tc_sum = 0;
  ret = pthread_create(&con, NULL, confunc, &ctx[0]);
  if (ret != 0) {
    printf("unable to create the consumer thread\n");
    return false;
  }

  // Start the producer threads.
  idx = 1;
  while (idx < 7) {
    ctx[idx].tc_que = &que;
    ctx[idx].tc_sum = 0;
    ctx[idx].tc_idx = idx - 1;
    ret = pthread_create(&pro[idx - 1], NULL, profunc, &ctx[idx]);
    if (ret != 0) {
      printf("unable to create a producer thread\n");
      return false;
    }
    idx += 1;
  } 

  // Wait for the consumer thread.
  ret = pthread_join(con, NULL);
  if (ret != 0) {
    printf("unable to join the consumer thread\n");
    return false;
  }

  // Wait for each of the producer threads and produce the overall sum.
  idx = 0;
  sum = 0;
  while (idx < 6) {
    ret = pthread_join(pro[idx], NULL);
    if (ret != 0) {
      printf("unable to join a producer thread %" PRIuMAX "\n", (uintmax_t)idx);
      return false;
    }

    sum += ctx[idx + 1].tc_sum;
    idx += 1;
  }

  // Verify that the sums match.
  if (sum != ctx[0].tc_sum) {
    printf("sums not matching exp=%" PRIuMAX " act=%" PRIuMAX "\n",
      (uintmax_t)sum, (uintmax_t)ctx[0].tc_sum);
    return false;
  }

  return true;
}

/// Test that producers communicate the values through the queue.
int main(void)
{
  uintmax_t rep; // Repetition counter.
  bool      ret; // Return value.

  // Repeatedly execute the test.
  rep = 0;
  while (rep < TEST_REP) {
    ret = run_test();
    if (ret == false) {
      return EXIT_FAILURE;
    }
    rep += 1;
  }

  return EXIT_SUCCESS;
}
