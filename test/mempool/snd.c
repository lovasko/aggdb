// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdatomic.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include <pthread.h>
#include <sched.h>

#include "mempool/mempool.h"
#include "bitutil/bitutil.h"
#include "genutil/rng.h"


#define NTHR 16    // Number of threads.
#define NIDX 8     // Number of allocable object in each thread.
#define NACT 10000 // Number of actions.

struct thrdctx {
          MEMPOOL_INT       tc_idx; // Thread index.
          struct mempool*   tc_mem; // Memory pool.
  _Atomic MEMPOOL_INT*      tc_set; // Bitset of used objects.
  _Atomic MEMPOOL_INT*      tc_err; // Error counter.
};

static void* thr_func(void* arg)
{
  struct genutil_rng rng;       // Random number generator.
  struct thrdctx*    ctx;       // Thread context.
  uintmax_t          ctr;       // Action counter.
  MEMPOOL_INT        act;       // Action.
  uintmax_t          cnt;       // Number of currently allocated objects.
  MEMPOOL_INT        idx[NIDX]; // Allocated objects..
  MEMPOOL_INT        ret;       // Allocated object index.
  MEMPOOL_INT        val;       // Object integer value.

  memset(idx, 0, NIDX * sizeof(MEMPOOL_INT));
  cnt = 0;

  ctx = (struct thrdctx*)arg;

  genutil_rng_new(&rng, ctx->tc_idx);

  ctr = 0;
  while (ctr < TEST_LEN) {
    // Randomly select an operation.
    act = genutil_rng_int(&rng, 0, 1);
    if ((act == 0 && cnt > 0) || cnt >= NIDX) {
      // Verify that the address contain the index of this thread
      val = mempool_get(ctx->tc_mem, idx[cnt - 1], 1);
      if (val != ctx->tc_idx) {
        return NULL;
      }

      // Write zero to the memory.
      mempool_put(ctx->tc_mem, idx[cnt - 1], 1, 0);

      // Release the memory back to the pool.
      mempool_rel(ctx->tc_mem, idx[cnt - 1]);
      
      cnt -= 1;
      ctr += 1;
      sched_yield();
    } else {
      // Attempt to allocate a memory block.
      ret = mempool_acq(ctx->tc_mem);
      if (ret == 0) {
        continue;
      }

      // Verify that the memory block contains zero.
      val = mempool_get(ctx->tc_mem, ret, 1);
      if (val != 0) {
        return NULL;
      }

      // Write the ID of the current thread.
      mempool_put(ctx->tc_mem, ret, 1, ctx->tc_idx);

      // Note the index down.
      idx[cnt] = ret;
      cnt     += 1;

      // Note the index in the overall used cells.
      (void)atomic_fetch_or(ctx->tc_set, ((uint64_t)1 << ret));
      sched_yield();
    }
  }

  // Return all remaining cells.
  while (cnt > 0) {
    // Clear the identifier before returning the cell.
    mempool_put(ctx->tc_mem, idx[cnt - 1], 1, 0);
    mempool_rel(ctx->tc_mem, idx[cnt - 1]);
    cnt -= 1;
  }

  return NULL;
}

static bool run_test(void)
{
  _Atomic MEMPOOL_INT      set;       // Bitset of used objects.
  _Atomic MEMPOOL_INT      err;       // Number of errors.
          MEMPOOL_INT      fin;       // Final values after the test.
          struct mempool   mem;       // Memory pool.
          pthread_t        thr[NTHR]; // Thread handles.
          struct thrdctx   ctx[NTHR]; // Thread contexts.
          int              ret;       // Return value.
          MEMPOOL_INT      idx;       // Array index.

  // Create the memory pool.
  ret = (int)mempool_new(&mem, MEMPOOL_INT_BIT, 2);
  if (ret == (int)false) {
    printf("unable to allocate memory for the memory pool\n");
    return false;
  }

  // Reset the used cell notifications.
  atomic_store(&set, 0);
  atomic_store(&err, 0);

  // Start the threads.
  idx = 0;
  while (idx < NTHR) {
    // Initialise the context.
    ctx[idx].tc_idx = idx + 1;
    ctx[idx].tc_mem = &mem;
    ctx[idx].tc_set = &set;
    ctx[idx].tc_err = &err;

    // Launch the thread.
    ret = pthread_create(&thr[idx], NULL, thr_func, &ctx[idx]);
    if (ret != 0) {
      printf("unable to launch a thread\n");
      return false;
    }
    
    // Advance to the next thread.
    idx += 1;
  }

  // One by one collect the threads.
  idx = 0;
  while (idx < NTHR) {
    // Wait for the thread to finish.
    ret = pthread_join(thr[idx], NULL);
    if (ret != 0) {
      printf("unable to join a thread\n");
      return false;
    }

    // Advance to the next thread.
    idx += 1;
  }

  // Retrieve the number of errors.
  fin = atomic_load(&err);
  if (fin > 0) {
    printf("error: threads reported errors\n");
    return false;
  }

  // Verify that all memory cells were returned to the memory pool.
  fin = atomic_load(&mem.mp_cnt);
  if (fin != 0) {
    printf("some threads did not return their cells\n");
    return false;
  }

  // Check whether a reasonable number of cells were used as part of the testing.
  fin = atomic_load(&set);
  if (fin != (MEMPOOL_INT)-1) {
    printf("not all cells were used: %" PRIuMAX "\n", (uintmax_t)bitutil_pop(set));
  }

  mempool_del(&mem);
  return true;
}

int main(void)
{
  MEMPOOL_INT rep; // Repetition counter.
  bool        ret; // Return value.

  // Repeatedly execute the test, each time passing a different random seed.
  rep = 0;
  while (rep < TEST_REP) {
    ret = run_test();
    if (ret == false) {
      return EXIT_FAILURE;
    }
    rep += 1;
  }

  return EXIT_SUCCESS;
}
