#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Ensure that any failure fails the whole testing process.
set -e
set -x

# Compilation settings.
cc="gcc"
flag="-std=c11 -Wall -Wextra -Werror -D_XOPEN_SOURCE -DTEST_SEED=210 -DTEST_REP=1000"
inc="-I../ -I../../src"
lib="../../src/mempool/mempool.c ../../src/genutil/rng.c ../../src/bitutil/bitutil.c -pthread"

# Test input lengths.
declare -A test_lens 
test_lens["-DTEST_LEN=10"]="1"
test_lens["-DTEST_LEN=100"]="2"
test_lens["-DTEST_LEN=1000"]="3"
test_lens["-DTEST_LEN=10000"]="4"
test_lens["-DTEST_LEN=100000"]="5"
test_lens["-DTEST_LEN=1000000"]="6"

# Compiler optimisation levels.
declare -A test_opts
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

# Bit widths.
declare -A test_ints
test_ints["-DMEMPOOL_INT_BIT=16 -DBITUTIL_INT_BIT=16 -DGENUTIL_INT_BIT=16 -DGENUTIL_FLT_EXC"]="16"
test_ints["-DMEMPOOL_INT_BIT=32 -DBITUTIL_INT_BIT=32 -DGENUTIL_INT_BIT=32 -DGENUTIL_FLT_EXC"]="32"

# Clear the existing compilation and execution plans.
> ./bin/make_plan
> ./bin/test_plan

# Create the compilation and execution plans.
for test_opt in "${!test_opts[@]}"; do
  for test_len in "${!test_lens[@]}"; do
    for test_int in "${!test_ints[@]}"; do
      # Skip cases where the integer width is not sufficient.
      if [ ${test_ints[${test_int}]} -eq "16" ] && [ ${test_lens[${test_len}]} -ge "5" ]; then
        continue
      fi
      if [ ${test_ints[${test_int}]} -eq "8"  ] && [ ${test_lens[${test_len}]} -ge "3" ]; then
        continue
      fi

      # Extract the identifiers.
      snd_path="./bin/snd_l${test_lens[$test_len]}_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}"
      
      # Note the compilation and execution plans.
      echo ${cc} ${test_len} ${test_int} ${flag} ${test_opt} ${inc} ${lib} snd.c -o ${snd_path} >> ./bin/make_plan
      echo ${snd_path} >> ./bin/test_plan
    done
  done
done

# Execute the compilation and execution of identity property tests in parallel.
parallel --tmpdir ./tmp/ -v         < ./bin/make_plan
parallel --tmpdir ./tmp/ --joblog - < ./bin/test_plan
