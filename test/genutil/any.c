// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_ERR
  #error "must set TEST_ERR to specify the maximal allowed error"
#endif

static bool run_test(
  const GENUTIL_INT rep) // Repetition number.
{
  struct genutil_rng rng;     // Secondary random number generator.
  GENUTIL_INT        idx;     // Array and loop index.
  GENUTIL_INT        any;     // Random value.
  GENUTIL_INT        min[2];  // Minimal value for modulo and division.
  GENUTIL_INT        max[2];  // Maximal value for modulo and division.
  GENUTIL_INT        mod[128]; // Modulo counters.
  GENUTIL_INT        div[128]; // Division counters.

  // Initialise the random number generator.
  genutil_rng_new(&rng, (uint32_t)rep);

  // Reset all counters.
  idx = 0;
  while (idx < 128) {
    mod[idx] = 0;
    div[idx] = 0;
    idx     += 1;
  }

  // Generate random numbers 
  idx = 0;
  while (idx < TEST_LEN) {
    any = genutil_rng_any(&rng);
    mod[any % 128]                     += 1;
    div[any / (GENUTIL_INT_MAX / 128)] += 1;
    idx                                += 1;
  }

  // Find the minimal number of hits.
  min[0] = mod[0];
  min[1] = div[0];
  idx = 1;
  while (idx < 128) {
    if (mod[idx] < min[0]) {
      min[0] = mod[idx];
    }
    if (div[idx] < min[1]) {
      min[1] = div[idx];
    }
    idx += 1;
  }

  // Find the maximal number of hits.
  max[0] = mod[0];
  max[1] = div[0];
  idx = 1;
  while (idx < 128) {
    if (mod[idx] > max[0]) {
      max[0] = mod[idx];
    }
    if (div[idx] > max[1]) {
      max[1] = div[idx];
    }
    idx += 1;
  }

  // Verify that the difference between them is within the expected error size.
  if (min[0] * TEST_ERR < max[0]) {
    (void)printf("modulo error too high min=%" PRIuMAX " max=%" PRIuMAX " err=%" PRIuMAX "\n",
      (uintmax_t)min[0], (uintmax_t)max[0], (uintmax_t)TEST_ERR);
    return false;
  }
  if (min[1] * TEST_ERR < max[1]) {
    (void)printf("division error too high min=%" PRIuMAX " max=%" PRIuMAX " err=%" PRIuMAX "\n",
      (uintmax_t)min[1], (uintmax_t)max[1], (uintmax_t)TEST_ERR);
    return false;
  }

  return true;
}

int main(void)
{
  GENUTIL_INT rep; // Repetition counter.
  bool        ret; // Return value.

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    // Execute a single test.
    ret = run_test(rep);
    if (ret == false) {
      break;
    }

    // Advance to the next test invocation.
    rep += 1;
  }

  // Terminate the process upon running into a failed test case.
  if (ret == false) {
    return EXIT_FAILURE;
  } else {
    return EXIT_SUCCESS;
  }
}
