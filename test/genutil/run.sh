#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Ensure that any failure fails the whole testing process.
set -e
set -x

# Compilation settings.
cc="gcc"
flag="-D_POSIX_C_SOURCE=201912 -std=c99 -Wall -Wextra -DTEST_LEN=50000 -DTEST_REP=100 -DTEST_ERR=2"
lib="../../src/genutil/rng.c"
inc="-I../../src/ -I../ -I../../test"

# Compiler optimisation levels.
declare -A test_opts
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

# Integer bit widths.
declare -A test_ints
test_ints["-DGENUTIL_INT_BIT=16" ]="16"
test_ints["-DGENUTIL_INT_BIT=32" ]="32"
test_ints["-DGENUTIL_INT_BIT=64" ]="64"
test_ints["-DGENUTIL_INT_BIT=128"]="128

# Floating-point bit widths.
declare -A test_flts
test_flts["-DGENUTIL_FLT_BIT=32"]="32"
test_flts["-DGENUTIL_FLT_BIT=64"]="64"
test_flts["-DGENUTIL_FLT_BIT=80"]="80"

# Clear the existing compilation and execution plans.
> make_plan
> test_plan

# Create the compilation and execution plans.
for test_opt in "${!test_opts[@]}"; do
  for test_int in "${!test_ints[@]}"; do
    # Extract the identifiers.
    any_path="./bin/any_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}"
    int_path="./bin/int_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}"
      
    # Note the compilation and execution plans.
    echo ${cc} ${test_int} -DGENUTIL_FLT_EXC ${flag} ${test_opt} ${inc} ${lib} any.c -o ${any_path} >> ./make_plan
    #echo ${cc} ${test_int} -DGENUTIL_FLT_EXC ${flag} ${test_opt} ${inc} ${lib} int.c -o ${int_path} >> ./make_plan
    echo ${any_path} >> ./test_plan
    echo ${int_path} >> ./test_plan

    for test_flt in "${!test_flts[@]}"; do
      # Extract the identifiers.
      flt_path="./bin/flt_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}_f${test_flts[${test_flt}]}"
      dbg_path="./bin/dbg_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}_f${test_flts[${test_flt}]}"

      # Note the compilation and execution plans.
      #echo ${cc} ${test_int} ${test_flt} ${flag} ${test_opt} ${inc} ${lib} flt.c -o ${flt_path} >> ./make_plan
      echo ${cc} ${test_int} ${test_flt} ${flag} ${test_opt} ${inc} ${lib} dbg.c -o ${dbg_path} >> ./make_plan
      echo ${flt_path} >> ./test_plan
      echo ${dbg_path} >> ./test_plan
    done
  done
done

# Execute the compilation and execution of identity property tests in parallel.
parallel --tmpdir ./tmp/ -v         < ./make_plan
parallel --tmpdir ./tmp/ --joblog - < ./test_plan
