#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include "strhash/strhash.h"
#include "genutil/rng.h"
#include "genutil/clk.h"


// Select a random nothing-up-my-sleeve seed.
#ifndef TEST_SEED
  #define TEST_SEED 12345
#endif

// Repetition count.
#ifndef TEST_REP
  #define TEST_REP 100000000
#endif

// Measurement repetition threshold.
#ifndef TEST_MTH
  #define TEST_MTH 1000
#endif

/// Execute a single test where the identity under composition is tested.
static bool run_test(
  struct genutil_rng *const restrict rng) // Random number generator.
{
  STRHASH_INT val[2];               // Integer representation.
  char        str[STRHASH_LEN + 1]; // String representation.
  bool        ret[2];               // Return values.
  
  // Ensure that the string buffer is cleared.
  (void)memset(str, '\0', STRHASH_LEN + 1);

  // Generate random value.
  val[0] = genutil_rng_any(rng);

  // Execute the encoding and decoding steps and measure their times.
  ret[0] = strhash_get(str, STRHASH_LEN, val[0]);
  ret[1] = strhash_put(&val[1], str, STRHASH_LEN);

  // Ensure that the functions runs were successful.
  if (ret[0] == false) {
    printf("failure: get(%" STRHASH_INT_FMT ")\n", val[0]);
    return false;
  }
  if (ret[1] == false) {
    printf("failure: put(%s)\n", str);
    return false;
  }

  // Ensure that the encode/decode values are identical.
  if (val[0] != val[1]) {
    printf("mismatch: exp=%" STRHASH_INT_FMT " act=%" STRHASH_INT_FMT "\n", val[0], val[1]);
    return false;
  }

  return true;
}

/// Run a set of tests that verify that strhash_put and strhash_get functions form an identity when
/// composed. The input values are selected randomly and do not exhaust the whole input domain. The
/// pseudo-random number generator used is deterministic across any system/architecture/libc in
/// order to avoid non-repeatable runs. The run times of the two functions are measured. 
int main(void)
{
  struct genutil_rng rng; // Random number generator.
  uintmax_t          rep; // Repetition counter.
  bool               ret; // Return value.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    // Execute a single test.
    ret = run_test(&rng);
    if (ret == false) {
      break;
    }

    // Advance to the next test invocation.
    rep += 1;
  }

  // Terminate the process upon running into a failed test case.
  if (ret == false) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
