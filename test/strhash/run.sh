#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Ensure that any failure fails the whole testing process.
set -e
set -x

# Compilation settings.
cc="gcc"
flag="-D_POSIX_C_SOURCE=201912 -std=c99 -Wall -Wextra -Wno-stringop-overflow -Werror"
lib="../../src/genutil/rng.c ../../src/genutil/clk.c ../../src/strhash/strhash.c"
inc="-I../../src/ -I../ -I../../test"

# Compiler optimisation levels.
declare -A test_opts
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

# Bit widths.
declare -A test_ints
test_ints["-DSTRHASH_INT_BIT=16 -DGENUTIL_INT_BIT=16 -DGENUTIL_FLT_EXC"]="16"
test_ints["-DSTRHASH_INT_BIT=32 -DGENUTIL_INT_BIT=32 -DGENUTIL_FLT_EXC"]="32"
test_ints["-DSTRHASH_INT_BIT=64 -DGENUTIL_INT_BIT=64 -DGENUTIL_FLT_EXC"]="64"

# Clear the existing compilation and execution plans.
> make_plan
> test_plan

# Create the compilation and execution plans.
for test_opt in "${!test_opts[@]}"; do
  for test_int in "${!test_ints[@]}"; do
    # Extract the identifiers.
    id_path="./bin/id_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}"
    eq_path="./bin/eq_o${test_opts[$test_opt]}_i${test_ints[${test_int}]}"
      
    # Note the compilation and execution plans.
    echo ${cc} ${test_int} ${flag} ${test_opt} ${inc} ${lib} id.c -o ${id_path} >> ./make_plan
    echo ${cc} ${test_int} ${flag} ${test_opt} ${inc} ${lib} eq.c -o ${eq_path} >> ./make_plan
    echo ${id_path} >> ./test_plan
    echo ${eq_path} >> ./test_plan
    done
done

# Execute the compilation and execution of identity property tests in parallel.
parallel --tmpdir ./tmp/ -v         < ./make_plan
parallel --tmpdir ./tmp/ --joblog - < ./test_plan
