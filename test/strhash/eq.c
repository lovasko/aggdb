#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "strhash/strhash.h"
#include "genutil/rng.h"


// Select a random nothing-up-my-sleeve seed.
#ifndef TEST_SEED
  #define TEST_SEED 23456
#endif

// Repetition count.
#ifndef TEST_REP
  #define TEST_REP 1000000
#endif


/// Execute a single test where the equality of two encodings is tested.
static bool run_test(
  struct genutil_rng* rng) // Random number generator.
{
  int         cmp;                     // Comparison result.
  STRHASH_INT val[2];                  // Integer representation.
  char        str[2][STRHASH_LEN + 1]; // String representation.

  // Generate two random integers.
  val[0] = genutil_rng_any(rng);
  val[1] = genutil_rng_any(rng);

  // Encode the two integers.
  (void)strhash_get(str[0], STRHASH_LEN, val[0]);
  (void)strhash_get(str[1], STRHASH_LEN, val[1]);

  // Verify that the two strings match if the two integers match.
  cmp = strncmp(str[0], str[1], STRHASH_LEN);
  if (cmp == 0 && val[0] != val[1]) {
    (void)printf("mismatch: fst=%" STRHASH_INT_FMT "/%.*s snd=%" STRHASH_INT_FMT "/%.*s\n",
      val[0], STRHASH_LEN, str[0],
      val[1], STRHASH_LEN, str[1]);
    return false;
  }

  return true;
}

int main(void)
{
  struct genutil_rng rng; // Random number generator.
  uintmax_t          rep; // Repetition counter.
  bool               ret; // Return value.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    // Execute a single test.
    ret = run_test(&rng);
    if (ret == false) {
      break;
    }

    // Advance to the next test invocation.
    rep += 1;
  }

  // Determine the process exit code.
  if (ret == false) {
    return EXIT_FAILURE;
  } else {
    return EXIT_SUCCESS;
  }
}
