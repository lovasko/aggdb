// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "mphfunc/mphfunc.h"
#include "bittape/bittape.h"
#include "zipints/zipints.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_EXT
  #error "must set TEST_EXT to specify the maximal length extension"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Execute a single instance of the test.
static bool run_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  MPHFUNC_INT        *               arr, // Values array.
  struct bittape     *const restrict bit) // Presence set.
{
  struct zipints zip; // Compressed integers.
  struct mphfunc mph; // Minimal perfect hash function.
  MPHFUNC_INT    idx; // Array index.
  MPHFUNC_INT    len; // Test length.
  MPHFUNC_INT    ext; // Length extension.
  MPHFUNC_INT    val; // Inserted value.
  MPHFUNC_INT    pop; // Population count.
  MPHFUNC_INT    cnt; // Bucket count.
  bool           ret; // Return value.

  // Reduce the test length at random.
  len = genutil_rng_int(rng, 1, TEST_LEN);
  ext = genutil_rng_int(rng, 0, TEST_EXT);

  // Generate an array of random unique integers. We achieve this by 
  idx = 0;
  len = 0;
  while (idx < TEST_LEN) {
    val = genutil_rng_int(rng, 0, 1);
    if (val == 1) {
      arr[len] = idx;
      len     += 1;
    }

    idx += 1;
  }

  printf("generated random array len=%" PRIuMAX "\n", (uintmax_t)len);

  // Sort and compress the array.
  ret = zipints_put(&zip, arr, len);
  if (ret == false) {
    printf("error: unable to compress the array\n");
    return false;
  }

  // Determine the right bucket count based on the test length.
  if (len > 1000000) {
    printf("unsupported test length\n");
    return false;
  }

  if (len < 1000000) {
    cnt = 65000;
  }

  if (len < 100000) {
    cnt = 6500;
  }

  if (len < 10000) {
    cnt = 650;
  }
  
  if (len < 1000) {
    cnt = 65;
  }

  if (len < 100) {
    cnt = 6;
  }

  // Build the minimal perfect hash function out of the compressed array.
  ret = mphfunc_new(&mph, cnt, ext, &zip);
  if (ret == false) {
    printf("error: unable to build the function\n");
    return false;
  }

  // Traverse the original array and verify that each hashed value is unique
  // and within the limits.
  bittape_nul(bit);
  idx = 0;
  while (idx < len) {
    mphfunc_idx(&mph, arr[idx], &val);
    if (val > len + ext) {
      printf("error: failed retrieval val=%" PRIuMAX "at idx=%" PRIuMAX "\n",
        (uintmax_t)arr[idx], (uintmax_t)idx);
      return false;
    }
    bittape_set1(bit, val);
    idx += 1;
  }

  // Verify that the presence set contains the same number of bits as the
  // number of integers in the array.
  bittape_pop(bit, &pop);
  if (pop != len) {
    printf("error: collision occurred, pop=%" PRIuMAX "\n", (uintmax_t)pop);
    return false;
  }

  // Release resources needed for the test.
  zipints_del(&zip);
  mphfunc_del(&mph);
  return true;
}

int main(void)
{
  MPHFUNC_INT*       arr; // Values array.
  struct genutil_rng rng; // Random number generator.
  struct bittape     bit; // Presence set.
  MPHFUNC_INT        rep; // Repetition counter.
  bool               ret; // Return value.

  // Allocate memory for the values.
  arr = calloc(TEST_LEN, sizeof(MPHFUNC_INT));
  if (arr == NULL) {
    return EXIT_FAILURE;
  }

  // Allocate memory for the presence set.
  ret = bittape_new(&bit, TEST_LEN + TEST_EXT);
  if (ret == false) {
    return EXIT_FAILURE;
  }

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Repeatedly execute the test.
  ret = true;
  rep = 0;
  while (rep < TEST_REP) {
    ret = run_test(&rng, arr, &bit);
    if (ret == false) {
      break;
    }

    rep += 1;
  }

  // Determine the exit value based on success/failure.
  if (ret == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
