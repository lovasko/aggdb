#!/bin/bash
# Copyright (c) 2022 Daniel Lovasko
# All rights reserved. Proprietary and confidential.

# Ensure that any failure fails the whole testing process.
set -e
set -x

# Compilation settings.
cc="gcc"
flag="-std=c99 -Wall -Wextra -Wno-maybe-uninitialized -Werror -D_POSIX_C_SOURCE=202211 -D_XOPEN_SOURCE -DTEST_SEED=210 -DTEST_REP=4"
inc="-I../ -I../../src"
lib="../../src/mphfunc/mphfunc.c ../../src/bittape/bittape.c ../../src/zipints/zipints.c ../../src/bitutil/bitutil.c ../../src/clkutil/clkutil.c ../../src/genutil/rng.c"

# Test input lengths.
declare -A test_lens 
test_lens["-DTEST_LEN=10"]="1"
test_lens["-DTEST_LEN=100"]="2"
test_lens["-DTEST_LEN=1000"]="3"
test_lens["-DTEST_LEN=10000"]="4"
test_lens["-DTEST_LEN=100000"]="5"
test_lens["-DTEST_LEN=1000000"]="6"

# Length extensions.
declare -A test_exts
test_exts["-DTEST_EXT=0"]="0"
test_exts["-DTEST_EXT=10"]="1"
test_exts["-DTEST_EXT=100"]="2"
test_exts["-DTEST_EXT=1000"]="3"

# Compiler optimisation levels.
declare -A test_opts
test_opts["-g"]="g"
test_opts["-O0"]="0"
test_opts["-O3"]="3"
test_opts["-Ofast -march=native -mtune=native -flto -fomit-frame-pointer"]="p"

# Bit widths.
declare -A test_ints
test_ints["-DMPHFUNC_INT_BIT=32 -DZIPINTS_INT_BIT=32 -DBITTAPE_INT_BIT=32 -DBITUTIL_INT_BIT=32 -DGENUTIL_INT_BIT=32 -DGENUTIL_FLT_EXC"]="32"
test_ints["-DMPHFUNC_INT_BIT=64 -DZIPINTS_INT_BIT=64 -DBITTAPE_INT_BIT=64 -DBITUTIL_INT_BIT=64 -DGENUTIL_INT_BIT=64 -DGENUTIL_FLT_EXC"]="64"

# Hardware acceleration settings.
declare -A test_accs
test_accs[" "]="0"
test_accs["-DBITUTIL_ACC"]="1"

# Clear the existing compilation and execution plans.
> ./bin/make_plan
> ./bin/test_plan

# Create the compilation and execution plans.
for test_opt in "${!test_opts[@]}"; do
  for test_acc in "${!test_accs[@]}"; do
    for test_len in "${!test_lens[@]}"; do
      for test_ext in "${!test_exts[@]}"; do
        for test_int in "${!test_ints[@]}"; do
          # Skip cases where the integer width is not sufficient.
          if [ ${test_ints[${test_int}]} -eq "16" ] && [ ${test_lens[${test_lena}]} -ge "5" ]; then
            continue
          fi

          # Extract the identifiers.
          idx_path="./bin/idx_l${test_lens[$test_len]}_e${test_exts[$test_ext]}_o${test_opts[$test_opt]}_a${test_accs[${test_acc}]}_i${test_ints[${test_int}]}"
          ext_path="./bin/ext_l${test_lens[$test_len]}_o${test_opts[$test_opt]}_a${test_accs[${test_acc}]}_i${test_ints[${test_int}]}"
      
          # Note the compilation and execution plans.
          echo ${cc} ${test_len} ${test_ext} ${test_int} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} idx.c -o ${idx_path} >> ./bin/make_plan
          echo ${cc} ${test_len} ${test_ext} ${test_int} ${flag} ${test_opt} ${test_acc} ${inc} ${lib} ext.c -o ${ext_path} >> ./bin/make_plan

          # Ignore the debugging builds.
          if [ ${test_opt} != "-g" ]
          then
            echo ${idx_path} >> ./bin/test_plan
            echo ${ext_path} >> ./bin/test_plan
          fi
        done
      done
    done
  done
done

# Execute the compilation and execution of identity property tests in parallel.
sort -u ./bin/make_plan | cat  | parallel --tmpdir ./tmp/ -v
sort -u ./bin/test_plan | shuf | parallel --tmpdir ./tmp/ --joblog -
