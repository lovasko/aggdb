// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "mphfunc/mphfunc.h"
#include "bittape/bittape.h"
#include "zipints/zipints.h"
#include "clkutil/clkutil.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_EXT
  #error "must set TEST_EXT to specify the maximal length extension"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Determine whether a number is a prime.
static bool test_prm(
  const MPHFUNC_INT inp) // Input value.
{
  MPHFUNC_INT val;

  if (inp < 2) {
    return true;
  }

  val = 2;
  while (val != inp / 2) {
    if (inp % val == 0) {
      return false;
    }
    val += 1;
  }

  return true;
}

/// Find the next greater prime.
static MPHFUNC_INT next_prm(
  const MPHFUNC_INT inp) // Input value.
{
  MPHFUNC_INT val; // Progressive value.
  bool        ret; // Return value.

  // Continuously 
  val = inp;
  while (true) {
    val += 1;
    ret = test_prm(val);
    if (ret == true) {
      break;
    }
  }

  return val;
}

/// Execute a single instance of the test.
static bool run_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  struct clkutil     *const restrict ock, // Original clock.
  struct clkutil     *const restrict eck, // Extended clock.
  MPHFUNC_INT        *               arr) // Values array.
{
  bool           ret;
  MPHFUNC_INT    idx;
  MPHFUNC_INT    len;
  MPHFUNC_INT    ext;
  MPHFUNC_INT    cnt;
  MPHFUNC_INT    val;
  struct zipints zip; // Compressed unsigned integers.
  struct mphfunc omf;
  struct mphfunc emf; // Extended minimal perfect hash function.

  // Generate an array of random unique integers. We achieve this by iterating through all numbers
  // in ascending order and randomly including or excluding them from the set.
  idx = 0;
  len = 0;
  while (idx < TEST_LEN) {
    val = genutil_rng_int(rng, 0, 1);
    if (val == 1) {
      arr[len] = idx;
      len     += 1;
    }
    idx += 1;
  }

  // Early exit from the test in case the generated length is a prime.
  ret = test_prm(len);
  if (ret == true) {
    return false;
  }

  // Compute the next greatest prime.
  ext = next_prm(len);

  // Compress the array.
  ret = zipints_put(&zip, arr, len);
  if (ret == false) {
    printf("error: unable to compress the array\n");
    return false;
  }

  // Determine the right bucket count based on the test length.
  if (len > 1000000) {
    printf("unsupported test length\n");
    return false;
  }

  if (len < 1000000) {
    cnt = 95000;
  }

  if (len < 100000) {
    cnt = 9500;
  }

  if (len < 10000) {
    cnt = 650;
  }
  
  if (len < 1000) {
    cnt = 65;
  }

  if (len < 100) {
    cnt = 6;
  }

  // Time the execution of the original length.
  clkutil_put(ock); 
  ret = mphfunc_new(&omf, len, cnt, &zip);
  if (ret == false) {
    printf("unable to find a function\n");
    exit(EXIT_FAILURE);
  }
  clkutil_put(ock);
  mphfunc_del(&omf);

  // Time the execution of the extended length.
  clkutil_put(eck);
  ret = mphfunc_new(&emf, ext, cnt, &zip);
  if (ret == false) {
    printf("unable to find a function\n");
    exit(EXIT_FAILURE);
  }
  clkutil_put(eck);
  mphfunc_del(&emf);

  return true;
}

int main(void)
{
  MPHFUNC_INT*       arr; // Values array.
  struct genutil_rng rng; // Random number generator.
  struct clkutil     ock; // Original clock.
  struct clkutil     eck; // Extended clock.
  uintmax_t          min; // Minimal run-time.
  uintmax_t          max; // Maximal run-time.
  uintmax_t          avg; // Average run-time.
  MPHFUNC_INT        rep; // Repetition counter.
  bool               ret; // Return value.

  // Allocate memory for the values.
  arr = calloc(TEST_LEN, sizeof(MPHFUNC_INT));
  if (arr == NULL) {
    return EXIT_FAILURE;
  }

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Initialise the original and extended clocks.
  clkutil_new(&ock, 0);
  clkutil_new(&eck, 0);

  // Repeatedly execute the test.
  rep = 0;
  while (rep < TEST_REP) {
    ret = run_test(&rng, &ock, &eck, arr);

    // Repeat the test if the length was a prime.
    if (ret == false) {
      continue;
    }
    rep += 1;
  }

  // Release values memory.
  free(arr);

  // Report the original clock measurements.
  clkutil_get(&ock, &min, &max, &avg);
  (void)printf("extended min=%" PRIuMAX " max=%" PRIuMAX " avg=%" PRIuMAX "\n", min, max, avg);
  
  // Report the extended clock measurements.
  clkutil_get(&eck, &min, &max, &avg);
  (void)printf("extended min=%" PRIuMAX " max=%" PRIuMAX " avg=%" PRIuMAX "\n", min, max, avg);
 
  return EXIT_SUCCESS;
}
