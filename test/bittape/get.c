// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "bittape/bittape.h"
#include "bitutil/bitutil.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Execute a single instance of the test.
static bool run_test(
  struct genutil_rng* rng) // Random number generator.
{
  struct bittape bit; // Bit tape.
  struct bithead pos; // Reading and writing heads.
  BITTAPE_INT    val; // Random bits.
  BITTAPE_INT    len; // Test length.
  BITTAPE_INT    cnt; // Bit count.
  BITTAPE_INT    fun; // Selected function.
  uintmax_t      exp; // Expected population count.
  uintmax_t      act; // Actual population count.
  uintmax_t      idx; // Array index.
  bool           ret; // Return value.

  // Reduce the test size and prepare the bit tape.
  len = genutil_rng_int(rng, 1, TEST_LEN);
  ret = bittape_new(&bit, len);
  if (ret == false) {
    (void)printf("unable to perform `new`\n");
    return false;
  }
  bithead_nul(&pos);

  // Insert each value to the bit tape.
  exp = 0;
  idx = 0;
  while (idx < len) {
    // Generate a random bit.
    val = genutil_rng_int(rng, 0, 1);
    if (val == 1) {
      exp += 1;
    }

    // Append the bit to the tape and advance to the next bit.
    ret = bittape_put(&bit, &pos, 1, val);
    if (ret == false) {
      (void)printf("unable to perform `put`\n");
      return false;
    }

    // Advance to the next bit.
    idx += 1;
  }

  // Read the bit tape in random-sized chunks and compute the population count of each.
  bithead_nul(&pos);
  act = 0;
  idx = 0;
  while (idx < len) {
    // Determine the function at random.
    fun = genutil_rng_int(rng, 0, 2);

    // Read the selected number of bits and count the set bits. Once the reading head approaches the
    // end of the bit tape, all remaining bits are processed.
    if (fun == 0) {
      if (idx + BITTAPE_INT_BIT > len) {
        cnt = len - idx;
      } else {
        cnt = genutil_rng_int(rng, 1, BITTAPE_INT_BIT);
      }
      ret = bittape_get(&bit, &pos, cnt, &val);
      if (ret == false) {
        (void)printf("unable to perform `get`\n");
        return false;
      }
      act += bitutil_pop(val);
    }

    if (fun == 1) {
      ret = bittape_get0(&bit, &pos, &cnt);
      if (ret == false) {
        (void)printf("unable to perform `get0`\n");
        return false;
      }
    }

    if (fun == 2) {
      ret = bittape_get1(&bit, &pos, &cnt);
      if (ret == false) {
        (void)printf("unable to perform `get1`\n");
        return false;
      }
      act += cnt;
    }
    
    // Record the reading movement.
    idx += cnt;
  }
  
  // Free up the resources.
  bittape_del(&bit);

  // Compare the actual and expected population counts.
  if (exp != act) {
    (void)printf("mismatch: exp=%" PRIuMAX " act=%" PRIuMAX "\n", exp, act);
    return false;
  } else {
    return true;
  }
}

/// Execute a test where a random bit tape is generated, computing its population count.  The `get`
/// operation is used to traverse all the bits and compute the population count again. The two
/// computations are expected to yield the same results.
int main(void)
{
  struct genutil_rng rng; // Random number generator.
  uintmax_t          rep; // Repetition counter.
  bool               res; // Return value.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    res = run_test(&rng);
    if (res == false) {
      break;
    }
    rep += 1;
  }

  // Determine the exit code based on the test results.
  if (res == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
