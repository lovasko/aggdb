// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "bittape/bittape.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Find the index of the n-th unset value within an array.
static BITTAPE_INT find_idx(
        bool*       arr, // Array.
  const BITTAPE_INT len, // Array length.
  const BITTAPE_INT nth, // Position of the value.
  const bool        val) // Value to search for.
{
  BITTAPE_INT idx; // Array index.
  BITTAPE_INT cnt; // Repetition count.

  // Traverse the array and count the expected objects.
  idx = 0;
  cnt = 0;
  while (idx < len) {
    // Extract an element and compare it.
    if (arr[idx] == val) {
      cnt += 1;
    }

    // Terminate the search upon reaching the expected count.
    if (cnt == nth + 1) {
      return idx;
    }

    // Advance to the next element.
    idx += 1;
  }

  // Should not be reached. Off by one from the unreachable constant in the library itself to
  // prevent matching constants.
  return BITTAPE_INT_MAX - 1;
}

/// Execute a single instance of the test.
/// TODO: consider comparing return values to check where the n-th set/unset bit is not present
static bool run_test(
  struct genutil_rng* rng, // Random number generator.
  bool*               arr) // Values array.
{
  struct bittape bit;    // Bit tape.
  struct bithead pos;    // Reading and writing heads.
  BITTAPE_INT    len;    // Test length.
  BITTAPE_INT    cnt[2]; // Set and unset bit counts.
  BITTAPE_INT    val;    // Random value.
  BITTAPE_INT    rbp;    // Position within the boolean array.
  BITTAPE_INT    exp;    // Expected index.
  BITTAPE_INT    act;    // Actual index.
  BITTAPE_INT    idx;    // Array index.
  bool           ret;    // Return value.

  // Clear the common values array.
  idx = 0;
  while (idx < TEST_LEN) {
    arr[idx] = false;
    idx += 1;
  }

  // Reduce the test length. 
  len = genutil_rng_int(rng, 1, TEST_LEN);

  // Create the bit tape.
  ret = bittape_new(&bit, len);
  if (ret == false) {
    (void)printf("unable to create the bit tape\n");
    return false;
  }
  bithead_nul(&pos);
  
  // Generate random array and count the number of set and unset bits.
  cnt[0] = 0;
  cnt[1] = 0;
  idx    = 0;
  while (idx < len) {
    val = genutil_rng_int(rng, 0, 1);
    bittape_put(&bit, &pos, 1, val);
    arr[idx]  = (bool)val;
    cnt[val] += 1;
    idx      += 1;
  }

  // Execute the test for unset bits.
  if (cnt[0] > 0) {
    // Generate a random bit position to search for.
    rbp = genutil_rng_int(rng, 0, cnt[0] - 1);

    // Find the n-th unset bit within the bit tape.
    ret = bittape_sel0(&bit, rbp, &act);
    if (ret == false) {
      (void)printf("unable to perform `sel0`\n");
      return false;
    }

    // Find the n-th unset bit within the boolean array.
    exp = find_idx(arr, len, rbp, false);

    // Compare the actual and expected indices. 
    if (act != exp) {
      (void)printf("mismatch for unset bits: exp=%" PRIuMAX " act=%" PRIuMAX "\n",
        (uintmax_t)exp, (uintmax_t)act);
      return false;
    }
  }

  // Execute the test for set bits.
  if (cnt[1] > 0) {
    // Generate a random bit position to search for.
    rbp = genutil_rng_int(rng, 0, cnt[1] - 1);

    // Find the n-th set bit within the bit tape.
    ret = bittape_sel1(&bit, rbp, &act);
    if (ret == false) {
      (void)printf("unable to perform `sel1`\n");
      return false;
    }

    // Find the n-th set bit within the boolean array.
    exp = find_idx(arr, len, rbp, true);

    // Compare the actual and expected indices. 
    if (act != exp) {
      (void)printf("mismatch for set bits: exp=%" PRIuMAX " act=%" PRIuMAX "\n",
        (uintmax_t)exp, (uintmax_t)act);
      return false;
    }
  }

  // Release all resources held by the bit tape.
  bittape_del(&bit);
  return true;
}

int main(void)
{
  bool*              arr; // Common values array.
  struct genutil_rng rng; // Random number generator.
  uintmax_t          rep; // Repetition counter.
  bool               res; // Return value.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Allocate common memory for all tests.
  arr = calloc(sizeof(bool), TEST_LEN);
  if (arr == NULL) {
    (void)printf("unable to allocate common memory\n");
    return EXIT_FAILURE;
  }

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    res = run_test(&rng, arr);
    if (res == false) {
      break;
    }
    rep += 1;
  }

  // Release the common memory.
  free(arr);

  // Determine the exit code based on the test results.
  if (res == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
