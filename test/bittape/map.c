// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "bittape/bittape.h"
#include "bitutil/bitutil.h"
#include "genutil/rng.h"


#ifndef TEST_LEN
  #error "must set TEST_LEN to specify the maximal test length"
#endif

#ifndef TEST_REP
  #error "must set TEST_REP to specify the test repetition count"
#endif

#ifndef TEST_SEED
  #error "must set TEST_SEED to specify the random number generator seed"
#endif

/// Count the number of set bits.
static bool pop_inc(
  BITTAPE_INT     idx, // Index (unused).
  BITTAPE_INT     val, // Bit value.
  void*           ctx) // Population count.
{
  // Explicitly ignore some of the arguments.
  (void)idx;

  // Increment the population count if the bit is set.
  if (val == 1) {
    (*(uintmax_t*)ctx) += 1;
  }

  // Always continue with the traversal.
  return true; 
}

static bool new_test(
  struct genutil_rng *const restrict rng, // Random number generator.
  struct bittape     *const restrict bit, // Bit tape.
  BITTAPE_INT        *const restrict len, // Reduced test length.
  BITTAPE_INT        *const restrict exp) // Expected number of set bits.
{
  struct bithead pos; // Reading and writing heads.
  BITTAPE_INT idx;
  BITTAPE_INT fun;
  BITTAPE_INT max;
  BITTAPE_INT msk;
  BITTAPE_INT val;
  BITTAPE_INT cnt;
  bool        ret;

  // Reduce the test size.
  *len = genutil_rng_int(rng, 1, TEST_LEN);
  ret = bittape_new(bit, *len);
  if (ret == false) {
    (void)printf("unable to perform `new`\n");
    return false;
  }
  *exp = 0;
  bithead_nul(&pos);

  idx = 0;
  while (idx < *len) {
    // Select one of the three functions randomly, unless the appending head is at the tail of the
    // array. In this case the last word is filled with random bits.
    if (idx + BITTAPE_INT_BIT > *len) {
      fun = 0;
      max = *len - idx;
    } else {
      fun = genutil_rng_int(rng, 0, 2);
      max = BITTAPE_INT_BIT;
    }

    // Append a random set of bits (up to BITTAPE_INT_BIT bits).
    if (fun == 0) {
      val = genutil_rng_any(rng);
      cnt = genutil_rng_int(rng, 1, max);
      ret = bittape_put(bit, &pos, cnt, val);
      if (ret == false) {
        (void)printf("unable to perform `put`\n");
        return false;
      }

      // Update the overall number of set bits.
      if (cnt == BITTAPE_INT_BIT) {
        msk = BITTAPE_INT_MAX;
      } else {
        msk = ((BITTAPE_INT)1 << cnt) - 1;
      }
      *exp += bitutil_pop(val & msk);

      // Advance the appending pointer.
      idx += cnt;
    }

    // Append a sequence of unset bits.
    if (fun == 1) {
      cnt = genutil_rng_int(rng, 1, (*len) - idx - 1);
      ret = bittape_put0(bit, &pos, cnt);
      if (ret == false) {
        (void)printf("unable to perform `put0`\n");
        return false;
      }

      // Advance the appending pointer.
      idx += cnt;
    }

    // Append a sequence of set bits.
    if (fun == 2) {
      cnt = genutil_rng_int(rng, 1, (*len) - idx - 1);
      ret = bittape_put1(bit, &pos, cnt);
      if (ret == false) {
        (void)printf("unable to perform `put1`\n");
        return false;
      }
      *exp += cnt;

      // Advance the appending pointer.
      idx += cnt;
    }
  }

  return true;
}

/// Execute a single instance of the test.
static bool run_test(
  struct genutil_rng* rng) // Random number generator.
{
  struct bittape bit; // Bit tape.
  BITTAPE_INT    len; // Test length.
  BITTAPE_INT    exp; // Expected population count.
  BITTAPE_INT    act; // Actual population count.
  bool           ret; // Return value.

  // Create a new test case.
  ret = new_test(rng, &bit, &len, &exp);
  if (ret == false) {
    return false;
  }

  // Compute the actual population count using the `map` operation.
  act = 0;
  bittape_map(&bit, pop_inc, &act);

  // Free up the resources.
  bittape_del(&bit);

  // Compare the actual and expected population counts.
  if (exp != act) {
    (void)printf("mismatch: exp=%" PRIuMAX " act=%" PRIuMAX "\n",
      (uintmax_t)exp, (uintmax_t)act);
    return false;
  } else {
    return true;
  }
}

/// Execute a test where a random bit tape is generated, computing its population count.  The `map`
/// operation is used to traverse all the bits and compute the population count again. The two
/// computations are expected to yield the same results.
int main(void)
{
  struct genutil_rng rng; // Random number generator.
  uintmax_t          rep; // Repetition counter.
  bool               res; // Return value.

  // Initialise the random number generator.
  genutil_rng_new(&rng, TEST_SEED);

  // Repeatedly execute a random test.
  rep = 0;
  while (rep < TEST_REP) {
    res = run_test(&rng);
    if (res == false) {
      break;
    }
    rep += 1;
  }

  // Determine the exit code based on the test results.
  if (res == true) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}
