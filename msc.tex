\documentclass[dissertation]{softeng}

\title{Design and Implementation of a Numerical Aggregate Streaming Database System}
\author{}
\college{Kellogg College}
\organisation{University of Oxford}
\award{Software and Systems Engineering}

\begin{document}
\maketitle

\begin{abstract}
Database is a software system that organises and stores data whilst facilitating a wide variety of
operations on top of the data. These systems take different shapes in terms of used data structures
and provided interface, based on the type of data they interact with. One of which is a numerical
aggregate database, which is intended to process streams of incoming numerical values provided the
aggregation criteria.

Complex systems of all sizes, from the world finance to weather, or even the computer that produced
this dissertation present a vast array of observable quantities. Recording these observations offers
a unique insight into the behaviour of these systems, allowing for investigative approach to
studying events such as outages, and provide base data to forecast future behaviour.

The available computer hardware presents a practical limitation to the amount of data that is
collected and analysed. This can be counteracted by selecting an appropriate architecture or
technology to address the problem definition. Examples of bottlenecks include computational
performance, latency and throughput, or memory pressure.

This dissertation presents the design and its accompanying implementation of a numerical aggregate
database system that has a succinct memory footprint whilst maintaining a reasonable
performance. This is achieved by careful selection of data structures and algorithms, and crafting
of their implementation using programming techniques such as hardware acceleration through
specialised instructions and CPU-aware data alignment, and distribution of the numerical workload to
facilitate concurrent execution.
\end{abstract}

\clearpage

\begin{acknowledgements}
  Thanks.  
\end{acknowledgements}

\clearpage

\pagenumbering{roman}
\pagestyle{plain}
\setcounter{tocdepth}{2}

\tableofcontents

\clearpage

\pagenumbering{arabic}
\pagestyle{myheadings}

\chapter{Introduction}
This chapter contains on the general overview and context of the dissertation work. It describes the
motivation behind the work, mostly derived from anecdotal professional experience and general
industry trends and challenges. Furthermore, it clearly states the set objectives and their
constituent goals. The chapter is concluded with a summary structure of the remaining chapters of
the thesis, offering a high-level content of each one. 

\section{Motivation}
Sensors and monitoring systems are able to generate great amounts of data in a very short timespan.
This data is seldom analysed by humans in the raw form, and instead one or more layers of analytic
transformations is performed on the data to capture important features of the data.

\subsection{Edge Computing}
Transporting the large data sets through a network is expensive, both in terms of transmission
bandwidth and the hardware required. Instead, the analytic layer of computation runs on the edge of
the network, only sending the critical insights. Data collection and processing at the edge is a
wider trend in the industry. Recent devices such as the Google Coral Edge TPU or the NVIDIA Jetson
line of single-board computer hardware offers performing neural network inference and parallel
processing with low energy requirements, albeit providing only a limited set of resources.

\subsection{Monolithic Improvement}
The shape and frequency of the incoming data that requires analysis defines the computational
hardware requirements. Certain requirements call for multiple computers working together, so that
the performance or memory bottleneck is resolved. But the complexity of distributed systems can at
times outweigh the benefits they unlock, especially in terms of resource availability. With this in
mind, extending the capabilities of monolithic single-computer systems to enable wider array of
applications yields one of the possible solutions to the problem - which is the focus of this
dissertation. Other solutions might include improving and democratising the network capabilities or
addressing the maintainability of large systems.

\subsection{Memory Footprint}
Instead of focusing on the computational resource provided by the processing unit, or a similar
specialised alternative, we chose the memory footprint bottleneck as the target. It is clear that
this approach is not a universal solution, but instead addresses a subset of problematic scenarios,
where the latency and throughput of the system do not create an obstacle to practical use.

\subsection{Aggregate Database}
The streaming aggregate database model performs such analytic function by applying aggregations on
the generated data, thus producing values that represent features of the data sets. Aggregate
databases can be deployed in multitude of scenarios. From my anecdotal professional experience in
the role of a reliability engineer, these databases serve a critical role in system troubleshooting,
as well as monitoring.

\section{Objectives}
Databases that solve the problem of numerical aggregation might focus on various aspects of the
database system surface, including performance, user experience, wide software ecosystem integration
or numerical precision. Instead, the focus of this thesis is an aggregate database that aims to
minimise its memory footprint as its guiding design goal.

\subsection{Memory Footprint}
Software engineering practice tends to yield design crossroads, where choice trade-offs are
presented, each providing its unique pros and cons. The memory footprint question is at a forefront
of these decisions, where reduction of the RAM memory footprint is a paramount goal of the design
and implementation throughout the dissertation, often at a cost to other qualities and properties.

\subsection{Performance Offset}
The memory reduction techniques applied to the database come at a cost of various other qualities of
the database, such as numerical precision and overall operational performance. All choices towards
the memory footprint reduction must still render the database practically usable for a reasonable
input latency and throughput. As this value is relative, the evaluation is performed on a reference
hardware to provide a report on the achieved performance. The dissertation 

\subsection{Precision Guarantee}
Similar to performance, the precision of the numerical computations used in the database system might
be reduced as part of a memory usage optimisation. The precision of the aggregate functions in
particular presents a critical user experience challenge, as it should meet some minimal criteria in
order to remain useful. The achieved precision degradation is monitored as a measurement of
error rate for a particular synthetic input and the numerical value type used, defined by the range
of values and the method of their generation. Given the absolute nature of the measurement, a
well-known scientific computation package is chosen as a reference.

\subsection{Implementation Quality}
Throughout ensuring the set goals of resource usage and data throughput, it is important that
correctness of the numerical processing and data manipulation is ensured. This can be achieved
through appropriate decomposition of the problem surface, systemic use of the afforded type system,
and, most importantly, through a set of software tests that verify the behaviour.

\section{Structure}
The remainder of the dissertation is structured as follows:

\paragraph{Chapter 2: Background} describes various algorithms, data structures, techniques, and
properties that are essential throughout the implementation phase. The sections are structured
around a single computing problem each, defining the problem and discussing solutions. The solutions
tend to explore the simplest naive solution, as well as other solutions in the space if relevant
options exist. \\

\noindent The central problem of numerical data aggregation is dissected in two separate categories:
one focusing on statistical moments, such as variance and kurtosis, and the other is the quantile
problem, with its special case of median value. \\

\noindent Other problems within the database system involve the object management, their
representation and organisation so that the appropriate structures can be updated. This is addressed
throughout sections on compression of a sequence of unsigned integers.

\paragraph{Chapter 3: Design} introduces the theoretical workings and object organization of the
database system that processes incoming numerical data and facilitates their retrieval through the
use of a programmable application interface. It introduces the concepts of metrics and views, along
with aggregate functions and the time intervals of their application. \\

\noindent This design is intended to act as a blueprint for further implementation, and does not try
to cover all potential features or user experience shortcuts; instead aims at declaring the core
practical subset of functionality that is required for a functioning database. \\

\noindent The versatility of the design is then exposed through a small number of different usage
scenarios, which describe the context where the database system's deployment comes in handy,
especially given the stated objectives of memory footprint reduction.

\paragraph{Chapter 4: Implementation} provides details about the practical software implementation
based on the presented design. The chapter starts with the overall approach to the implementation of
the database based on the introduced design.  Taking a top-down direction, the first sections of
this chapter focus on work distribution, using the decomposition strategy as a way to manage the
complexity of the development. \\

\noindent Upon the partition of the problem space, each section descends into detail of the major
software libraries that implement an important part of the final database. This includes a library
that performs aggregation on top of numerical sequences, compression of metric identifiers, or an
efficient space-conscious lookup mechanism. Each of these sections further explores the trade-offs
in areas of performance and storage, describing the engineering techniques used to achieve the set
goals, and any drawbacks that exist as a result. \\

\noindent As the mentioned libraries only address the pure computational side of the database, the
remaining sections introduce input/output challenges, such as the interface of the database and its
integration with the event subsystem. \\

\noindent Finally, the outer shell of the programming is address, the testing strategy that ensures
that the properties of the software implementation are correct. Throughout the sections, the
property testing strategy is explained, including the random input generation and resolving the
challenges related to comprehensive coverage.

\paragraph{Chapter 5: Evaluation} explores the finished implementation by running it against
generated time series data and observing the behaviour of the database in terms of the stated goals
of a succinct memory footprint. Additionally, other properties such as the number of memory page
faults, processor usage, and file descriptor usage are discussed. \\

\noindent The evaluation is executed on a reference hardware such that the designed and implemented
database system can be anchored in a practical real-world setting. This also allows to provide a
monetary reference to the actual cost of running the database, further exploring the engineering
trade-offs at the core of the design. Full description of the hardware used for evaluation is
provided, along with the software stack, including the operating system and its interfaces, as well
as the standard library.

\chapter{Background}
Throughout the design and implementation of the aggregate database, a varied set of algorithms, data
structures, and data representations are required to organise and process the numerical values and
auxiliary structures. The following sections describe some of the methods most critical to achieving
the set design goals of a succinct memory footprint. Such listing is not conclusive, as various
technological methods are used to solve practical problems within the implementation.

\section{Floating-Point Arithmetic}
Computer systems need to represent 

\subsection{IEEE 754}
The IEEE 754 format is the de-facto standard for representing real numbers in computer systems. This
is evidenced by its direct hardware support in all desktop and server-grade processors. In addition
to that, most mainstream languages provide a built-in type to facilitate the use of the standard. \\

\noindent 

\subsection{Standard Formats}
The IEEE 754 standard defines a set of formats, each 

\subsubsection{Limitations}
Precision and Range

\subsubsection{Special Values}
NaN, infinity

\subsection{Language Support}
The C language, from its inception and throughout all subsequent standards, 

\section{Statistical Moment Estimation}
Some of the simple aggregate functions rely on a trivial implementation that does not involve any .
In opposition, aggregate functions such as arithmetical mean, variance (and the derived standard
deviation), skewness and kurtosis do require more involved computation based on their mathematical
definitions.

The Welford’s method [] introduces a new way of performing the required computation that does not require storage of all observations, whilst

\subsection{}
\subsection{}

\section{Quantile Estimation}
\subsection{Problem Statement}
Quantile is a summarising value for a set of values that partitions the set into values that are
greater and smaller than the quantile value. The partition is parametrised by a single argument,
that represents the percentage of values that are less than the quantile value. \\

\noindent Observing the quantile of a set of values provides crucial insight in areas of quality
control and identification of outliers. For example, the 0.9-quantile of duration of network
requests uncovers the limit at which at 90\% of all network requests

\subsection{Naive Solution}
The conceptually simple algorithm involves

\noindeint analysis of the naive solution and its drawbacks

\subsection{Sample Sort Algorithm}
 * talk about how it reduces the memory footprint at the cost of precision
 * highlight that there is still a connection to the number of values in the series
 * the variance of possible results (and the )

\subsection{T-digest Algorithm}
 * 

\subsection{Piecewise-Parabolic Algorithm}
  *

\section{Compression of Monotonous Integer Sequence}
\subsection{Problem Statement}
\subsection{Naive Solutions}
\subsection{Binary Interpolative Coding}
\subsection{Elias-Fano Coding}

\section{Minimal Perfect Hash Function}
\subsection{Problem Statement}
\subsection{CHD Algorithm}
\subsubsection{Hash Functions}

\section{Inter-Process Communication}
\subsection{UNIX Domain Sockets}
\subsection{Signals}


%\section{Compare-and-Swap Synchronisation}

\chapter{Design}
\section{Aggregate Functions}
 * needs to be said that these are real functions

The aggregation of the incoming data is done through a set of selected aggregate functions. Each
function summarises the values into a single value, representing a particular feature of the whole
data set. The functions are referentially transparent and can result in failure if insufficient or
corrupt data are presented.

\subsection{Selected Functions}
The selected functions for the database system cover a wide range of statistical aggregations. The
intention is to cover the most commonly used functions, that 

\paragraph {The \em{first} function} selects the first value from the data set. The expectation for
this function is that at least a single value is supplied. The real-world use for this function
might be a market open value of a particular stock, or as a base of a monotonically incrementing
metric.

\paragraph{The \em{last} function} is the analogical opposite to the `first` function, as the
function selects the last value from the data set. Both this and the `first` function can be used as
a computationally inexpensive method of retrieving an actual value from the set.

\paragraph{The \em{count} function} ignores the actual numerical values and only 

\paragraph{The \em{sum} function} adds all values in the series together, resulting in the series'
sum.

\paragraph{The \em{minimum} function} 

\paragraph{The \em{maximum} function} is a converse function to the \em{minimum} function, instead
selecting the greatest value from the series.

\paragraph{The \em{average} function} 

\paragraph{The \em{standard deviation} function} 

\subsection{Formatting}
The aforementioned aggregate functions are accessible and identifiable through an interface. The
abbreviations for the functions, listed in the same order: {\tt fst}, {\tt lst}, {\tt cnt}, {\tt
sum}, {\tt min}, {\tt max}, {\tt avg}, {\tt var}, {\tt dev}


\section{Metric}
The metric object provides a logical grouping, where a stream of values is uniquely identified by a
human-readable name. There is no other function to the metric objects, as they serve only as a tag.
The coupling of a value to a metric is done by the producer of the data. \\

\noindent The metric names consist of repeated lower-case letters, digits, and the underscore
character. The underscore character is intended for separation purposes between objects, similar to
the slash character in UNIX filenames.

\section{Views}


\section{Filter}
 * incoming values are raw, or processed by the originating system
 * the originating system might not have processing power to spare
   * or might not have a easy way to change that when needed (microcontrollers)
 * storing this logic within the originating system also means that it needs to manage the memory in a reduced fashion, thus replicating the efforts of the database system
 * data pre- and post-processing functions are offered to address common needs, parametrised to allow for great coverage of problems
\subsection{Input Transformations}
  *  
  * 
\subsection{Output Transformations}
  * main purpose of output transformations is to post-process the resulting aggregate values for
  further storage or consumption
  * consumption 
\subsection{Histogram}
  * a set of filters is able to represent a histogram
  * the configuration is constructed by using the ranges and comparison filters, using the counting
  aggregate function
  * alternatively, a different function can be used to obtain other aggregates for the full spectrum
  of values
\section{Operations}
  * 
\section{Example Problem Domains}
\subsection{Embedded}
\subsection{Large Data Sets}
\subsection{Network Monitoring}
\section{Drawbacks}
The design aims to satisfy a narrow band of applications, focusing on the stated dissertation goals.
It does introduce a set of drawbacks that are just as important as the features it offers for any
potential users.

\subsection{Reconfiguration}
The data is aggregated through views, which already exist at the time of the data point being
ingested. The database design does not offer the possibility of reconfiguration of the views on
historical data. This is due to the fact that the raw data points are dropped once all aggregate
functions are applied so that they do not inflate the memory requirements. \\

\noindent Limited combinations of intervals are possible in theory, where the aggregate function
result is re-usable for further aggregation without significant precision loss, e.g. the {\tt max}
function. Even then, it is not possible to determine the resulting value in a sub-interval, as well
as combination of views with non-matching interval durations might yield imprecise results. \\

\noindent This design drawback is a direct consequence of the overall memory footprint goal, as
storing the actual data points would cause a tremendous amount of memory pressure, both in terms of
operational memory and long-term storage. It constitutes a non-goal for the system, despite the
clear usefulness of the functionality, especially when it comes to troubleshooting of historical
issues, where a particular aggregation was not yet deemed important.

\subsection{Limitations}
Particular user experience has to be limited due to resource optimisation. Limitations such as
metric name length, maximal number of views or filters could hamper successful deployment of the
database system in certain practical applications. 

\section{Omissions}
The stated design does not concern itself with many areas of software engineering. Running the
database system as a software service 

\chapter{Implementation}
The implementation is a realisation of the design through the use of software engineering, resulting
in a usable software program. Aspects of the system design, along with practical challenges and
techniques are presented, closely matching the stated goals.

\section{Technology}
Following the industry standard practice and its wide adoption, the database system takes shape of
a user-space executable targeting the Linux kernel as its input/output interface. The kernel
interface is represented through a wide array of system calls which are used within the
implementation to access storage and facilitate inter-process communication.

\subsection{Language}
The chosen language of implementation is the C language, namely its ISO C17 standard. The language
was selected due to its ability to express memory layout through clear alignment rules and bit
fields of structures, as well as explicit memory management where the location of any object can be
selected in compile time. Similarly, the lifetime of any object, whether representing an
intermediate computation result or an integral node of a data structure, can be precisely determined
through manual allocation and deallocation. \\

\noindent The C language, through the Linux kernel libraries implementing the POSIX interface,
provides direct access to the system calls, avoiding any intermediate layers that might inflict
memory cost through techniques such as buffering or caching.

\subsection{Libraries}
The C17 standard library provides common functions dynamic memory management and other memory
operations such as copying or reseting. The standard library also offers standard integer and
boolean types that allow for precise integer bit width selection. The {\tt stdio} functionality of
the standard library is avoided due to issues with buffering of the input and output streams. \\

\noindent The POSIX interface, as implemented by the Linux system, provides systems calls exposed as
C library functions to access and manage the files, facilitate communication through UNIX domain
sockets, and obtain time and calendar information. \\

\noindent Additionally, the Linux-specific system calls not included in the POSIX standard are
accessed through a set of header files. This applies to the {\tt epoll} event subsystem, as well as
the {\tt signalfd} signal management facility through the use of file descriptors instead of the
standard function callback mechanism.

\section{Architecture}
The implementation strategy used followed ideas of decomposition, where each logical component of
the system was developed and tested as a standalone software library. This approach ensured clear
separation of boundaries, and allowed for good control over programming interface and its
maintenance. \\

  * explanation of the overall design of the program
  * that there are metrics and each has an associated list of lenses
  * that lenses need to be a linked list instead of an array 

\section{Implementation of the {\tt aggstat} library}
The {\tt aggstat} library implements the aggregate functions used as part of the view objects. 
  * the core of the database system are the aggregate functions
  * they present a logical component of the system that can be implemented and tested separately
  * the aggregate functions share a lot of common techniques, and their API is near identical
\subsection{Real Types}
  * the computations process real numbers 
  * selecting the type to represent them has wide impact on performance, storage, precision and
  portability
  * the size of the floating-point type is selected through the AGGSTAT\_FLT\_BIT macro

The following table lists the accepted values for the {\tt AGGSTAT\_FLT\_BIT} macro, the underlying
type that is selected, the effective bits used, and its effective range on two major architectures.
Note that the {\tt \_\_float128} type is not available on the {\tt aarch64} architecture as the {\tt
long double} type already covers the 128 effective bits.
\begin{center}
\begin{tabular}{l l l l}
{\tt AGGSTAT\_FLT\_BIT} value & C type & x86\_64 effective bits & aarch64 effective bits \\
32  & {\tt float}        & 32  & 32 \\
64  & {\tt double}       & 64  & 64 \\
80  & {\tt long double}  & 80  & 128 \\
128 & {\tt \_\_float128} & 128 & N/A \\
\end{tabular}
\end{center}
\subsection{Integral Types}
The sizes of the integral types do not present

\subsection{Structures}
Each aggregate function is represented as a standalone structure that contains the current state of
the function progression. Altogether, there are 13 structures, one for each aggregate function
listed in Section XXX of the Design chapter. In all cases, the structure consists of only integer
and floating-point types. \\


The following code excerpt provides an example of an actual structure type declaration for the
variance aggregate function:
\begin{verbatim}
struct aggstat_var {
  AGGSTAT_INT av_cnt; // Number of values.
  AGGSTAT_FLT av_fst; // First statistical moment.
  AGGSTAT_FLT av_snd; // Second statistical moment.
};
\end{verbatim}

\subsubsection{Identification}
* this approach would have been required in other languages, thus reducing their language constructs
to that of C, as virtual table inflicts addition memory cost for each view object.

\noindent The structures do not contain identification fields, as they are not expected to be used
within a C union, a construct that imitates polymorphism. This design choice was chosen to optimise
the memory required to identify the type of the aggregate function in runtime. Given that only 12
types exist, four bits are sufficient for the representation. Further implementation of this concept
is detailed in the Memory Layout section. \\

\noindent This approach would have been required had any other language been chosen for the
implementation, as concepts such as virtual tables incur a greater overhead for identification. Not
being able to use class polymorphism in C++ would reduce the language constructs available to that
of C.

\subsubsection{Sizes}
\noindent The following table lists the structures, their member types, and the overall memory
footprint in bits of selected type configurations. The listed sizes are listed without any
compression techniques applied. The configurations are identified by the floating-point type width
as set by the {\tt AGGSTAT\_FLT\_BIT} macro, prefixed with {\tt f}, and integer type width as set by
the {\tt AGGSTAT\_INT\_BIT} macro, prefixed with {\tt i}. Bit sizes are reported as observed for the
{\tt aarch64} architecture using the GCC 12 compiler on a Fedora 37 Linux operating system.
\begin{center}
\begin{tabular}{l l l l l}
Structure & Floating-point & Integer & {\tt f32i32} & {\tt f80i32} \\
{\tt aggstat\_fst} & 1 & 0 & 4  & 16 \\
{\tt aggstat\_lst} & 1 & 0 & 4  & 16 \\
{\tt aggstat\_cnt} & 0 & 1 & 4  & 16 \\
{\tt aggstat\_sum} & 1 & 0 & 4  & 16 \\
{\tt aggstat\_min} & 1 & 0 & 4  & 16 \\
{\tt aggstat\_max} & 1 & 0 & 4  & 16 \\
{\tt aggstat\_avg} & 1 & 1 & 8  & 24 \\
{\tt aggstat\_var} & 2 & 1 & 12 & 36 \\
{\tt aggstat\_skw} & 2 & 1 & 12 & 36 \\
{\tt aggstat\_krt} & 2 & 1 & 12 & 36 \\
{\tt aggstat\_med} & 2 & 1 & 12 & 36 \\
{\tt aggstat\_qnt} & 2 & 1 & 12 & 36 \\
\end{tabular}
{\it Table 1: {\tt aggstat} structure size}
\end{center}

\subsection{Functions}
Each aggregate function structure is controlled through three functions: first that initialises its
state, second to update the aggregate function with a new value, and a third to obtain the current
aggregate value. Their respective suffixes are {\tt new}, {\tt put}, and {\tt get}, appended to the
structure name, resulting in a name such as {\tt aggstat\_var\_put}. \\

\noindent * return values as get is expected to fail
* garbage in garbage out with NaN

\subsection{Algorithms}
  * describe how the simple functions are implemented
  * the average/variance functions are using the welfords' algo
  * and that quantile is using the p2 algorithm
  * and that median is using the fame algorithm (but can be supplanted with p2 with argument of 0.5)

\subsection{Compression}
Each view as defined in the Design chapter contains a single aggregate function and thus presents a
significant contribution to the overall memory footprint. Given this this significant linear impact,
the storage requirement of each aggregate function ought to be as reduced as possible.

\subsubsection{Special Values}
The naive implementation approach to certain functions requires additional memory to address edge
cases. For example, the maximum function where the special case of no values requires tracking of
whether a value was already provided. As described in the Background chapter the IEEE 754
floating-point format contains a provision for special values that do not represent any numerical 
values. \\

\noindent In particular, the non-signalling NaN set of numbers is used in the {\it first}, {\it
last}, {\it minimum}, and {\it maximum} aggregate functions to represent the state where no values
was provided to the function yet. The implementation uses the {\tt nan} family of functions to
create the NaN value with all message bits unset. The {\tt isnan} function is used to detect the
special value and act accordingly in both retrieval and update of the aggregate function.

\subsubsection{Truncation Precision Decay}
The IEEE 754 format stores the 
  * Conformance with NaN Format (because last bits are not important for NaN)
  * include an example of how the PI value decays for a float type, maybe even the simple program to
  obtain it

\noindent The following table illustrates the precision decay of truncating the floating-point value
using the 32-bit {\tt float} type. The chosen value is the {\tt M\_PI} constant that represents the
$\pi$ constant typecast to the {\tt float} type. The values are rounded to the tenth decimal place.

\begin{center}
\begin{tabular}{ccc}
Number of truncated bits & Resulting value & Lost precision \\
0  & 3.1415927410 & 0.0000000000 \\
1  & 3.1415925026 & 0.0000002384 \\
2  & 3.1415920258 & 0.0000007153 \\
3  & 3.1415920258 & 0.0000007153 \\
4  & 3.1415901184 & 0.0000026226 \\
5  & 3.1415863037 & 0.0000064373 \\
6  & 3.1415863037 & 0.0000064373 \\
7  & 3.1415710449 & 0.0000216961 \\
8  & 3.1415405273 & 0.0000522137 \\
9  & 3.1414794922 & 0.0001132488 \\
10 & 3.1413574219 & 0.0002353191 \\
11 & 3.1411132812 & 0.0004794598 \\
12 & 3.1406250000 & 0.0009677410 \\
13 & 3.1406250000 & 0.0009677410 \\
14 & 3.1406250000 & 0.0009677410 \\
15 & 3.1406250000 & 0.0009677410 \\
16 & 3.1406250000 & 0.0009677410 \\
\end{tabular}
{\it Table 2}
\end{center}

\noindent The table shows the progressive reduction of precision for the floating-point constant.
Notably, the precision drops for set bits of the mantissa component. \\

\noindent This compression method causes a significant reduction in precision, especially when
repeated on the same value, hence causing a compound effect on the last bits. Therefore, it is best
applied to functions where the value is always replaced instead of manipulated with, such as the
aggregate functions {\it first}, {\tt last}, {\tt minimum}, and {\tt maximum}. \\

\noindent To illustrate the compound issue, consider the {\tt sum} functions where all input data
are combined using the plus function. If all values, close to zero, fall below the error rate, the
resulting sum would be zero, even though a multiply of the error rate might be off by an order of
magnitude. It is therefore not appropriate to utilise this technique for this aggregate function.

\subsubsection{Invariants and Recomputation}
The reference implementation of the $P^2$ algorithm is provided in the original publication as
pseudocode. The code in question utilises five markers, five counters, and five desired positions.
The first counter never increases, as by definition it represents the minimal value and thus its
value remains invariant at value one. Removing the first counter and instead adjusting the indices
in the remainder of the implementation saves a single integer. \\

\noindent Additionally, the desired positions always take the form of $(n - 1)x$, where $n$ is the
number of observations and $x$ is one of the five observed quantiles. As the value can be computed
at every turn and does not need to be stored, the trade-off choice is made such that memory is
exchanged for additional computation. \\

\noindent As a result, the $P^2$ implementation uses four integers, and five floating-point values.
The memory savings in the {\tt f32f32} configuration is 24 bytes per each quantile aggregate
function. 

\subsection{Approximation Errors}
  * the chosen algorithms trade the precision of computation for reduced memory footprint
  * but the precision ought to remain within practical limits
  * the truncation can cause underflow or overflow for minimum and maximum functions
  * the actual errors were measured against an industry standard library: GSL
  * for each function a set of inputs
\subsection{Function Macros}
  * operations are described through macros
  * each type can therefore define its own 
  * this is a C language solution to polymorphism as the floating point type can't be supplied as
  a type parameter

\section{Implementation of the {\tt zipints} library}
 
\subsection{Integer Types}
  * the chosen in
\subsection{Structure}
  * 
 
\subsection{Functions}

\subsection{Hardware Acceleration}
  * 

\section{Implementation of the {\tt mphfunc} library}
\subsection{}
\subsection{Monotonicity}
  * 
\subsection{Hash Functions}

\section{Memory Layout}
  * reiterate the layers
  * explain how to combine the layers

\section{Lazy Evaluation of Intervals}
  * upon completion of each interval, the final aggregate value should be either serialised, send
  over some communication channel or reported to the user
  * an action needs to be taken
  * the way how to realise this: 
    * 

  * in addition, 

\subsection{Interface}
Most of the algorithms and data structures described in sections above deal with pure functions that
implement the inner workings of the database system. No functionality yet described facilitates the
nteraction outside of the database process. An application programming interface allows for external
access and control over the data and organisational objects of the database.

\subsubsection{Format Definition}
Abiding by the 

\subsubsection{Alternatives Considered}
  * why not HTTPS over TCP
  * why not 
\subsubsection{UNIX Domain Sockets}
\subsubsection{Signals}
\subsubsection{Event Loop}
\subsubsection{User Experience}
\subsubsection{Extensibility}

\subsection{Compilation}
\subsubsection{Compiler Selection}
\subsubsection{Compiler and Linker Flags}

\subsection{Testing}
  
\subsubsection{Strategy}
\subsubsection{Random Input Generation}
\subsubsection{Hardware Acceleration}
\subsubsection{Reducing Impact of the Exponential Explosion}

\subsection{Engineering Practices}
\subsubsection{Documentation}
  * 
\subsubsection{Version Control}
  * version control was used throughout the development of the software implementation
  * the version control software used was git, with remote servers of Google Cloud (Source
  Repositories)
  * using git has aided the development in 
\subsubsection{Coding Standards}
  * the C language has numerous constructs, operators and types that often represent the same
  underlying idea
  * to prevent inconsistencies across the codebase, a set of coding standards was defined
  * the coding standards in each area of the language set limits to its use
  * 

\chapter{Evaluation}
  * the software implementation described in the previous chapter ought to be executed against a
  reasonable input data set to determine the degree to which the set goals were met
  * the evaluation is conducted on two computer systems, each using a different hardware
  architecture and reaches a different monetary value
\section{Hardware Selection}
\subsection{\pounds 100 Target}
  * given a budget of 100gbp
  * given a budget of 1000gbp
\section{Database Configuration}
  * the evaluation is done in two configurations, f32i32 and f80i64
  * this affects the metric names, the memory requirements and alignment
\section{Random Data Generation}
  * reasonable data is required to test the behaviour of the system
  * instead of using random data as seen in the 
\subsection{Metrics and Views}
\subsection{Time Series Data}
\section{Resource Utilisation}
  * the test involves execution of the database software.

\chapter{Reflection}
\section{Achievements}
  * based on the implementation and evaluation, what were the success stories in terms of
  technological achievement
  * this could include the low memory footprint of the implementation per metric/lens
\subsection{Portability}
  * the implementation is portable across architectures and operating systems due to its use of the
  C language in the widely available POSIX interface
  * the use of the epoll and signalfd subsystems presents the only OS-specific aspects of the
  implementation
  * this could be simply replaced by using the kqueue system on the BSD-based systems, or IOCP
  mechanism on Windows
  * another alternative would be a 
  * it could be used as a standalone application as demonstrated in the Evaluation chapter, but al
\section{Missed Opportunities}
\subsection{Aggregate Function Deduplication}
Certain aggregate functions share the aggregate values or partial computations. From the set of
selected aggregate functions, both the minimum and maximum values are contained within the structure
that implements the $P^2$ algorithm, as they are required as markers. Similarly, the four functions
average, variance, skewness, and kurtosis gradually share the intermediate statistical moment
values.
\section{Personal Findings}
  * what was surprising or beneficial to me throughout the implementation phase
  * perhaps also that this is usable for personal projects and that it meets the criteria

\chapter{Bibliography}

\chapter{Appendix A: Type and Function Declarations}
\chapter{Appendix B: Testing Script Example}
\chapter{Appendix C: Estimation Error Tables}

\end{document}
