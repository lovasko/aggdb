// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#if !__has_builtin(__builtin_readcyclecounter)
  #include <time.h>
#endif

#include "clkutil/clkutil.h"


/// Initialise the clock.
void clkutil_new(
        struct clkutil *const clk, // Clock.
  const uintmax_t             mth) // Measurement threshold.
{
  clk->cu_prv = 0;
  clk->cu_stt = 0;
  clk->cu_min = UINTMAX_MAX;
  clk->cu_max = 0;
  clk->cu_sum = 0;
  clk->cu_cnt = 0;
  clk->cu_mth = mth;
}

/// Make a measurement and optionally update the statistics.
void clkutil_put(
  struct clkutil *const clk) // Clock.
{
  uintmax_t cur; // Current nanosecond timestamp.
  uintmax_t dif; // Time difference.

  // Obtain the current time.
  #if __has_builtin(__builtin_readcyclecounter)
    cur = __builtin_readcyclecounter();
  #else
    struct timespec now; // Current time.
    (void)clock_gettime(CLOCK_MONOTONIC, &now);
    cur = (uintmax_t)now.tv_sec * 1000000000ULL + (uintmax_t)now.tv_nsec;
  #endif

  // Measure the time stamp difference on every second invocation.
  if (clk->cu_stt == 1) {
    // Update the counter and determine whether to continue.
    clk->cu_cnt += 1;
    if (clk->cu_cnt < clk->cu_mth) {
      return;
    }

    // Compute the interval difference.
    dif = cur - clk->cu_prv;

    // Update the statistics and advance to the next state.
    if (dif < clk->cu_min) {
      clk->cu_min = dif;
    }
    if (dif > clk->cu_max) {
      clk->cu_max = dif;
    }
    clk->cu_sum += dif;
  } else {
    // Save the measurement and advance to the next state.
    clk->cu_prv = cur;
    clk->cu_stt = 1;
  }
}

/// Retrieve the current clock time statistic.
bool clkutil_get(
  const struct clkutil *const restrict clk, // Clock.
        uintmax_t      *const restrict min, // Minimal value.
        uintmax_t      *const restrict max, // Maximal value.
        uintmax_t      *const restrict avg) // Average value.
{
  // Ensure that there is at least a single measurement.
  if (clk->cu_cnt <= clk->cu_mth) {
    return false;
  }

  // Retrieve and compute the statistic values.
  *min = clk->cu_min;
  *max = clk->cu_max;
  *avg = clk->cu_sum / (clk->cu_cnt - clk->cu_mth);
  return true;
}
