// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef CLKUTIL_H
#define CLKUTIL_H

#include <stdbool.h>
#include <stdint.h>


/// Clock.
struct clkutil {
  uintmax_t cu_prv; // Previous value.
  uintmax_t cu_stt; // State.
  uintmax_t cu_min; // Minimal value.
  uintmax_t cu_max; // Maximal value.
  uintmax_t cu_sum; // Sum of all values.
  uintmax_t cu_cnt; // Number of values.
  uintmax_t cu_mth; // Measurement threshold.
};

void clkutil_new(struct clkutil *const clk, const uintmax_t mth);
void clkutil_put(struct clkutil *const clk);
bool clkutil_get(
  const struct clkutil *const restrict clk,
        uintmax_t      *const restrict min,
        uintmax_t      *const restrict max,
        uintmax_t      *const restrict avg);

#endif
