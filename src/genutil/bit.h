// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.

#ifndef GENUTIL_BIT_H
#define GENUTIL_BIT_H

#include <stdint.h>


// Ensure that the type sizes are selected by the user.
#ifndef GENUTIL_INT_BIT
  #error "must define GENUTIL_INT_BIT to set unsigned integer width"
#endif

// Define the appropriate types based on the selection.
#if   GENUTIL_INT_BIT == 16
  #define GENUTIL_INT      uint16_t
#elif GENUTIL_INT_BIT == 32
  #define GENUTIL_INT      uint32_t
#elif GENUTIL_INT_BIT == 64
  #define GENUTIL_INT      uint64_t
#else
  #error "invalid value for GENUTIL_INT_BIT: '" GENUTIL_INT_BIT "' [16, 32, 64]"
#endif

/// Attempt to select the appropriate builtin for the selected type to count trailing zeros.
#ifdef GENUTIL_ACC
  #if   GENUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_ctz)
    #define genutil_bit_ctz __builtin_ctz
  #elif GENUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_ctzl)
    #define genutil_bit_ctz __builtin_ctzl
  #elif GENUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_ctzll)
    #define genutil_bit_ctz __builtin_ctzll
  #endif
#endif

/// Attempt to select the appropriate builtin for the selected type to count leading zeros.
#ifdef GENUTIL_ACC
  #if   GENUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_clz)
    #define genutil_bit_clz __builtin_clz
  #elif GENUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_clzl)
    #define genutil_bit_clz __builtin_clzl
  #elif GENUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_clzll)
    #define genutil_bit_clz __builtin_clzll
  #endif
#endif

/// Attempt to select the appropriate builtin for the selected type to count set bits.
#ifdef GENUTIL_ACC
  #if   GENUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_popcount)
    #define genutil_bit_pop __builtin_popcount
  #elif GENUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_popcountl)
    #define genutil_bit_pop __builtin_popcountl
  #elif GENUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_popcountll)
    #define genutil_bit_pop __builtin_popcountll
  #endif
#endif

#ifndef genutil_bit_ctz
  /// Count the number of trailing unset bits of a word.
  GENUTIL_INT genutil_bit_ctz(
    const GENUTIL_INT inp);
#endif

#ifndef genutil_bit_clz
  /// Count the number of leading unset bits of a word.
  GENUTIL_INT genutil_bit_clz(
    const GENUTIL_INT inp);
#endif

#ifndef genutil_bit_pop
  /// Compute the number of set bits in a word (population count).
  GENUTIL_INT genutil_bit_pop(
    const GENUTIL_INT inp);
#endif

/// Compute the one-based index of the n-th set bit within a single word.
GENUTIL_INT genutil_bit_sel(
  const GENUTIL_INT inp,
  const GENUTIL_INT nth);

#endif
