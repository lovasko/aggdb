// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef GENUTIL_RNG_H
#define GENUTIL_RNG_H

#include <stdint.h>

#if defined(GENUTIL_ACC) && defined(__x86_64__)
  #include <x86intrin.h>
#endif

// Ensure that the type sizes are selected by the user.
#ifndef GENUTIL_INT_BIT
  #error "must define GENUTIL_INT_BIT to set unsigned integer width"
#endif

#ifndef GENUTIL_FLT_EXC
  #ifndef GENUTIL_FLT_BIT
    #error "must define GENUTIL_FLT_BIT to set floating-point width"
  #endif
#endif

// Define the appropriate types based on the selection.
#if   GENUTIL_INT_BIT == 8
  #define GENUTIL_INT      uint8_t
  #define GENUTIL_INT_BYTE 1
  #define GENUTIL_INT_MAX  UINT8_MAX 

#elif GENUTIL_INT_BIT == 16
  #define GENUTIL_INT      uint16_t
  #define GENUTIL_INT_BYTE 2
  #define GENUTIL_INT_MAX  UINT16_MAX
  #if defined(GENUTIL_ACC) && defined(__x86_64__)
    #define GENUTIL_INT_STEP _rdrand16_step
  #endif

#elif GENUTIL_INT_BIT == 32
  #define GENUTIL_INT      uint32_t
  #define GENUTIL_INT_BYTE 4
  #define GENUTIL_INT_MAX  UINT32_MAX
  #if defined(GENUTIL_ACC) && defined(__x86_64__)
    #define GENUTIL_INT_STEP _rdrand32_step
  #endif

#elif GENUTIL_INT_BIT == 64
  #define GENUTIL_INT      uint64_t
  #define GENUTIL_INT_BYTE 8
  #define GENUTIL_INT_MAX  UINT64_MAX
  #if defined(GENUTIL_ACC) && defined(__x86_64__)
    #define GENUTIL_INT_STEP _rdrand64_step
  #endif

#elif GENUTIL_INT_BIT == 128
  #define GENUTIL_INT      __uint128_t
  #define GENUTIL_INT_BYTE 16
  #define GENUTIL_INT_MAX  (__uint128_t)(-1)
#else
  #error "invalid value for GENUTIL_INT_BIT: '" GENUTIL_INT_BIT "' [16, 32, 64, 128]"
#endif

#ifndef GENUTIL_FLT_EXC
  #if   GENUTIL_FLT_BIT == 32
    #define GENUTIL_FLT                 float
    #define GENUTIL_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## f
  #elif GENUTIL_FLT_BIT == 64
    #define GENUTIL_FLT                 double
    #define GENUTIL_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E
  #elif GENUTIL_FLT_BIT == 80
    #define GENUTIL_FLT                 long double
    #define GENUTIL_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## L
  #elif GENUTIL_FLT_BIT == 128 
    #define GENUTIL_FLT                 __float128
    #define GENUTIL_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## Q
  #else
    #error "invalid value for GENUTIL_FLT_BIT: '" GENUTIL_FLT_BIT "' [32, 64, 80, 128]"
  #endif

  #define GENUTIL_FLT_65535_0 GENUTIL_FLT_NUM(65535, 0, +, 0)
#endif


/// Random number generator based on the "Small PRNG".
struct genutil_rng {
  #ifndef GENUTIL_INT_STEP
    uint32_t gr_val[4]; // State.
  #endif
};

/// Initialise the random number generator state based on a seed.
void genutil_rng_new(
        struct genutil_rng *const rng,  // Random number generator.
  const uint32_t                  rsd); // Random seed.

/// Generate an integer within the selected bounds.
GENUTIL_INT genutil_rng_int(
        struct genutil_rng *const rng,  // Random number generator.
  const GENUTIL_INT               lob,  // Lower bound (inclusive).
  const GENUTIL_INT               upb); // Upper bound (inclusive).

/// Generate an integer from the full range of possible values.
GENUTIL_INT genutil_rng_any(
  struct genutil_rng *const rng); // Random number generator.

#ifndef GENUTIL_FLT_EXC
/// Generate a floating-point number within the selected bounds.
GENUTIL_FLT genutil_rng_flt(
        struct genutil_rng *const rng,  // Random number generator.
  const GENUTIL_FLT               lob,  // Lower bound (inclusive).
  const GENUTIL_FLT               upb); // Upper bound (inclusive).
#endif

#endif
