// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#if !__has_builtin(__builtin_readcyclecounter)
  #include <time.h>
#endif

#include "clk.h"


/// Initialise the clock.
void genutil_clk_new(
        struct genutil_clk* clk, // Clock.
  const uintmax_t           mth) // Measurement threshold.
{
  clk->gc_prv = 0;
  clk->gc_stt = 0;
  clk->gc_min = UINTMAX_MAX;
  clk->gc_max = 0;
  clk->gc_sum = 0;
  clk->gc_cnt = 0;
  clk->gc_mth = mth;
}

/// Make a measurement and optionally update the statistics.
void genutil_clk_put(
  struct genutil_clk* clk) // Clock.
{
  uintmax_t       cur; // Current nanosecond timestamp.
  uintmax_t       dif; // Time difference.

  // Obtain the current time.
  #if __has_builtin(__builtin_readcyclecounter)
    cur = __builtin_readcyclecounter();
  #else
    struct timespec now; // Current time.
    (void)clock_gettime(CLOCK_MONOTONIC, &now);
    cur = (uintmax_t)now.tv_sec * 1000000000ULL + (uintmax_t)now.tv_nsec;
  #endif

  // Measure the time stamp difference on every second invocation.
  if (clk->gc_stt == 1) {
    // Update the counter and determine whether to continue.
    clk->gc_cnt += 1;
    if (clk->gc_cnt < clk->gc_mth) {
      return;
    }

    // Compute the interval difference.
    dif = cur - clk->gc_prv;

    // Update the statistics and advance to the next state.
    if (dif < clk->gc_min) {
      clk->gc_min = dif;
    }
    if (dif > clk->gc_max) {
      clk->gc_max = dif;
    }
    clk->gc_sum += dif;
  } else {
    // Save the measurement and advance to the next state.
    clk->gc_prv = cur;
    clk->gc_stt = 1;
  }
}

/// Retrieve the current clock time statistic.
bool genutil_clk_get(
  const struct genutil_clk *const restrict clk, // Clock.
        uintmax_t          *const restrict min, // Minimal value.
        uintmax_t          *const restrict max, // Maximal value.
        uintmax_t          *const restrict avg) // Average value.
{
  // Ensure that there is at least a single measurement.
  if (clk->gc_cnt <= clk->gc_mth) {
    return false;
  }

  // Retrieve and compute the statistic values.
  *min = clk->gc_min;
  *max = clk->gc_max;
  *avg = clk->gc_sum / (clk->gc_cnt - clk->gc_mth);
  return true;
}
