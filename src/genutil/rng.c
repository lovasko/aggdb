// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include "rng.h"


#ifndef GENUTIL_INT_STEP
/// Rotate a 32-bit unsigned integer.
static uint32_t genutil_rng_rot(
  const uint32_t val,
  const uint32_t cnt)
{
  uint32_t xhv;
  uint32_t yhv;

  xhv = val << cnt;
  yhv = val >> (32 - cnt);
  return xhv | yhv;
}

/// Generate a random byte.
static uint8_t genutil_rng_get(
  struct genutil_rng *const rng) // Random number generator.
{
  uint32_t xhv; // Helper variable.

  xhv            = rng->gr_val[0] - genutil_rng_rot(rng->gr_val[1], 27);
	rng->gr_val[0] = rng->gr_val[1] ^ genutil_rng_rot(rng->gr_val[2], 17);
	rng->gr_val[1] = rng->gr_val[2] + rng->gr_val[3];
	rng->gr_val[2] = rng->gr_val[3] + xhv;
	rng->gr_val[3] = rng->gr_val[0] + xhv;

  return (uint8_t)(rng->gr_val[3] % 256);
}
#endif

/// Initialise the random number generator state based on a seed.
void genutil_rng_new(
        struct genutil_rng *const rng, // Random number generator state.
  const uint32_t                  rsd) // Random seed.
{
  #ifndef GENUTIL_INT_STEP
    GENUTIL_INT ctr; // Counter.

    // Initialise the state.
  	rng->gr_val[0] = 0xF1EA5EED;
  	rng->gr_val[1] = rsd;
  	rng->gr_val[2] = rsd;
  	rng->gr_val[3] = rsd;

    // Advance the generator sequence.
	  ctr = 0;
	  while (ctr < 20) {
	    (void)genutil_rng_get(rng);
	    ctr += 1;
	  }
  #else
    (void)rng;
    (void)rsd;
  #endif
}

/// Generate an integer within the selected bounds.
GENUTIL_INT genutil_rng_int(
        struct genutil_rng *const rng, // Random number generator state.
  const GENUTIL_INT               lob, // Lower boundary (inclusive).
  const GENUTIL_INT               upb) // Upper boundary (inclusive).
{
  GENUTIL_INT val;

  // Generate an integer from the full range.
  val = genutil_rng_any(rng);
  
  // Reduce the integer to the requested range.
  val = (val % (upb - lob + 1)) + lob;
  return val;
}

/// Generate an integer from the full range of possible values.
GENUTIL_INT genutil_rng_any(
  struct genutil_rng *const rng) // Random number generator.
{
  #ifndef GENUTIL_INT_STEP
    union {
      uint8_t     byt[GENUTIL_INT_BYTE]; // Bytes of the random number.
      GENUTIL_INT val;                   // Random number.
    } num;                               // Random number.
    GENUTIL_INT   idx;                   // Array index.

    // Generate the integer, one byte at a time.
    idx = 0;
    while (idx < GENUTIL_INT_BYTE) {
      num.byt[idx] = genutil_rng_get(rng);
      idx += 1;
    }

    return num.val;
  #else
    // This is an exception due to mismatch in the 64-bit argument of the `_rdrand64_step` function
    // and the definition of the `uint64_t` type in `stdint.h`.
    #if GENUTIL_INT_BIT == 64
      unsigned long long int val;
    #else
      GENUTIL_INT val; // Random number.
    #endif

    (void)rng;
    GENUTIL_INT_STEP(&val);
    return (GENUTIL_INT)val;
  #endif
}

#ifndef GENUTIL_FLT_EXC
/// Generate a floating-point number within the selected bounds.
GENUTIL_FLT genutil_rng_flt(
        struct genutil_rng *const rng, // Random number generator state.
  const GENUTIL_FLT               lob, // Lower bound (inclusive).
  const GENUTIL_FLT               upb) // Upper bound (inclusive).
{
  GENUTIL_FLT val; // Random value.
  GENUTIL_INT rnd; // Random integer value.

  // Generate a value between 0.0 and 1.0.
  rnd = genutil_rng_int(rng, 0, 65535);
  val = (GENUTIL_FLT)rnd / GENUTIL_FLT_65535_0;

  // Scale the value to fit the expected range.
  return (val * (upb - lob) + lob);
}
#endif
