// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.

#include "bit.h"


#ifndef genutil_bit_ctz 
/// Count the number of trailing unset bits of a word.
GENUTIL_INT genutil_bit_ctz(
  const GENUTIL_INT inp) // Word.
{
  GENUTIL_INT idx; // Bit index.
  GENUTIL_INT cnt; // Number of zeros.

  // Iterate the bits, starting with the least significant. Terminate once a set bit is reached.
  idx = 0;
  cnt = 0;
  while (idx < GENUTIL_INT_BIT) {
    if (((inp >> idx) & 1) == 0) {
      cnt += 1;
    } else {
      break;
    }

    idx += 1;
  }

  return cnt;
}
#endif

#ifndef genutil_bit_clz
/// Count the number of leading unset bits of a word.
GENUTIL_INT genutil_bit_clz(
  const GENUTIL_INT inp) // Word.
{
  GENUTIL_INT idx; // Bit index.
  GENUTIL_INT cnt; // Number of zeros.

  // Iterate the bits, starting with the least significant. Terminate once a set bit is reached.
  idx = 0;
  cnt = 0;
  while (idx < GENUTIL_INT_BIT) {
    if (((inp << idx) & ((GENUTIL_INT)1 << (GENUTIL_INT_BIT - 1))) == 0) {
      cnt += 1;
    } else {
      break;
    }

    idx += 1;
  }

  return cnt;
}
#endif

#ifndef genutil_bit_pop
/// Compute the number of set bits in a word (population count).
GENUTIL_INT genutil_bit_pop(
  const GENUTIL_INT inp) // Word.
{
  GENUTIL_INT idx; // Bit index.
  GENUTIL_INT cnt; // Population count.

  cnt = 0;
  idx = 0;
  while (idx < GENUTIL_INT_BIT) {
    if (inp >> idx & 1) {
      cnt += 1;
    }

    idx += 1;
  }

  return cnt;
}
#endif

/// Compute the one-based index of the n-th set bit within a single word.
GENUTIL_INT genutil_bit_sel(
  const GENUTIL_INT inp, // Word.
  const GENUTIL_INT nth) // Number of the set bit.
{
  GENUTIL_INT cnt; // Set bit count.
  GENUTIL_INT idx; // Bit index.
  GENUTIL_INT val; // Word.

  // Create a mutable copy of the input word.
  val = inp;

  // One by one scan the bits of the byte and count the set bits.
  cnt = 0;
  idx = 0;
  while (idx < GENUTIL_INT_BIT) {
    // Extract and process the first bit.
    cnt += val & 1;

    // Check for the intended condition.
    if (cnt == (nth + 1)) {
      return idx;
    }

    // Advance to the next bit.
    val >>= 1;
    idx  += 1;
  }

  // This should not be reached.
  return 0;
}
