// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef GENUTIL_CLK_H
#define GENUTIL_CLK_H

#include <stdbool.h>
#include <stdint.h>


struct genutil_clk {
  uintmax_t gc_prv;
  uintmax_t gc_stt;
  uintmax_t gc_min;
  uintmax_t gc_max;
  uintmax_t gc_sum;
  uintmax_t gc_cnt;
  uintmax_t gc_mth; // Measurement threshold.
};

void genutil_clk_new(
        struct genutil_clk *const clk,
  const uintmax_t                 mth);
void genutil_clk_put(struct genutil_clk *const clk);
bool genutil_clk_get(
  const struct genutil_clk *const restrict clk,
        uintmax_t          *const restrict min,
        uintmax_t          *const restrict max,
        uintmax_t          *const restrict avg);

#endif
