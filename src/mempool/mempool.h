// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef MEMPOOL_H
#define MEMPOOL_H

#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>


// Ensure that the bit width is defined.
#ifndef MEMPOOL_INT_BIT
  #error "MEMPOOL_INT_BIT must be defined"
#endif

// Ensure that the integer type is selected based on the bit width.
#ifdef MEMPOOL_INT
  #error "MEMPOOL_INT must be set by this header file"
#endif
#ifdef MEMPOOL_INT2
  #error "MEMPOOL_INT2 must be set by this header file"
#endif


// Based on the selected bit width, select the appropriate type that will be
// used to represent all indices within the memory pool.
//  * MEMPOOL_INT     - integer type used for indices
//  * MEMPOOL_INT2    - integer type twice the size of the indices

#if   MEMPOOL_INT_BIT == 8
  #define MEMPOOL_INT  uint8_t
  #define MEMPOOL_INT2 uint16_t
#elif MEMPOOL_INT_BIT == 16
  #define MEMPOOL_INT  uint16_t
  #define MEMPOOL_INT2 uint32_t
#elif MEMPOOL_INT_BIT == 32
  #define MEMPOOL_INT  uint32_t
  #define MEMPOOL_INT2 uint64_t
#elif
  #error "MEMPOOL_INT_BIT is set to an unsupported value"
#endif

// Ensure that the selected types is implemented as lock-free in the current standard C library.
#if MEMPOOL_INT_BIT == __SHORT_WIDTH__
  #if ATOMIC_SHORT_LOCK_FREE != 2
    #error "the selected type is not lock-free"
  #endif

  #if ATOMIC_SHORT_LOCK_FREE != 2
    #error "the selected type's double is not lock free"
  #endif
#endif

#if MEMPOOL_INT_BIT == __INT_WIDTH__
  #if ATOMIC_INT_LOCK_FREE != 2
    #error "the selected type is not lock-free"
  #endif

  #if ATOMIC_LONG_LOCK_FREE != 2
    #error "the selected type's double is not lock free"
  #endif
#endif

/// Memory pool.
struct mempool {
  _Atomic MEMPOOL_INT* mp_mem; // Backing array acting as a storage of objects.
  _Atomic MEMPOOL_INT2 mp_nxt; // Unique counter & index of the next object.
  _Atomic MEMPOOL_INT  mp_cnt; // Number of allocated objects.
          MEMPOOL_INT  mp_obj; // Size of each object in integers.
          MEMPOOL_INT  mp_max; // Total number of objects.
};

bool        mempool_new(struct mempool* mem, const MEMPOOL_INT max, const MEMPOOL_INT obj);
void        mempool_del(struct mempool* mem);
MEMPOOL_INT mempool_acq(struct mempool* mem);
void        mempool_rel(struct mempool* mem, const MEMPOOL_INT idx);
MEMPOOL_INT mempool_get(struct mempool* mem, const MEMPOOL_INT idx, const MEMPOOL_INT off);
void        mempool_put(struct mempool* mem, const MEMPOOL_INT idx, const MEMPOOL_INT off, const MEMPOOL_INT val);

#endif
