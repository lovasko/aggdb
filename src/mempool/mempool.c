// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdlib.h>
#include <string.h>
#include <stdatomic.h>
#include <inttypes.h>

#include "mempool.h"


/// Allocate a new memory pool.
bool mempool_new(
        struct mempool *const mem, // Memory pool.
  const MEMPOOL_INT           max, // Maximal number of objects.
  const MEMPOOL_INT           obj) // Object size in integers.
{
  MEMPOOL_INT idx; // Array index.

  // Allocate memory from the system.
  mem->mp_mem = calloc(max, obj * sizeof(_Atomic MEMPOOL_INT));
  if (mem->mp_mem == NULL) {
    return false;
  }

  // Initialise the internal state.
  mem->mp_obj = obj;
  mem->mp_max = max;
  atomic_store(&mem->mp_cnt, 0);
  atomic_store(&mem->mp_nxt, 0);

  // Ensure that each first integer of all objects of the memory pool contain an object index
  // pointing to the next acquirable object.
  idx = 1;
  while (idx < mem->mp_max) {
    atomic_store(&mem->mp_mem[(idx - 1) * mem->mp_obj], idx); 
    idx += 1;
  }

  return true;
}

/// Free resources of an existing memory pool.
void mempool_del(
  struct mempool* mem) // Memory pool.
{
  free(mem->mp_mem);
  mem->mp_mem = NULL;
  mem->mp_obj = 0;
  mem->mp_max = 0;
  atomic_store(&mem->mp_cnt, 0);
  atomic_store(&mem->mp_nxt, 0);
}

/// Acquire an object from the memory pool.
MEMPOOL_INT mempool_acq(
  struct mempool *const mem) // Memory pool.
{
  _Atomic MEMPOOL_INT2 old;    // Existing `mp_nxt` value.
  _Atomic MEMPOOL_INT2 new;    // Proposed `mp_nxt` value.
          MEMPOOL_INT  val[2]; // Decomposition of `mp_nxt`.
          MEMPOOL_INT  cnt;    // Current value count.
          bool         ret;    // Return value.

  // Attempt to increase the number of used cells from the pool. In case this exceeds the available
  // count, the counter is subsequently decremented and a NULL address is returned. This section can
  // be access by any number of threads, as the counter always recovers to the intended value.
  cnt = atomic_fetch_add(&mem->mp_cnt, 1);
  if (cnt >= mem->mp_max) {
    atomic_fetch_sub(&mem->mp_cnt, 1);
    return 0;
  }

  // At this point a limited number of threads is allowed to enter here. The value in the next index
  // is exchanged for the actual value stored in that particular cell, whereas the old one is
  // returned to be further processed.
  while (true) {
    old = atomic_load(&mem->mp_nxt);

    (void)memcpy(val, &old, sizeof(MEMPOOL_INT2));
    val[0]  = mem->mp_mem[val[0] * mem->mp_obj];
    val[1] += 1;
    (void)memcpy(&new, val, sizeof(MEMPOOL_INT2));

    ret = atomic_compare_exchange_weak(&mem->mp_nxt, &old, new);
    if (ret == true) {
      break;
    }
  }

  // Retrieve the previous object index to give out to the caller. All indices returned to the user
  // are offset by one, so that zero can be treated as the failure value.
  (void)memcpy(val, &old, sizeof(MEMPOOL_INT2));
  return val[0] + 1;
}

/// Release an object back to the pool.
void mempool_rel(
        struct mempool *const mem, // Memory pool.
  const MEMPOOL_INT           idx) // Object index.
{
  _Atomic MEMPOOL_INT2 old;    // Existing `mp_nxt` value.
  _Atomic MEMPOOL_INT2 new;    // Proposed `mp_nxt` value.
          MEMPOOL_INT  val[2]; // Decomposition of `mp_nxt`.
          bool         ret;    // Return value.

  while (true) {
    old = atomic_load(&mem->mp_nxt);

    (void)memcpy(val, &old, sizeof(MEMPOOL_INT2));
    atomic_store(&mem->mp_mem[(idx - 1) * mem->mp_obj], val[0]);
    val[0]  = idx - 1;
    val[1] += 1;
    (void)memcpy(&new, val, sizeof(MEMPOOL_INT2));

    ret = atomic_compare_exchange_weak(&mem->mp_nxt, &old, new);
    if (ret == true) {
      break;
    }
  }

  // Reflect the return on the available number of objects.
  (void)atomic_fetch_sub(&mem->mp_cnt, 1);
}

/// Convert a cell index to the respective memory pointer.
//_Atomic MEMPOOL_INT*
//mempool_ptr(const struct mempool* pool, const MEMPOOL_INT idx)
//{
//  // Ensure that the index is a valid one.
//  if (idx == 0) {
//    return NULL;
//  }
//
//  // Ensure that the index is within bounds.
//  if ((idx - 1) > pool->mp_full) {
//    return NULL;
//  }
// 
//  // Compute the appropriate offset.
//  return &pool->mp_data[(idx - 1) * pool->mp_size];
//}

/// Read an integer from an object stored in the pool.
MEMPOOL_INT mempool_get(
        struct mempool *const mem, // Memory pool.
  const MEMPOOL_INT           idx, // Object index.
  const MEMPOOL_INT           off) // Integer offset within the object.
{
  return atomic_load(&mem->mp_mem[(idx - 1) * mem->mp_obj + off]);
}

/// Write an integer to an object stored in the pool.
void mempool_put(
        struct mempool *const mem, // Memory pool.
  const MEMPOOL_INT           idx, // Object index.
  const MEMPOOL_INT           off, // Integer offset within the object.
  const MEMPOOL_INT           val) // Arbitrary integer value.
{
  atomic_store(&mem->mp_mem[(idx - 1) * mem->mp_obj + off], val);
}
