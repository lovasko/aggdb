// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdlib.h>

#include "mphfunc/mphfunc.h"


#define MPHFUNC_GPAR 0xa66db // AGGDB in hexadecimal.

/// Bucket description.
struct mphfunc_bkt {
  MPHFUNC_INT mb_val; // Hash value.
  MPHFUNC_INT mb_len; // Number of integers in the bucket.
  MPHFUNC_INT mb_par; // Bucket hash function parameter.
};

/// Mapping function context.
struct mphfunc_ctx {
  struct mphfunc*     mc_mph; // Minimal perfect hash function.
  struct mphfunc_bkt* mc_bkt; // Bucket or buckets.
  MPHFUNC_INT*        mc_out; // Output values.
  MPHFUNC_INT         mc_idx; // Array index.
};

/// Comparator ordering buckets in descending order based on bucket size.
static int mphfunc_ord_len(
  const void* fst, // First bucket.
  const void* snd) // Second bucket.
{
  MPHFUNC_INT val[2]; // Integer values.

  // Unwrap the arguments and extract the relevant values.
  val[0] = ((const struct mphfunc_bkt*)fst)->mb_len;
  val[1] = ((const struct mphfunc_bkt*)snd)->mb_len;

  return (val[1] > val[0]) - (val[1] < val[0]);
}

/// Comparator ordering buckets in ascending order based on bucket hash value.
static int mphfunc_ord_val(
  const void* fst, // First bucket.
  const void* snd) // Second bucket.
{
  MPHFUNC_INT val[2]; // Integer values.

  // Unwrap the arguments and extract the relevant values.
  val[0] = ((const struct mphfunc_bkt*)fst)->mb_val;
  val[1] = ((const struct mphfunc_bkt*)snd)->mb_val;

  return (val[0] > val[1]) - (val[0] < val[1]);
}

/// Murmur2/Murmur3 hashing function.
#if   MPHFUNC_INT_BIT == 32
static MPHFUNC_INT mphfunc_mur(
  const MPHFUNC_INT inp, // Input value.
  const MPHFUNC_INT par) // Seed parameter.
{
  MPHFUNC_INT val; // Value mutations.
  MPHFUNC_INT out; // Output value.

  // Perform mutations on the input value.
  val = inp;
  val *= 0x5bd1e995;
  val ^= val >> 24;
  val *= 0x5bd1e995;

  // Mix the value with the parameter.
  out  = par ^ 4;
  out *= 0x5bd1e995;
  out ^= val;
  out ^= out >> 13;
  out *= 0x5bd1e995;
  out ^= out >> 15;

  return out;
} 
#elif MPHFUNC_INT_BIT == 64
static MPHFUNC_INT mphfunc_mur(
  const MPHFUNC_INT inp, // Integer value.
  const MPHFUNC_INT par) // Seed parameter.
{
  MPHFUNC_INT val; // Value mutations.
  MPHFUNC_INT out; // Output value.

  // Perform mutations on the input value.
  val  = inp;
  val *= 0xc6a4a7935bd1e995;
  val ^= val >> 47;
  val *= 0xc6a4a7935bd1e995;

  // Mix the value with the parameter.
  out  = par ^ (8 * 0xc6a4a7935bd1e995); 
  out ^= val;
  out *= 0xc6a4a7935bd1e995;
  out ^= out >> 47;
  out *= 0xc6a4a7935bd1e995;
  out ^= out >> 47;

  return out;
}
#else
  #error "invalid bit count"
#endif

/// Count the number of integers in each bucket.
static bool mphfunc_cnt(
  MPHFUNC_INT idx, // Index of the value.
  MPHFUNC_INT val, // Integer value.
  void*       ptr) // Mapping context.
{
  struct mphfunc_ctx* ctx; // Mapping context.
  MPHFUNC_INT         bix; // Bucket index.


  // Explicitly ignore the unused argument.
  (void)idx;

  // Unpack the context.
  ctx = (struct mphfunc_ctx*)ptr;
  
  // Apply the function to retrieve the bucket number.
  bix = mphfunc_mur(val, MPHFUNC_GPAR) % ctx->mc_mph->mf_cnt;
  
  // Note the occurrence.
  ctx->mc_bkt[bix].mb_len += 1;

  // Always continue the traversal.
  return true;
}

/// Extract numbers that match a particular bucket.
static bool mphfunc_ext(
  MPHFUNC_INT idx, // Index of the value.
  MPHFUNC_INT val, // Integer value.
  void*       ptr) // Mapping context.
{
  struct mphfunc_ctx* ctx; // Mapping context.
  MPHFUNC_INT         bix; // Bucket index.

  // Explicitly ignore the unused argument.
  (void)idx;

  // Unpack the context.
  ctx = (struct mphfunc_ctx*)ptr;
  
  // Determine the bucket index.
  bix = mphfunc_mur(val, MPHFUNC_GPAR) % ctx->mc_mph->mf_cnt;
  
  // Append the value in case of a match.
  if (bix == ctx->mc_bkt->mb_val) {
    ctx->mc_out[ctx->mc_idx] = val;
    ctx->mc_idx             += 1;
  }
  
  // Always continue the traversal.
  return true;
}

/// Generate a minimal perfect hash function from the given data.
bool mphfunc_new(
        struct mphfunc *const restrict mph, // Minimal perfect hash function.
  const MPHFUNC_INT                    cnt, // Number of buckets.
  const MPHFUNC_INT                    ext, // Extension length.
        struct zipints *const restrict zip) // Encoded unsigned integers.
{
  struct mphfunc_bkt* bkt; // Buckets.
  struct mphfunc_ctx  ctx; // Mapping context.
  struct bittape      gum; // Global usage marks.
  struct bittape      lum; // Local usage marks.
  MPHFUNC_INT         bix; // Bucket index.
  MPHFUNC_INT         vix; // Integer value index.
  MPHFUNC_INT         pop; // Population count.
  MPHFUNC_INT         sim; // Number of similarities.
  MPHFUNC_INT         len; // Number of values.
  MPHFUNC_INT         par; // Number of values.
  MPHFUNC_INT         val; // Hash value.
  bool                ret; // Return value.
  bool                suc; // Success/failure indication.

  // Early exit if no buckets are selected.
  if (cnt == 0) {
    return false;
  }

  // Retrieve the number of integers.
  zipints_len(zip, &len);

  // Fill in the structure.
  mph->mf_len = len + ext;
  mph->mf_cnt = cnt;

  // Allocate memory for bucket descriptions.
  bkt = calloc(cnt, sizeof(struct mphfunc_bkt));
  if (bkt == NULL) {
    return false;
  }

  bix = 0;
  while (bix < cnt) {
    bkt[bix].mb_val = bix;
    bix += 1;
  }

  // Compute the integer counts for each bucket.
  ctx.mc_mph = mph;
  ctx.mc_bkt = bkt;
  ctx.mc_out = NULL;
  ctx.mc_idx = 0;
  zipints_map(zip, mphfunc_cnt, &ctx);

  // Sort the buckets in descending order based on the integer counts.
  qsort(bkt, cnt, sizeof(struct mphfunc_bkt), mphfunc_ord_len);

  // Allocate memory for the usage marks that prevent collisions. The number of
  // usage marks is the same as the number of input integers, as the load
  // factor of the hash function is set to be 100%.
  ret = bittape_new(&gum, mph->mf_len);
  if (ret == false) {
    free(bkt);
    return false;
  }

  ret = bittape_new(&lum, mph->mf_len);
  if (ret == false) {
    bittape_del(&gum);
    free(bkt);
    return false;
  }
  
  // Search for a second hash function for each bucket such that it maps to a
  // unique value within the global usage marks.
  suc = false;
  bittape_nul(&gum);
  bix = 0;
  while (bix < cnt) {
    // Skip the bucket if no values fell into it.
    if (bkt[bix].mb_len == 0) {
      bix += 1;
      continue;
    }

    // Allocate memory for the values from the bucket.
    ctx.mc_out = calloc(bkt[bix].mb_len, sizeof(MPHFUNC_INT));  
    if (ctx.mc_out == NULL) {
      free(bkt);
      bittape_del(&gum);
      bittape_del(&lum);
      return false;
    }

    // Extract the exact integer values that belong to that bucket.
    ctx.mc_mph = mph;
    ctx.mc_bkt = &bkt[bix];
    ctx.mc_idx = 0;
    printf("values in this bucket = ");
    zipints_map(zip, mphfunc_ext, &ctx);
    printf("\n");

    // Iterate through  
    par = 0;
    while (par < MPHFUNC_INT_MAX) {
      printf("trying parameter %" PRIuMAX "\n", (uintmax_t)par);
      // Clear the local usage marks used for the second hash function.
      bittape_nul(&lum);

      // Compute the hash function for each of the numbers.
      vix = 0;
      while (vix < bkt[bix].mb_len) {
        val = mphfunc_mur(ctx.mc_out[vix], par) % mph->mf_len;
        bittape_set1(&lum, val);
        vix += 1;
      }

      // Ensure that the hash function did not produce any collisions
      // within the local bucket. 
      bittape_pop(&lum, &pop);
      if (pop != bkt[bix].mb_len) {
        par += 1;
        continue;
      }

      // Ensure that the hash function does not collide with the global usage
      // marks, merged from all previous buckets.
      (void)bittape_sim(&lum, &gum, &sim);
      if (sim != 0) {
        par += 1;
        continue;
      }

      // By reaching this point the local hash function does appear to be
      // matching the 
      suc = true;
      break;
    }
    
    // Commit the local usage marks globally.
    (void)bittape_add(&gum, &lum);
    bkt[bix].mb_par = par;

    // Release resources for the bucket values.
    free(ctx.mc_out);
    
    // Advance to the next bucket.
    bix += 1;
  }

  // Verify that the function was found.
  if (suc == false) {
    free(bkt);
    bittape_del(&gum);
    bittape_del(&lum);
    return false;
  }

  // Sort the buckets back to their original ordering based on the hash value.
  qsort(bkt, cnt, sizeof(struct mphfunc_bkt), mphfunc_ord_val);

  // Allocate memory for the final set of parameters.
  mph->mf_par = calloc(cnt, sizeof(MPHFUNC_INT));
  if (mph->mf_par == NULL) {
    free(bkt);
    bittape_del(&gum);
    bittape_del(&lum);
    return false;
  }
  
  // Copy the bucket parameters to the parameter array.
  bix = 0;
  while (bix < cnt) {
    mph->mf_par[bix] = bkt[bix].mb_par;
    bix += 1;
  }

  // Free all auxiliary resources.
  free(bkt);
  bittape_del(&gum);
  bittape_del(&lum);

  return true;
}

/// Retrieve the index of a value.
void mphfunc_idx(
        struct mphfunc *const restrict mph, // Minimal perfect hash function.
  const MPHFUNC_INT                    val, // Value to search for.
        MPHFUNC_INT    *const restrict out) // Output index.
{
  MPHFUNC_INT bix; // Bucket index.
  MPHFUNC_INT vix; // Value index.

  // Resolve the bucket of the value.
  bix = mphfunc_mur(val, MPHFUNC_GPAR) % mph->mf_cnt;
  
  // Perform the lookup with the second function. If the output falls outside
  // of the array, the value was not in the input array.
  vix = mphfunc_mur(val, mph->mf_par[bix]) % mph->mf_len;
  *out = vix;
}

/// Release resources held by the hash function.
void mphfunc_del(
  struct mphfunc* mph) // Minimal perfect hash function.
{
  free(mph->mf_par);
}
