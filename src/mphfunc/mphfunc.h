// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdbool.h>
#include <stdint.h>

#include "zipints/zipints.h"


#ifndef MPHFUNC_H
#define MPHFUNC_H

#ifndef MPHFUNC_INT_BIT
  #error "must define MPHFUNC_INT_BIT to select integer width"
#endif

// Select the appropriate integer type.
#if   MPHFUNC_INT_BIT == 32
  #define MPHFUNC_INT     uint32_t
  #define MPHFUNC_INT_MAX UINT32_MAX

#elif MPHFUNC_INT_BIT == 64
  #define MPHFUNC_INT     uint64_t
  #define MPHFUNC_INT_MAX UINT64_MAX

#else
  #define _str(x) #x
  #define str(x) _str(x)
  #error "MPHFUNC_INT_BIT has unsupported value: " str(MPHFUNC_INT_BIT) " [32, 64]"
#endif

/// Minimal perfect hash function.
struct mphfunc {
  MPHFUNC_INT* mf_par; // Array of second function parameters.
  MPHFUNC_INT  mf_len; // Number of integers (plus the extension).
  MPHFUNC_INT  mf_cnt; // Number of buckets.
};

/// Generate a minimal perfect hash function from the given data.
bool mphfunc_new(
        struct mphfunc *const restrict mph,  // Minimal perfect hash function.
  const MPHFUNC_INT                    cnt,  // Number of buckets.
  const MPHFUNC_INT                    ext,  // Extension length.
        struct zipints *const restrict zip); // Input values.

/// Retrieve the index of a value.
void mphfunc_idx(
        struct mphfunc *const restrict mph,  // Minimal perfect hash function.
  const MPHFUNC_INT                    val,  // Value to search for.
        MPHFUNC_INT    *const restrict out); // Output index.

/// Release resources held by the hash function.
void mphfunc_del(
  struct mphfunc* mph); // Minimal perfect hash function.

#endif
