// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <inttypes.h>

#include "zipints/zipints.h"
#include "bitutil/bitutil.h"


/// Encode a single pair of a gap and a run.
static bool put(
  struct zipints *const restrict zip, // Compressed unsigned integers.
  struct bithead *const restrict hpo, // High bits reading head.
  const ZIPINTS_INT run, // Length of the run.
  const ZIPINTS_INT dif) // Value difference between this and the previous run.
{
  bool ret; // Return value.

  // Encode the gap between the current and the previous high bits.
  if (dif > 0) {
    ret = bittape_put0(&zip->zi_hbt, hpo, dif - 1);
    if (ret == false) {
      return false;
    }
  }
      
  // Serialise the run length as unary encoding.
  ret = bittape_put1(&zip->zi_hbt, hpo, run);
  if (ret == false) {
    return false;
  }
  ret = bittape_put(&zip->zi_hbt, hpo, 1, 0);
  if (ret == false) {
    return false;
  }

  return true;
}

/// Compute the split between the low and high bits.
static ZIPINTS_INT spl(
  const ZIPINTS_INT *const inp, // Input array.
  const ZIPINTS_INT        len) // Length of the input array.
{  
  ZIPINTS_INT uni; // Universe of the values (range).

  uni = inp[len - 1] - inp[0];
  if (uni > len) {
    return ZIPINTS_INT_BIT - bitutil_clz(uni / len);
  } else {
    // Ensure that the split is always at least one, so that the bit tape containing the lower bits
    // does not remain empty.
    return 1;
  }
}

/// Encode an array of sorted unsigned integers using the Elias Fano encoding.
bool zipints_put(
        struct zipints *const restrict zip, // Compressed unsigned integers.
  const ZIPINTS_INT    *const restrict inp, // Input array.
  const ZIPINTS_INT                    len) // Length of the input array.
{
  struct bithead hpo;
  struct bithead lpo;
  ZIPINTS_INT idx; // Array index.
  ZIPINTS_INT lob; // Low bits.
  ZIPINTS_INT hib; // High bits.
  ZIPINTS_INT run; // Number of repeated high bits.
  ZIPINTS_INT lst; // Last high bits within the same run.
  ZIPINTS_INT prv; // Previous value of high bits.
  ZIPINTS_INT val; // Value to encode.
  bool        ret; // Return value.

  // Handle the edge case of an empty array.
  if (len == 0) {
    zip->zi_len = 0;
    return true;
  }

  // Otherwise continue with the generic algorithm.
  zip->zi_len = len;
  zip->zi_max = inp[len - 1];
  zip->zi_off = inp[0];

  // Determine the split between low and high bits.
  zip->zi_spl = spl(inp, len);

  // Allocate storage for low bits.
  ret = bittape_new(&zip->zi_lbt, len * zip->zi_spl);
  if (ret == false) {
    return false;
  }

  // Allocate storage for high bits.
  //ret = bittape_new(&zip->zi_hbt, len + (uni >> zip->zi_sep) + 2);
  ret = bittape_new(&zip->zi_hbt, len * 2);
  if (ret == false) {
    return false;
  }

  // Reset the reading heads for both bit tapes.
  bithead_nul(&hpo);
  bithead_nul(&lpo);

  // The encoding process starts without a run, and implicitly assumes that the current high bits
  // value is zero, and that the previous was also zero. This prevents inserting the predecessor
  // bits at the beginning of the high bit tape.
  run = 0;
  lst = 0;
  prv = 0;

  // Traverse the array an encode the integers.
  idx = 0;
  while (idx < len) {
    // Split the value into low and high bits.
    val = inp[idx] - zip->zi_off;
    lob = val & (((ZIPINTS_INT)1 << zip->zi_spl) - 1);
    hib = val >> zip->zi_spl;

    // Encode the low bits explicitly.
    ret = bittape_put(&zip->zi_lbt, &lpo, zip->zi_spl, lob);
    if (ret == false) {
      return false;
    }

    // implicit start is at all high bits being zero. if the first upper bits are NOT zero, so the
    // prv != 

    // We compute the bits to store by unary encoding the run only when it ends.
    if (lst == hib) {
      run += 1;
    } else {
      ret = put(zip, &hpo, run, lst - prv);
      if (ret == false) {
        return false;
      }

      // Reset the run length and assign the current high bit value as the previous one.
      run = 1;
      prv = lst;
    }

    // Update the last seen high value.
    lst = hib;

    // Advance to the next integer.
    idx += 1;
  }

  // Write the final run to the upper bits tape.
  ret = put(zip, &hpo, run, hib - prv);
  if (ret == false) {
    return false;
  }

  // Trim the allocated memory for the higher bits.
  ret = bittape_cut(&zip->zi_hbt);
  if (ret == false) {
    return false;
  }

  return true;
}

/// Decode Elias Fano encoded unsigned integers into a sorted array.
bool zipints_get(
  const struct zipints *const restrict zip, // Compressed unsigned integers.
        ZIPINTS_INT    *const restrict out, // Output array.
        ZIPINTS_INT    *const restrict len) // Length of the output array.
{
  struct bithead hpo; // High bits reading head.
  struct bithead lpo; // Low bits reading head.
  ZIPINTS_INT idx; // Integer index.
  ZIPINTS_INT rep; // Repetition counter.
  ZIPINTS_INT hib; // High bits.
  ZIPINTS_INT lob; // Low bits.
  ZIPINTS_INT cnt; // Bit count.
  bool        ret; // Return value.

  // Reset the reading heads for both lower and higher bits.
  bithead_nul(&hpo);
  bithead_nul(&lpo);

  // The assumed high bits at the beginning are all zeros.
  hib = 0;

  // Traverse all stored integers.
  idx = 0;
  while (idx < zip->zi_len) {
    // Retrieve the number of set bits indicating the number of repetitions of higher bits.
    ret = bittape_get1(&zip->zi_hbt, &hpo, &cnt);
    if (ret == false) {
      return false;
    }

    // Repeatedly apply the high bits to the next set of low bits.
    rep = 0;
    while (rep < cnt) {
      // Retrieve the low bits.
      ret = bittape_get(&zip->zi_lbt, &lpo, zip->zi_spl, &lob);
      if (ret == false) {
        return false;
      }

      // Reconstruct the original value.
      out[idx] = zip->zi_off + (lob | (hib << zip->zi_spl));

      // Advance to the next repetition and value.
      idx += 1;
      rep += 1;
    }

    // Retrieve the number zeros, signifying the gap towards the next high bits.
    ret = bittape_get0(&zip->zi_hbt, &hpo, &cnt);
    if (ret == false) {
      return false;
    }

    // Apply the gap skip.
    hib += cnt;
  }

  // Provide the number of decoded integers.
  *len = zip->zi_len;
  return true;
}

/// Retrieve the number of encoded integers.
void zipints_len(
  const struct zipints *const restrict zip, // Compressed unsigned integers.
        ZIPINTS_INT    *const restrict out) // Number of unsigned integers.
{
  *out = zip->zi_len;
}

/// Execute a function for all integers bound by a common context.
bool zipints_map(
  struct zipints* zip,                                   // Compressed unsigned integers.
  bool          (*fun)(ZIPINTS_INT, ZIPINTS_INT, void*), // Function to execute.
  void*           ctx)                                   // Context.
{
  struct bithead hpo; // High bits reading head.
  struct bithead lpo; // Low bits reading head.
  ZIPINTS_INT idx; // Integer index.
  ZIPINTS_INT rep; // Repetition counter.
  ZIPINTS_INT hib; // High bits.
  ZIPINTS_INT lob; // Low bits.
  ZIPINTS_INT val; // Integer value.
  ZIPINTS_INT cnt; // Bit count.
  bool        ret; // Return value.

  // Reset the reading heads for both lower and higher bits.
  bithead_nul(&lpo);
  bithead_nul(&hpo);

  // The assumed high bits at the beginning are all zeros.
  hib = 0;

  // Traverse all stored integers.
  idx = 0;
  while (idx < zip->zi_len) {
    // Retrieve the number of set bits indicating the number of repetitions of higher bits.
    ret = bittape_get1(&zip->zi_hbt, &hpo, &cnt);
    if (ret == false) {
      return false;
    }

    // Repeatedly apply the high bits to the next set of low bits.
    rep = 0;
    while (rep < cnt) {
      // Retrieve the low bits.
      ret = bittape_get(&zip->zi_lbt, &lpo, zip->zi_spl, &lob);
      if (ret == false) {
        return false;
      }

      // Reconstruct the original value.
      val = zip->zi_off + (lob | (hib << zip->zi_spl));

      // Execute the function and optionally terminate the traversal.
      ret = fun(idx, val, ctx);
      if (ret == false) {
        return true;
      }

      // Advance to the next repetition and value.
      idx += 1;
      rep += 1;
    }

    // Retrieve the number zeros, signifying the gap towards the next high bits.
    ret = bittape_get0(&zip->zi_hbt, &hpo, &cnt);
    if (ret == false) {
      return false;
    }

    // Apply the gap skip.
    hib += cnt;
  }

  return true;
}

/// Obtain the index of a value.
bool zipints_idx(
  const struct zipints *const restrict zip, // Compressed unsigned integers.
  const ZIPINTS_INT                    val, // Value to search for.
        ZIPINTS_INT    *const restrict out) // Index of the value.
{
  struct bithead lpo; // Low head reading head.
  ZIPINTS_INT    idx; // Value index.
  ZIPINTS_INT    hib; // High bits of the value.
  ZIPINTS_INT    lob; // Low bits of the value.
  ZIPINTS_INT    cur; // Current low bits.
  ZIPINTS_INT    prv; // Previous low bits.
  ZIPINTS_INT    inp;
  bool           ret; // Return value.

  // Apply the offset to the value.
  if (val < zip->zi_off) {
    return false;
  }
  inp = val - zip->zi_off;

  // Ensure that the value is within the stored bounds.
  if (inp > zip->zi_max) {
    return false;
  }

  // Split the value into lower and upper bits.
  lob = inp & (((ZIPINTS_INT)1 << zip->zi_spl) - 1);
  hib = inp >> zip->zi_spl;

  // Locate the index of the upper bits first.
  if (hib == 0) {
    idx = 0;
  } else {
    ret = bittape_sel0(&zip->zi_hbt, hib - 1, &idx);
    if (ret == false) {
      return false;
    }

    idx -= hib;
    idx += 1;
  }

  // Iterate through the lower bits to find a match with the value's low bits. If the next set of
  // lower bits is lesser than the currently visited one, the end of the current high bit index is
  // reached and the value is not found.
  prv = 0;
  bithead_nul(&lpo);
  bithead_fwd(&lpo, idx  * zip->zi_spl, 0);

  while (true) {
    // Retrieve lower bits. In case there are no further bits to explore, terminate the search as
    // there is no match.
    ret = bittape_get(&zip->zi_lbt, &lpo, zip->zi_spl, &cur);
    if (ret == false) {
      return false;
    }

    // Compare for equivalence, possibly resulting in a match.
    if (cur == lob) {
      *out = idx;
      return true;
    }

    // Terminate the search if the current set of lower bits is less than the previous one,
    // indicating that the search has reached the next high bits index.
    if (cur < prv) {
      return false;
    } else {
      prv = cur;
    }

    // Advance to the next value.
    idx += 1;
  }

  // Report no match.
  return false;
}

/// Retrieve the n-th consecutive value.
bool zipints_nth(
  const struct zipints *const restrict zip, // Compressed unsigned integers.
  const ZIPINTS_INT                    nth, // Index of the value.
        ZIPINTS_INT    *const restrict out) // Output value.
{
  struct bithead pos; // Reading and writing heads.
  ZIPINTS_INT    lob; // Lower bits.
  ZIPINTS_INT    hib; // Higher bits.
  bool           ret; // Return value.

  // Early exit upon an unsuccessful bounds check.
  if (nth > zip->zi_len - 1) {
    return false;
  }

  // Retrieve the lower bits explicitly.
  bithead_set(&pos, nth * zip->zi_spl, 0);
  ret = bittape_get(&zip->zi_lbt, &pos, zip->zi_spl, &lob);
  if (ret == false) {
    return false;
  }

  // The higher bits are computed by juxtaposing the index and the number of set bits.
  ret = bittape_sel1(&zip->zi_hbt, nth, &hib);
  if (ret == false) {
    return false;
  }
  hib -= nth;

  // Reassemble the value.
  *out = zip->zi_off + (lob | (hib << zip->zi_spl));

  return true;
}

/// Free resources used by the compressed integers.
void zipints_del(
  struct zipints *const zip)
{
  bittape_del(&zip->zi_lbt);
  bittape_del(&zip->zi_hbt);
}
