// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef ZIPINTS_H
#define ZIPINTS_H

#include <stdbool.h>
#include <stdint.h>

#include "bittape/bittape.h"


// Set the default bit width.
#ifndef ZIPINTS_INT_BIT
  #error "must define ZIPINTS_INT_BIT to set the bit width [16, 32, 64]"
#endif

// Select the appropriate integer based on the selected bit width.
#if   ZIPINTS_INT_BIT == 16
  #define ZIPINTS_INT     uint16_t
  #define ZIPINTS_INT_FMT PRIu16
  #define ZIPINTS_INT_SCN SCNu16

#elif ZIPINTS_INT_BIT == 32
  #define ZIPINTS_INT     uint32_t
  #define ZIPINTS_INT_FMT PRIu32
  #define ZIPINTS_INT_SCN SCNu32

#elif ZIPINTS_INT_BIT == 64
  #define ZIPINTS_INT     uint64_t
  #define ZIPINTS_INT_FMT PRIu64
  #define ZIPINTS_INT_SCN SCNu64

#else
  #define _str(x) #x
  #define str(x) _str(x)
  #error "invalid value for ZIPINTS_INT_BIT: '" str(ZIPINTS_INT_BIT) "'"
#endif

// Compressed array of sorted unsigned integers.
struct zipints {
  struct bittape zi_lbt; // Bit tape for low bits.
  struct bittape zi_hbt; // Bit tape for high bits.
  ZIPINTS_INT    zi_spl; // Index of split between low and high bits.
  ZIPINTS_INT    zi_len; // Number of integers stored.
  ZIPINTS_INT    zi_max; // Maximal stored value.
  ZIPINTS_INT    zi_off; // Offset of values.
};

/// Encode an array of integers.
bool zipints_put(
        struct zipints *const restrict zip,
  const ZIPINTS_INT    *const restrict inp,
  const ZIPINTS_INT                    len);

/// Decode an array of integers.
bool zipints_get(
  const struct zipints *const restrict zip,
        ZIPINTS_INT    *const restrict out,
        ZIPINTS_INT    *const restrict len);

/// Retrieve the number of encoded integers.
void zipints_len(
  const struct zipints *const restrict zip,
        ZIPINTS_INT    *const restrict out);

/// Execute a function for all integers bound by a common context.
bool zipints_map(
  struct zipints* zip,
  bool          (*fun)(ZIPINTS_INT, ZIPINTS_INT, void*),
  void*           ctx);

/// Retrieve the index of a value.
bool zipints_idx(
  const struct zipints *const restrict zip,
  const ZIPINTS_INT                    val,
        ZIPINTS_INT    *const restrict out);

/// Retrieve the n-th consecutive value.
bool zipints_nth(
  const struct zipints *const restrict zip,
  const ZIPINTS_INT                    nth,
        ZIPINTS_INT    *const restrict out);

/// Free all resources held for the encoding.
void zipints_del(
  struct zipints* zip);

#endif
