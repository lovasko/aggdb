// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <tgmath.h>
#include <string.h>
#include <stddef.h>

#include "aggstat.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  COMMON                                                                                   ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
static AGGSTAT_FLT cut(
        AGGSTAT_FLT inp,
  const AGGSTAT_INT len)
{
  AGGSTAT_FLT_INT val; // Integer storage.
  AGGSTAT_FLT     out; // Output value.

  // Copy the bits and apply the bit mask.
  (void)memcpy(&val, &inp, sizeof(AGGSTAT_FLT_INT));
  val &= (~(AGGSTAT_FLT_INT)0) << len;
  (void)memcpy(&out, &val, sizeof(AGGSTAT_FLT));

  return out;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  FST (First)                                                                              ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_fst_new(
  struct aggstat_fst *const agg) // Aggregate function.
{
  agg->af_val = AGGSTAT_FLT_NAN;
}

/// Update the aggregate function.
void aggstat_fst_put(
        struct aggstat_fst* agg, // Aggregate function.
  const AGGSTAT_FLT         inp) // Input value.
{
  // Only assign once when the current value is not a NaN.
  if (AGGSTAT_FLT_TNAN(agg->af_val)) {
    agg->af_val = inp;
  }
}

/// Obtain the current aggregate value.
bool aggstat_fst_get(
  const struct aggstat_fst *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->af_val;
  return (!AGGSTAT_FLT_TNAN(agg->af_val));
}

void aggstat_fst_cut(
  struct aggstat_fst *const agg,
  const AGGSTAT_INT len)
{
  agg->af_val = cut(agg->af_val, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  LST (Last)                                                                               ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_lst_new(
  struct aggstat_lst *const agg) // Aggregate function.
{
  agg->al_val = AGGSTAT_FLT_NAN;
}

/// Update the aggregate value.
void aggstat_lst_put(
        struct aggstat_lst* agg, // Aggregate function.
  const AGGSTAT_FLT         inp) // Input value.
{
  agg->al_val = inp;
}

/// Obtain the current aggregate value.
bool aggstat_lst_get(
  const struct aggstat_lst *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->al_val;
  return (!AGGSTAT_FLT_TNAN(agg->al_val));
}

void aggstat_lst_cut(
  struct aggstat_lst *const agg,
  const AGGSTAT_INT len)
{
  agg->al_val = cut(agg->al_val, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  CNT (Count)                                                                              ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_cnt_new(
  struct aggstat_cnt *const agg) // Aggregate function.
{
  agg->ac_val = 0;
}

/// Update the aggregate value.
void aggstat_cnt_put(
        struct aggstat_cnt* agg, // Aggregate function.
  const AGGSTAT_FLT         inp) // Input value.
{
  (void)inp;
  agg->ac_val += 1;
}

/// Obtain the current aggregate value.
bool aggstat_cnt_get(
  const struct aggstat_cnt *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = (AGGSTAT_FLT)agg->ac_val;
  return true;
}

void aggstat_cnt_cut(
  struct aggstat_cnt *const agg,
  const AGGSTAT_INT len)
{
  // As the state of the count function does not contain any floats, this function is a no-op.
  (void)agg;
  (void)len;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  SUM (Summation)                                                                          ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_sum_new(
  struct aggstat_sum *const agg) // Aggregate function.
{
  agg->as_val = AGGSTAT_FLT_0_0;
}

/// Update the streaming aggregate function value.
void aggstat_sum_put(
        struct aggstat_sum* agg, // Aggregate function.
  const AGGSTAT_FLT         inp) // Input value.
{
  agg->as_val += inp;
}

/// Obtain the current aggregate value.
bool aggstat_sum_get(
  const struct aggstat_sum *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->as_val;
  return true;
}

void aggstat_sum_cut(
  struct aggstat_sum *const agg,
  const AGGSTAT_INT len)
{
  agg->as_val = cut(agg->as_val, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  MIN (Minimum)                                                                            ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_min_new(
  struct aggstat_min *const agg) // Aggregate function.
{
  agg->an_val = AGGSTAT_FLT_MAX;
}

/// Update the aggregate value.
void aggstat_min_put(
        struct aggstat_min *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  agg->an_val = AGGSTAT_FLT_FMIN(inp, agg->an_val);
}

/// Obtain the current aggregate value.
bool aggstat_min_get(
  const struct aggstat_min *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->an_val;
  return (agg->an_val != AGGSTAT_FLT_MAX);
}

void aggstat_min_cut(
  struct aggstat_min *const agg,
  const AGGSTAT_INT len)
{
  agg->an_val = cut(agg->an_val, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  MAX (Maximum)                                                                            ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_max_new(
  struct aggstat_max *const agg) // Aggregate function.
{
  agg->ax_val = AGGSTAT_FLT_MIN;
}

/// Update the aggregate value.
void aggstat_max_put(
        struct aggstat_max *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  agg->ax_val = AGGSTAT_FLT_FMAX(agg->ax_val, inp);
}

/// Obtain the current aggregate value.
bool aggstat_max_get(
  const struct aggstat_max *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->ax_val;
  return (agg->ax_val != AGGSTAT_FLT_MIN);
}

void aggstat_max_cut(
  struct aggstat_max *const agg,
  const AGGSTAT_INT len)
{
  agg->ax_val = cut(agg->ax_val, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  AVG (Arithmetic mean)                                                                    ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_avg_new(
  struct aggstat_avg *const agg) // Aggregate function.
{
  agg->aa_cnt = 0;
  agg->aa_fst = AGGSTAT_FLT_0_0;
}

/// Update the streaming aggregate function value.
void aggstat_avg_put(
        struct aggstat_avg *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  agg->aa_cnt += 1;
  agg->aa_fst += (inp - agg->aa_fst) / (AGGSTAT_FLT)(agg->aa_cnt);
}

/// Retrieve the current aggregate value.
bool aggstat_avg_get(
  const struct aggstat_avg *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = (AGGSTAT_FLT)agg->aa_fst;
  return (agg->aa_cnt > 0);
}

void aggstat_avg_cut(
  struct aggstat_avg *const agg,
  const AGGSTAT_INT len)
{
  agg->aa_fst = cut(agg->aa_fst, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  VAR (Variance)                                                                           ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_var_new(
  struct aggstat_var *const agg)
{
  agg->av_cnt = 0;
  agg->av_fst = AGGSTAT_FLT_0_0;
  agg->av_snd = AGGSTAT_FLT_0_0;
}

/// Update the streaming aggregate function value.
void aggstat_var_put(
        struct aggstat_var *const agg,
  const AGGSTAT_FLT               inp)
{
  AGGSTAT_FLT x;
  AGGSTAT_FLT y;
  AGGSTAT_FLT z;

  x = inp - agg->av_fst;
  y = x / (AGGSTAT_FLT)(agg->av_cnt + 1);

  #ifdef AGGSTAT_HERB
    z = x * (x / ((AGGSTAT_FLT)(agg->av_cnt + 1) / (AGGSTAT_FLT)agg->av_cnt));
  #else
    z = x * y * (AGGSTAT_FLT)agg->av_cnt;
  #endif

  agg->av_fst += y;
  agg->av_snd += z;
  agg->av_cnt += 1;
}

/// TODO: this function is violating the fast math where we tell the compiler that no infinite
///       values might appear, thus potentially breaking the code. is this a real issue? should it
///       perhaps instead do some fmax(cnt - 1, EPS) to have a non-zero divisor? this needs an
///       AGGSTAT_FLT_EPS value to be added
/// Retrieve the current aggregate.
bool aggstat_var_get(
  const struct aggstat_var *const restrict agg,
        AGGSTAT_FLT        *const restrict out)
{
  // The following potential division by zero might appear as a mistake; it is not. The division by
  // zero is a well-defined IEEE-745 operation yielding a positive/negative infinity. The value
  // itself in this case is wrong, but that does not matter, as the function returns `false` in
  // this case, meaning that the resulting value shall not be consulted.
  *out = agg->av_snd / (AGGSTAT_FLT)(agg->av_cnt - 1);
  return agg->av_cnt > 1;
}

void aggstat_var_cut(
  struct aggstat_var *const agg,
  const AGGSTAT_INT len)
{
  agg->av_fst = cut(agg->av_fst, len);
  agg->av_snd = cut(agg->av_snd, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  SKW (Skewness)                                                                           ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_skw_new(
  struct aggstat_skw *const agg) // Aggregate function.
{
  agg->aw_cnt = 0;
  agg->aw_fst = AGGSTAT_FLT_0_0;
  agg->aw_snd = AGGSTAT_FLT_0_0;
  agg->aw_trd = AGGSTAT_FLT_0_0;
}

/// Update the aggregate function.
void aggstat_skw_put(
        struct aggstat_skw *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  AGGSTAT_FLT x; // Helper variable.
  AGGSTAT_FLT y; // Helper variable.
  AGGSTAT_FLT z; // Helper variable.

  x = inp - agg->aw_fst;
  y = x / (AGGSTAT_FLT)(agg->aw_cnt + 1);
  z = x * y * (AGGSTAT_FLT)agg->aw_cnt;

  agg->aw_fst += y;
  agg->aw_trd += z               * y * (AGGSTAT_FLT)(agg->aw_cnt - 1)
               - AGGSTAT_FLT_3_0 * y * agg->aw_snd;
  agg->aw_snd += z;
  agg->aw_cnt += 1;
}

/// Obtain the current aggregate value.
bool aggstat_skw_get(
  const struct aggstat_skw *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = AGGSTAT_FLT_SQRT((AGGSTAT_FLT)agg->aw_cnt)
       * agg->aw_trd
       / AGGSTAT_FLT_POW(agg->aw_snd, AGGSTAT_FLT_1_5);

  return (agg->aw_cnt > 0);
}

void aggstat_skw_cut(
  struct aggstat_skw *const agg,
  const AGGSTAT_INT len)
{
  agg->aw_fst = cut(agg->aw_fst, len);
  agg->aw_snd = cut(agg->aw_snd, len);
  agg->aw_trd = cut(agg->aw_trd, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  KRT (Kurtosis)                                                                           ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_krt_new(
  struct aggstat_krt *const agg) // Aggregate function.
{
  agg->ak_cnt = 0;
  agg->ak_fst = AGGSTAT_FLT_0_0;
  agg->ak_snd = AGGSTAT_FLT_0_0;
  agg->ak_trd = AGGSTAT_FLT_0_0;
  agg->ak_fth = AGGSTAT_FLT_0_0;
}

/// Update the aggregate function. 
void aggstat_krt_put(
        struct aggstat_krt *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  AGGSTAT_FLT x;
  AGGSTAT_FLT y;
  AGGSTAT_FLT z;
  AGGSTAT_FLT w;

  x = inp - agg->ak_fst;
  y = x / (AGGSTAT_FLT)(agg->ak_cnt + 1);
  z = x * y * (AGGSTAT_FLT)agg->ak_cnt;
  w = (AGGSTAT_FLT)(agg->ak_cnt + 1);

  agg->ak_fst += y;
  agg->ak_fth += z 
               * y
               * y
               * (w * w - AGGSTAT_FLT_3_0 * w + AGGSTAT_FLT_3_0)
               + AGGSTAT_FLT_6_0 * y * y * agg->ak_snd
               - AGGSTAT_FLT_4_0 * y * agg->ak_trd;
  agg->ak_trd += z               * y * (AGGSTAT_FLT)(agg->ak_cnt - 1)
               - AGGSTAT_FLT_3_0 * y * agg->ak_snd;
  agg->ak_snd += z;
  agg->ak_cnt    += 1;
}

/// Obtain the current aggregate value.
bool aggstat_krt_get(
  const struct aggstat_krt *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = (AGGSTAT_FLT)(agg->ak_cnt)
       * agg->ak_fth
       / (agg->ak_snd * agg->ak_snd)
       - AGGSTAT_FLT_3_0;
  return true;
}

void aggstat_krt_cut(
  struct aggstat_krt *const agg,
  const AGGSTAT_INT len)
{
  agg->ak_fst = cut(agg->ak_fst, len);
  agg->ak_snd = cut(agg->ak_snd, len);
  agg->ak_trd = cut(agg->ak_trd, len);
  agg->ak_fth = cut(agg->ak_fth, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  MED (Median)                                                                             ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_med_new(
        struct aggstat_med *const agg, // Aggregate function.
  const AGGSTAT_FLT               stp) // Approximation step.
{
  agg->am_val = AGGSTAT_FLT_NAN;
  agg->am_stp = stp;
}

/// Update the aggregate function. 
void aggstat_med_put(
        struct aggstat_med *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  AGGSTAT_FLT dif; // Difference between input value and the median estimation.

  // Initialise the first value.
  if (AGGSTAT_FLT_TNAN(agg->am_val)) {
    agg->am_val = inp;
    return;
  }

  // Update the median estimate.
  if (agg->am_val > inp) {
    agg->am_val -= agg->am_stp;
  } else {
    agg->am_val += agg->am_stp;
  }
  
  // Update the step.
  dif = AGGSTAT_FLT_ABS(agg->am_val - inp);
  if (dif < agg->am_stp) {
    agg->am_stp /= AGGSTAT_FLT_2_0;
  }
}

/// Obtain the current aggregate value.
bool aggstat_med_get(
  const struct aggstat_med *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->am_val;
  return !AGGSTAT_FLT_TNAN(agg->am_val);
}

void aggstat_med_cut(
  struct aggstat_med *const agg,
  const AGGSTAT_INT len)
{
  agg->am_val = cut(agg->am_val, len);
  agg->am_stp = cut(agg->am_stp, len);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                           ////
////  QNT (p-Quantile)                                                                         ////
////                                                                                           ////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/// Initialise the aggregate function.
void aggstat_qnt_new(
        struct aggstat_qnt *const agg, // Aggregate function.
  const AGGSTAT_FLT               par) // Quantile parameter.
{
  agg->aq_cnt[0] = 0;
  agg->aq_cnt[1] = 0;
  agg->aq_cnt[2] = 0;
  agg->aq_cnt[3] = 0;
  agg->aq_par    = par;
  agg->aq_val[0] = AGGSTAT_FLT_0_0;
  agg->aq_val[1] = AGGSTAT_FLT_0_0;
  agg->aq_val[2] = AGGSTAT_FLT_0_0;
  agg->aq_val[3] = AGGSTAT_FLT_0_0;
  agg->aq_val[4] = AGGSTAT_FLT_0_0;
}

/// Linear extrapolation between two points.
static AGGSTAT_FLT aggstat_qnt_lin(
        struct aggstat_qnt *const restrict agg, // Aggregate function.
  const AGGSTAT_INT                        idx, // Marker index.
  const int8_t                             dir, // Direction of extrapolation.
  const AGGSTAT_INT        *const restrict cnt) // Simulated counts.
{
  return agg->aq_val[idx]       + (AGGSTAT_FLT)dir
      * (agg->aq_val[idx + dir] - agg->aq_val[idx])
      / (        cnt[idx + dir] -         cnt[idx]);
}

/// Parabolic extrapolation between two points.
static AGGSTAT_FLT aggstat_qnt_prb(
        struct aggstat_qnt *const restrict agg, // Aggregate function.
  const AGGSTAT_INT                        idx, // Marker index.
  const int8_t                             dir, // Direction of extrapolation.
  const AGGSTAT_INT        *const restrict cnt) // Simulated counts.
{
  AGGSTAT_FLT x;
  AGGSTAT_FLT y;

  x = (AGGSTAT_FLT)(        cnt[idx]     -         cnt[idx  - 1] + dir)
    *              (agg->aq_val[idx + 1] - agg->aq_val[idx])
    / (AGGSTAT_FLT)(        cnt[idx + 1] -         cnt[idx]);

  y = (AGGSTAT_FLT)(        cnt[idx + 1] -         cnt[idx] - dir)
    *              (agg->aq_val[idx]     - agg->aq_val[idx  - 1])
    / (AGGSTAT_FLT)(        cnt[idx]     -         cnt[idx  - 1]);

  return agg->aq_val[idx] + dir
       * (x + y)
       / (AGGSTAT_FLT)(cnt[idx + 1] - cnt[idx - 1]);
}

/// Readjust values after a new value was applied.
static void aggstat_qnt_adj(
        struct aggstat_qnt *const agg, // Aggregate function.
  const AGGSTAT_FLT               tar, // Target count.
  const AGGSTAT_INT               idx) // Adjusted value index.
{
  AGGSTAT_INT cnt[5]; // Simulated counts.
  AGGSTAT_FLT dlt;    // Difference between actual and target counts.
  AGGSTAT_FLT prb;    // Parabolic extrapolation value.
  int8_t      dir;    // Direction of adjustment.
  bool        ord[2]; // Descending ordering of actual counts.

  // Readjust the counts array.
  cnt[0] = 1;
  cnt[1] = agg->aq_cnt[0];
  cnt[2] = agg->aq_cnt[1];
  cnt[3] = agg->aq_cnt[2];
  cnt[4] = agg->aq_cnt[3];

  // Compute the current differences.
  dlt    = tar          - (AGGSTAT_FLT)cnt[idx];
  ord[0] = cnt[idx + 1] > (cnt[idx] + 1);
  ord[1] = cnt[idx - 1] < (cnt[idx] - 1);

  // Only continue with the readjustment if the values are out of order.
  if ((dlt >= AGGSTAT_FLT_1_0 && ord[0]) || (dlt <= -AGGSTAT_FLT_1_0 && ord[1])) {
    // Decide the movement direction.
    dir = (int8_t)AGGSTAT_FLT_SIGN(AGGSTAT_FLT_1_0, dlt);

    // Determine which estimation to use.
    prb = aggstat_qnt_prb(agg, idx, dir, cnt);

    // In case the piecewise parabolic estimation would result in out of order values, revert to
    // the linear estimation.
    if (agg->aq_val[idx - 1] < prb && prb < agg->aq_val[idx + 1]) {
      agg->aq_val[idx] = prb;
    } else {
      agg->aq_val[idx] = aggstat_qnt_lin(agg, idx, dir, cnt);
    }

    agg->aq_cnt[idx - 1] += dir;
  }
}

/// Sort the estimate values.
static void aggstat_qnt_ord(
        struct aggstat_qnt* agg) // Aggregate function.
{
  AGGSTAT_INT idx; // Outer loop index.
  AGGSTAT_INT jdx; // Inner loop index.
  AGGSTAT_INT min; // Local minimum.
  AGGSTAT_FLT swp; // Intermediate swap storage.

  // Traverse all but the last element with 
  idx = 0;
  while (idx < 4) {
    // Assume that the first element is the smallest.
    min = idx;

    // Find the smallest element in the rest of the array. 
    jdx = idx + 1;
    while (jdx < 5) {
      if (agg->aq_val[jdx] < agg->aq_val[min]) {
        min = jdx;
      }
      jdx += 1;
    }

    // Swap the minimal and the current value.
    swp = agg->aq_val[idx];
    agg->aq_val[idx] = agg->aq_val[min];
    agg->aq_val[min] = swp;
    idx += 1;
  }
}

/// Update the aggregate function.
void aggstat_qnt_put(
        struct aggstat_qnt *const agg, // Aggregate function.
  const AGGSTAT_FLT               inp) // Input value.
{
  AGGSTAT_INT inc[3]; // Increment decisions.
  AGGSTAT_FLT tar[3]; // Target counts.

  if (agg->aq_cnt[3] < 4) {
    agg->aq_val[agg->aq_cnt[3]] = inp;
    agg->aq_cnt[3] += 1;
    return;
  }

  // Switch to the advanced algorithm.
  if (agg->aq_cnt[3] == 4) {
    agg->aq_val[4] = inp;

    // Sort the values.
    aggstat_qnt_ord(agg);

    // Initialise the counts.
    agg->aq_cnt[0] = 2;
    agg->aq_cnt[1] = 3;
    agg->aq_cnt[2] = 4;
    agg->aq_cnt[3] = 5;
    return;
  }

  // Determine which counts need to be incremented.
  inc[0] = !!(inp < agg->aq_val[1]);
  inc[1] = !!(inp < agg->aq_val[2]);
  inc[2] = !!(inp < agg->aq_val[3]);

  // Increment the counts.
  agg->aq_cnt[0] += inc[0];
  agg->aq_cnt[1] += inc[0] || inc[1];
  agg->aq_cnt[2] += inc[0] || inc[1] || inc[2];
  agg->aq_cnt[3] += 1;

  // Adjust minimum and maximum.
  agg->aq_val[0] = AGGSTAT_FLT_FMIN(agg->aq_val[0], inp);
  agg->aq_val[4] = AGGSTAT_FLT_FMAX(agg->aq_val[4], inp);


  // Compute the target counts.
  tar[0] = AGGSTAT_FLT_2_0 * agg->aq_par                   + (AGGSTAT_FLT)agg->aq_cnt[3] * agg->aq_par / AGGSTAT_FLT_2_0;
  tar[1] = AGGSTAT_FLT_4_0 * agg->aq_par                   + (AGGSTAT_FLT)agg->aq_cnt[3] * agg->aq_par;
  tar[2] = AGGSTAT_FLT_2_0 + AGGSTAT_FLT_2_0 * agg->aq_par + (AGGSTAT_FLT)agg->aq_cnt[3] * (AGGSTAT_FLT_1_0 + agg->aq_par) / AGGSTAT_FLT_2_0;

  // Adjust the middle values.
  aggstat_qnt_adj(agg, tar[0], 1);
  aggstat_qnt_adj(agg, tar[1], 2);
  aggstat_qnt_adj(agg, tar[2], 3);
}

/// Obtain the current aggregate value.
bool aggstat_qnt_get(
  const struct aggstat_qnt *const restrict agg, // Aggregate function.
        AGGSTAT_FLT        *const restrict out) // Output value.
{
  *out = agg->aq_val[2];
  return (agg->aq_cnt[3] >= 5);
}

/// Trim the floating-point 
void aggstat_qnt_cut(
  struct aggstat_qnt *const agg,
  const AGGSTAT_INT len)
{
  agg->aq_val[0] = cut(agg->aq_val[0], len);
  agg->aq_val[1] = cut(agg->aq_val[1], len);
  agg->aq_val[2] = cut(agg->aq_val[2], len);
  agg->aq_val[3] = cut(agg->aq_val[3], len);
  agg->aq_par    = cut(agg->aq_par,    len);
}
