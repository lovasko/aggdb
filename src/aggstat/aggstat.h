// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef AGGSTAT_H
#define AGGSTAT_H

#include <stdbool.h>
#include <stdint.h>
#include <float.h>
#include <tgmath.h>


// This constant ensures that all code that is made available by this library is strictly compliant
// with the C99 language standard.
#ifndef AGGSTAT_STD
  #define AGGSTAT_STD 1
#endif

// This constant selects the width of the floating point type used by the library in all
// computations. The default value is 64, which denotes the `double` type.  Other permissible values
// include 32 for `float` and 128 for `__float128`. The latter type is a non-standard extension and
// will result in compile-time error unless the `AGGSTAT_STD` macro evaluates to `0`.
#ifndef AGGSTAT_FLT_BIT
  #define AGGSTAT_FLT_BIT 32
#endif

// This constant selects the width of the unsigned integer type used by the library in all
// computations. The default value is 64, which denotes the `uint64_t` type. Other permissible
// values are 32 for `uint32_t` and 16 for `uint16_t`. Furthermore, value 128 results in the
// `__uint128_t` type which is a non-standard extension that will result in compile-time error
// unless the `AGGSTAT_STD` macro evaluates to `0`.
#ifndef AGGSTAT_INT_BIT
  #define AGGSTAT_INT_BIT 32
#endif

// Determine the appropriate type-related constants and functions.
#if AGGSTAT_FLT_BIT == 32
  // Types.
  #define AGGSTAT_FLT float
  #define AGGSTAT_FLT_INT uint32_t

  // Functions.
  #define AGGSTAT_FLT_SQRT sqrtf
  #define AGGSTAT_FLT_POW  powf
  #define AGGSTAT_FLT_ABS  fabsf
  #define AGGSTAT_FLT_FMIN fminf
  #define AGGSTAT_FLT_FMAX fmaxf
  #define AGGSTAT_FLT_SIGN copysignf
  #define AGGSTAT_FLT_MODF modff
  #define AGGSTAT_FLT_EXP  expf
  #define AGGSTAT_FLT_COS  cosf
  #define AGGSTAT_FLT_TNAN isnanf
  #define AGGSTAT_FLT_NAN  nanf("")

  // Constants.
  #define AGGSTAT_FLT_FMT  "%e"
  #define AGGSTAT_FLT_STR  strfromf
  #define AGGSTAT_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## f
  #define AGGSTAT_FLT_MIN  -FLT_MAX
  #define AGGSTAT_FLT_MAX  FLT_MAX
#elif AGGSTAT_FLT_BIT == 64
  // Types.
  #define AGGSTAT_FLT double
  #define AGGSTAT_FLT_INT uint64_t

  // Functions.
  #define AGGSTAT_FLT_SQRT sqrt
  #define AGGSTAT_FLT_POW  pow
  #define AGGSTAT_FLT_ABS  fabs
  #define AGGSTAT_FLT_FMIN fmin
  #define AGGSTAT_FLT_FMAX fmax
  #define AGGSTAT_FLT_SIGN copysign
  #define AGGSTAT_FLT_MODF modf
  #define AGGSTAT_FLT_EXP  exp
  #define AGGSTAT_FLT_COS  cos
  #define AGGSTAT_FLT_TNAN isnan
  #define AGGSTAT_FLT_NAN  nan("") 

  // Constants.
  #define AGGSTAT_FLT_FMT  "%le"
  #define AGGSTAT_FLT_STR  strfromd
  #define AGGSTAT_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E
  #define AGGSTAT_FLT_MIN  -DBL_MAX
  #define AGGSTAT_FLT_MAX  DBL_MAX
#elif AGGSTAT_FLT_BIT == 80
  // Types.
  #define AGGSTAT_FLT long double
  #define AGGSTAT_FLT_INT __uint128_t

  // Functions.
  #define AGGSTAT_FLT_SQRT sqrtl
  #define AGGSTAT_FLT_POW  powl
  #define AGGSTAT_FLT_ABS  fabsl
  #define AGGSTAT_FLT_FMIN fminl
  #define AGGSTAT_FLT_FMAX fmaxl
  #define AGGSTAT_FLT_SIGN copysignl
  #define AGGSTAT_FLT_MODF modfl
  #define AGGSTAT_FLT_EXP  expl
  #define AGGSTAT_FLT_COS  cosl
  #define AGGSTAT_FLT_TNAN isnanl
  #define AGGSTAT_FLT_NAN  nanl("")

  // Constants.
  #define AGGSTAT_FLT_FMT  "%Le"
  #define AGGSTAT_FLT_STR  strfroml
  #define AGGSTAT_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## L
  #define AGGSTAT_FLT_MIN  -LDBL_MAX
  #define AGGSTAT_FLT_MAX  LDBL_MAX
#elif AGGSTAT_FLT_BIT == 128
  // Ensure that the type cannot be seleted when a strict standard-compliance is requested.
  #if AGGSTAT_STD == 1
    #error "AGGSTAT_FLT_BIT == 128 is a non-standard extension"
  #endif

  // Required header file.
  #include <quadmath.h>

  // Types.
  #define AGGSTAT_FLT __float128
  #define AGGSTAT_FLT_INT __uint128_t

  // Functions.
  #define AGGSTAT_FLT_SQRT sqrtq
  #define AGGSTAT_FLT_POW  powq
  #define AGGSTAT_FLT_ABS  fabsq
  #define AGGSTAT_FLT_FMIN fminq
  #define AGGSTAT_FLT_FMAX fmaxq
  #define AGGSTAT_FLT_SIGN copysignq
  #define AGGSTAT_FLT_MODF modfq
  #define AGGSTAT_FLT_COS  cosq
  #define AGGSTAT_FLT_TNAN isnanq
  #define AGGSTAT_FLT_NAN  nanq("")

  // Constants.
  #define AGGSTAT_FLT_FMT  "%Qe"
  #define AGGSTAT_FLT_STR  strofromf128
  #define AGGSTAT_FLT_NUM(I, F, S, E) I ## . ## F ## e ## S ## E ## Q
  #define AGGSTAT_FLT_MIN  -FLT128_MAX
  #define AGGSTAT_FLT_MAX  FLT128_MAX
#else
  #error "invalid value of AGGSTAT_FLT_BIT: " AGGSTAT_FLT_BIT
#endif

#if AGGSTAT_INT_BIT == 16
  // Types.
  #define AGGSTAT_INT     uint16_t

  // Constants.
  #define AGGSTAT_INT_MAX  UINT16_MAX
  #define AGGSTAT_INT_BYTE 2
#elif AGGSTAT_INT_BIT == 32
  // Types.
  #define AGGSTAT_INT     uint32_t

  // Constants.
  #define AGGSTAT_INT_MAX  UINT32_MAX
  #define AGGSTAT_INT_BYTE 4
#elif AGGSTAT_INT_BIT == 64
  // Types.
  #define AGGSTAT_INT     uint64_t

  // Constants.
  #define AGGSTAT_INT_MAX  UINT64_MAX
  #define AGGSTAT_INT_BYTE 8
#elif AGGSTAT_INT_BIT == 128
  // Ensure that the type cannot be selected when a strict standard-compliance is requested.
  #if AGGSTAT_STD == 1
    #error "AGGSTAT_INT_BIT == 128 is a non-standard extension"
  #endif

  // Types.
  #define AGGSTAT_INT     __uint128_t

  // Constants.
  #define AGGSTAT_INT_MAX  (AGGSTAT_INT)-1
  #define AGGSTAT_INT_BYTE 16
#else
  #error "invalid value of AGGSTAT_INT_BIT: " AGGSTAT_INT_BIT
#endif

// Numerical constants.
#define AGGSTAT_FLT_0_0  AGGSTAT_FLT_NUM(0,  0, +, 0)
#define AGGSTAT_FLT_0_1  AGGSTAT_FLT_NUM(0,  1, +, 0)
#define AGGSTAT_FLT_0_5  AGGSTAT_FLT_NUM(0,  5, +, 0)
#define AGGSTAT_FLT_0_75 AGGSTAT_FLT_NUM(0, 75, +, 0)
#define AGGSTAT_FLT_0_9  AGGSTAT_FLT_NUM(0,  9, +, 0)
#define AGGSTAT_FLT_0_95 AGGSTAT_FLT_NUM(0, 95, +, 0)
#define AGGSTAT_FLT_0_99 AGGSTAT_FLT_NUM(0, 99, +, 0)
#define AGGSTAT_FLT_1_0  AGGSTAT_FLT_NUM(1,  0, +, 0)
#define AGGSTAT_FLT_1_5  AGGSTAT_FLT_NUM(1,  5, +, 0)
#define AGGSTAT_FLT_2_0  AGGSTAT_FLT_NUM(2,  0, +, 0)
#define AGGSTAT_FLT_3_0  AGGSTAT_FLT_NUM(3,  0, +, 0)
#define AGGSTAT_FLT_4_0  AGGSTAT_FLT_NUM(4,  0, +, 0)
#define AGGSTAT_FLT_5_0  AGGSTAT_FLT_NUM(5,  0, +, 0)
#define AGGSTAT_FLT_6_0  AGGSTAT_FLT_NUM(6,  0, +, 0)

// Aggregate function `first`.
struct aggstat_fst {
  AGGSTAT_FLT af_val; // Value.
};
void aggstat_fst_new(struct aggstat_fst* agg);
void aggstat_fst_put(struct aggstat_fst* agg, AGGSTAT_FLT inp);
bool aggstat_fst_get(const struct aggstat_fst *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_fst_cut(struct aggstat_fst *const agg, const AGGSTAT_INT len);

// Aggregate function `last`.
struct aggstat_lst {
  AGGSTAT_FLT al_val; // Value.
};
void aggstat_lst_new(struct aggstat_lst* agg);
void aggstat_lst_put(struct aggstat_lst* agg, const AGGSTAT_FLT inp);
bool aggstat_lst_get(const struct aggstat_lst *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_lst_cut(struct aggstat_lst *const agg, const AGGSTAT_INT len);

// Aggregate function `count`.
struct aggstat_cnt {
  AGGSTAT_INT ac_val; // Value.
};
void aggstat_cnt_new(struct aggstat_cnt* agg);
void aggstat_cnt_put(struct aggstat_cnt* agg, const AGGSTAT_FLT inp);
bool aggstat_cnt_get(const struct aggstat_cnt *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_cnt_cut(struct aggstat_cnt *const agg, const AGGSTAT_INT len);

// Aggregate function `sum`.
struct aggstat_sum {
  AGGSTAT_FLT as_val; // Value.
};
void aggstat_sum_new(struct aggstat_sum* agg);
void aggstat_sum_put(struct aggstat_sum* agg, const AGGSTAT_FLT inp);
bool aggstat_sum_get(const struct aggstat_sum *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_sum_cut(struct aggstat_sum *const agg, const AGGSTAT_INT len);

// Aggregate function `minimum`.
struct aggstat_min {
  AGGSTAT_FLT an_val; // Value.
};
void aggstat_min_new(struct aggstat_min* agg);
void aggstat_min_put(struct aggstat_min* agg, AGGSTAT_FLT inp);
bool aggstat_min_get(const struct aggstat_min *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_min_cut(struct aggstat_min *const agg, const AGGSTAT_INT len);

// Aggregate function `maximum`.
struct aggstat_max {
  AGGSTAT_FLT ax_val; // Value.
};
void aggstat_max_new(struct aggstat_max* agg);
void aggstat_max_put(struct aggstat_max* agg, const AGGSTAT_FLT inp);
bool aggstat_max_get(const struct aggstat_max *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_max_cut(struct aggstat_max *const agg, const AGGSTAT_INT len);

// Aggregate function `average`.
struct aggstat_avg {
  AGGSTAT_INT aa_cnt; // Number of values.
  AGGSTAT_FLT aa_fst; // First statistical moment.
};
void aggstat_avg_new(struct aggstat_avg* agg);
void aggstat_avg_put(struct aggstat_avg* agg, AGGSTAT_FLT val);
bool aggstat_avg_get(const struct aggstat_avg *restrict agg, AGGSTAT_FLT *restrict val);
void aggstat_avg_cut(struct aggstat_avg *const agg, const AGGSTAT_INT len);

// Aggregate function `variance`.
struct aggstat_var {
  AGGSTAT_INT av_cnt; // Number of values.
  AGGSTAT_FLT av_fst; // First statistical moment.
  AGGSTAT_FLT av_snd; // Second statistical moment.
};
void aggstat_var_new(struct aggstat_var* agg);
void aggstat_var_put(struct aggstat_var* agg, AGGSTAT_FLT val);
bool aggstat_var_get(const struct aggstat_var *restrict agg, AGGSTAT_FLT *restrict val);
void aggstat_var_cut(struct aggstat_var *const agg, const AGGSTAT_INT len);

// Aggregate function `skewness`.
struct aggstat_skw {
  AGGSTAT_INT aw_cnt; // Number of values.
  AGGSTAT_FLT aw_fst; // First standardised moment.
  AGGSTAT_FLT aw_snd; // Second standardised moment.
  AGGSTAT_FLT aw_trd; // Third standardised moment.
};
void aggstat_skw_new(struct aggstat_skw* agg);
void aggstat_skw_put(struct aggstat_skw* agg, AGGSTAT_FLT inp);
bool aggstat_skw_get(const struct aggstat_skw *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_skw_cut(struct aggstat_skw *const agg, const AGGSTAT_INT len);

// Aggregate function `kurtosis`.
struct aggstat_krt {
  AGGSTAT_INT ak_cnt; // Number of values.
  AGGSTAT_FLT ak_fst; // First standardised moment.
  AGGSTAT_FLT ak_snd; // Second standardised moment.
  AGGSTAT_FLT ak_trd; // Third standardised moment.
  AGGSTAT_FLT ak_fth; // Fourth standardised moment.
};
void aggstat_krt_new(struct aggstat_krt* agg);
void aggstat_krt_put(struct aggstat_krt* agg, const AGGSTAT_FLT inp);
bool aggstat_krt_get(const struct aggstat_krt *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_krt_cut(struct aggstat_krt *const agg, const AGGSTAT_INT len);

// Aggregate function `median`.
struct aggstat_med {
  AGGSTAT_FLT am_val; // Median value.
  AGGSTAT_FLT am_stp; // Step change.
};
void aggstat_med_new(struct aggstat_med* agg, const AGGSTAT_FLT stp);
void aggstat_med_put(struct aggstat_med* agg, const AGGSTAT_FLT inp);
bool aggstat_med_get(const struct aggstat_med *restrict agg, AGGSTAT_FLT *restrict uot);
void aggstat_med_cut(struct aggstat_med *const agg, const AGGSTAT_INT len);

// Aggregate function `quantile`.
struct aggstat_qnt {
  AGGSTAT_INT aq_cnt[4]; // Number of values for each adjacent quantile.
  AGGSTAT_FLT aq_par;    // Quantile parameter.
  AGGSTAT_FLT aq_val[5]; // Values for each adjacent quantile.
};
void aggstat_qnt_new(struct aggstat_qnt* agg, const AGGSTAT_FLT pct);
void aggstat_qnt_put(struct aggstat_qnt* agg, const AGGSTAT_FLT inp);
bool aggstat_qnt_get(const struct aggstat_qnt *restrict agg, AGGSTAT_FLT *restrict out);
void aggstat_qnt_cut(struct aggstat_qnt *const agg, const AGGSTAT_INT len);

#endif
