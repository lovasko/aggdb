// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef QUEMPSC_H
#define QUEMPSC_H

#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>


#ifndef QUEMPSC_INT_BIT
  #error "must define QUEMPSC_INT_BIT to select integer width"
#endif

// Select the appropriate word type and its accompanying constants and functions.
//  * QUEMPSC_INT = integer type
#if   QUEMPSC_INT_BIT == 16
  #define QUEMPSC_INT uint16_t
  
#elif QUEMPSC_INT_BIT == 32
  #define QUEMPSC_INT uint32_t

#elif QUEMPSC_INT_BIT == 64
  #define QUEMPSC_INT uint64_t

#else
  #define _str(x) #x
  #define str(x) _str(x)
  #error "QUEMPSC_INT_BIT has unsupported value: " str(QUEMPSC_INT_BIT) " [16, 32, 64]"
#endif

// Ensure that the selected type is implemented as lock-free in the current standard C library.
#if QUEMPSC_INT_BIT == __SHORT_WIDTH__
  #if ATOMIC_SHORT_LOCK_FREE != 2
    #error "the selected type is not lock-free"
  #endif
#endif

#if QUEMPSC_INT_BIT == __INT_WIDTH__
  #if ATOMIC_INT_LOCK_FREE != 2
    #error "the selected type is not lock-free"
  #endif
#endif

#if QUEMPSC_INT_BIT == __LONG_WIDTH__
  #if ATOMIC_LONG_LOCK_FREE != 2
    #error "the selected type is not lock-free"
  #endif
#endif

/// Multi-producer single-consumer integer queue.
struct quempsc {
  _Atomic QUEMPSC_INT* qm_mem; // Integer store.
  _Atomic QUEMPSC_INT  qm_put; // Writing head.
  _Atomic QUEMPSC_INT  qm_get; // Reading head.
  _Atomic QUEMPSC_INT  qm_cnt; // Number of integers in the queue.
          QUEMPSC_INT  qm_len; // Maximal number of integers in the queue.
};

/// Create a new queue.
bool quempsc_new(
        struct quempsc* que,  // Queue.
  const QUEMPSC_INT     len); // Maximal length of the queue.

/// Release resources held by the queue.
void quempsc_del(
  struct quempsc* que);  // Queue.

/// Push an integer to the queue.
bool quempsc_put(
        struct quempsc* que,  // Queue.
  const QUEMPSC_INT     inp); // Input value.

/// Pop an integer from the queue.
bool quempsc_get(
  struct quempsc *const restrict que,  // Queue.
  QUEMPSC_INT    *const restrict out); // Output value.

#endif
