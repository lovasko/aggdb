// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdlib.h>
#include <stddef.h>

#include "quempsc.h"


/// Create a new empty queue.
bool quempsc_new(
        struct quempsc* que, // Queue.
  const QUEMPSC_INT     len) // Maximal number of values.
{
  que->qm_len = len;
  que->qm_cnt = 0; 
  que->qm_get = 0;    
  que->qm_put = 0;

  // Allocate memory for the queue values that has all bits unset.
  que->qm_mem = calloc(len, sizeof(_Atomic QUEMPSC_INT));
  if (que->qm_mem == NULL) {
    return false;
  }

  return true;
}

/// Release all resources held by the queue and reset all counters.
void quempsc_del(
  struct quempsc* que) // Queue.
{
  free(que->qm_mem);
  que->qm_len = 0;
  que->qm_cnt = 0;
  que->qm_put = 0;
  que->qm_get = 0;
}

/// Push an integer to the queue.
bool quempsc_put(
        struct quempsc* que, // Queue.
  const QUEMPSC_INT     inp) // Input value.
{
  QUEMPSC_INT cnt; // Number of values in the queue.
  QUEMPSC_INT put; // Unique writing position.

  // Attempt to secure a spot.
  cnt = atomic_fetch_add(&que->qm_cnt, 1);
  if (cnt >= que->qm_len) {
    atomic_fetch_sub(&que->qm_cnt, 1);
    return false;
  }

  // Increment the writing head to allocate a unique cell.
  put = atomic_fetch_add(&que->qm_put, 1);
  que->qm_mem[put % que->qm_len] = inp;

  return true;
}

/// Pop an integer from the queue.
bool quempsc_get(
  struct quempsc *const restrict que, // Queue.
  QUEMPSC_INT    *const restrict out) // Output value.
{
  QUEMPSC_INT val; // Retrieved value.

  val = atomic_exchange(&que->qm_mem[que->qm_get % que->qm_len], 0);
  if (val == 0) {
    return false;
  }

  // Increment the reading head.
  atomic_fetch_add(&que->qm_get, 1);

  // Decrement the number of items in the queue.
  atomic_fetch_sub(&que->qm_cnt, 1);

  *out = val;
  return true;
}
