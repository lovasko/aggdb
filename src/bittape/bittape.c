// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.

// TODO get0 and get1 should fail, if there are no bits to read, and should not return 0 as that is
// not really the number of bits that they read.

// IDEA WORD/INT distinction
// it could be beneficial if it were possible to distinguish these two so that the fast operations
// are done on the native type, but it would be possible to address more bits this way

// TODO code dedup
// get0 and get1 appear to be doing the same stuff, but with applying reverse bits function on the
// array. maybe there should be some common code there? like counting the first one and counting the
// last one? and then the functions only provide the 

#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <inttypes.h>

#include "bittape/bittape.h"
#include "bitutil/bitutil.h"


/// Initialise the bit tape.
bool bittape_new(
        struct bittape *const bit, // Bit tape.
  const BITTAPE_INT           max) // Maximal number of stored bits.
{
  // Initialise the counters.
  bit->bt_max = max;
  bit->bt_len = 0;

  // Allocate the required memory so that `max` bits can be stored. The memory is rounded up to the
  // next BITTAPE_INT.
  bit->bt_buf = calloc(sizeof(BITTAPE_INT), max / BITTAPE_INT_BIT + 1);
  if (bit->bt_buf == NULL) {
    return false;
  }

  return true;
}

/// Free resources held by the bit tape.
void bittape_del(
  struct bittape *const bit) // Bit tape.
{
  free(bit->bt_buf);
}

/// Trim unused resources held by the bit tape.
bool bittape_cut(
  struct bittape *const bit)
{
  BITTAPE_INT lst; // Last word index.

  // Compute the index of the last used word and trim the memory buffer accordingly.
  lst = bit->bt_len / BITTAPE_INT_BIT + 1;
  bit->bt_buf = realloc(bit->bt_buf, sizeof(BITTAPE_INT) * lst);
  if (bit->bt_buf == NULL) {
    return false;
  }

  // The current writing head becomes the end of the tape.
  bit->bt_max = bit->bt_len;
  return true;
}

/// Perform a write within a single word.
static void put(
        struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    cnt, // Number of bits to write.
  const BITTAPE_INT                    val) // Bits to write.
{
  BITTAPE_INT idx; // Word index.
  BITTAPE_INT off; // Offset within the word.
  BITTAPE_INT msk; // Input bits mask.

  // Compute the current writing word index.
  idx = pos->bh_put / BITTAPE_INT_BIT;

  // Compute the offset within the word.
  off = pos->bh_put % BITTAPE_INT_BIT;

  if (cnt == BITTAPE_INT_BIT) {
    msk = BITTAPE_INT_MAX;
  } else {
    msk = ((BITTAPE_INT)1 << cnt) - 1;
  }

  // Write the requested bits.
  bit->bt_buf[idx] |= ((val & msk) << off);

  // Advance the writing head.
  pos->bh_put += cnt;
}

/// Write bits to the tape at the writing head position.
bool bittape_put(
        struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    cnt, // Number of bits to write.
  const BITTAPE_INT                    val) // Bits to write.
{
  BITTAPE_INT spl; // Split index between the two words.
  BITTAPE_INT idx; // Bit index within the current word.
  BITTAPE_INT adj; // Adjusted bit index after the write.

  // Ensure that the tape has sufficient space for the requested bits.
  if (pos->bh_put + cnt > bit->bt_max) {
    return false;
  }

  // Ignore writes of zero bits.
  if (cnt == 0) {
    return true;
  }

  // Determine where the split ought to be.
  idx =  pos->bh_put        % BITTAPE_INT_BIT;
  adj = (pos->bh_put + cnt) % BITTAPE_INT_BIT;
  spl = adj * (adj <= idx);

  // Perform the first write which always occurs.
  put(bit, pos, cnt - spl, val);

  // Only perform the second write if any bits fall onto the second word.
  if (spl > 0) {
    put(bit, pos, spl, val >> (cnt - spl));
  }

  // Update the upper bound of used bits and advance the writing head.
  if (pos->bh_put > bit->bt_len) {
    bit->bt_len = pos->bh_put;
  }

  return true;
}

/// Perform a read within a single word.
static BITTAPE_INT get(
  const struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    cnt) // Number of bits to read.
{
  BITTAPE_INT idx; // Index of the reading word.
  BITTAPE_INT off; // Offset within the word.
  BITTAPE_INT val; // Read bits.

  // Compute the current reading word index.
  idx = pos->bh_get / BITTAPE_INT_BIT;

  // Compute the offset within the word.
  off = pos->bh_get % BITTAPE_INT_BIT;

  // Retrieve the value at the selected offset.
  val = bit->bt_buf[idx] >> off;

  // Remove all unwanted higher bits.
  if (cnt < BITTAPE_INT_BIT) {
    val &= ((BITTAPE_INT)1 << cnt) - 1;
  }

  // Advance the reading head.
  pos->bh_get += cnt;

  return val;
}

/// Read bits from the tape.
bool bittape_get(
  const struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    cnt, // Number of bits to read.
        BITTAPE_INT    *const restrict val) // Output bits.
{
  BITTAPE_INT spl; // Split index between the two words.
  BITTAPE_INT idx; // Bit index within the current word.
  BITTAPE_INT adj; // Adjusted bit index after the read.

  // Ensure that the tape has requested number of bits available.
  if (pos->bh_get + cnt > bit->bt_len) {
    return false;
  }

  // Determine where the split ought to be.
  idx =  pos->bh_get        % BITTAPE_INT_BIT;
  adj = (pos->bh_get + cnt) % BITTAPE_INT_BIT;
  spl = adj * (adj <= idx);

  // Perform two reads, one for each part.
  *val = get(bit, pos, cnt - spl);

  // Only perform the second read if the request spans two words.
  if (spl > 0) {
    *val |= (get(bit, pos, spl) << (cnt - spl));
  }

  return true;
}

/// Retrieve the available bits for writing.
void bittape_avl(
  const struct bittape *const restrict bit, // Bit tape.
        BITTAPE_INT    *const restrict out) // Available bits.
{
  *out = bit->bt_max - bit->bt_len;
}

/// Iterate through all the bits and execute a function for each.
/// TODO: make the bittape const, don't provide it as an argument to the functor
void bittape_map(
  struct bittape* bit,                                   // Bit tape.
  bool          (*fun)(BITTAPE_INT, BITTAPE_INT, void*), // Function.
  void*           ctx)                                   // Context.
{
  BITTAPE_INT bix; // Bit index.
  BITTAPE_INT idx; // Word index.
  BITTAPE_INT off; // Offset within the word.
  BITTAPE_INT val; // Value of the bit.
  bool        ret; // Return value.

  bix = 0;
  while (bix < bit->bt_len) {
    // Determine the position of the bit.
    idx = bix / BITTAPE_INT_BIT;
    off = bix % BITTAPE_INT_BIT;

    // Extract the bit.
    val = (bit->bt_buf[idx] >> off) & 1;

    // Execute the function.
    ret = fun(bix, val, ctx);
    if (ret == false) {
      break;
    }

    // Advance to the next bit.
    bix += 1;
  }
}

/// Read a sequence of unset bits and retrieve their count.
bool bittape_get0(
  const struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
        BITTAPE_INT    *const restrict out) // Number of bits.
{
  BITTAPE_INT cnt; // Number of zeros.
  BITTAPE_INT idx; // Index of the reading word.
  BITTAPE_INT off; // Offset within the word.
  BITTAPE_INT fst; // First word.
  BITTAPE_INT mid; // Middle words.
  BITTAPE_INT lst; // Last word.
  BITTAPE_INT max; // Index of the last written word.
  BITTAPE_INT x;

  // Ensure that the tape has requested number of bits available.
  if (pos->bh_get >= bit->bt_len) {
    return false;
  }

  // Start with no zeros.
  cnt = 0;

  // Compute the current reading word index.
  idx = pos->bh_get / BITTAPE_INT_BIT;

  // Compute the offset within the word.
  off = pos->bh_get % BITTAPE_INT_BIT;

  // Compute the trailing set of zeros in the current word by shifting it all the way to the left
  // and counting the leading zeros.
  x = bit->bt_buf[idx] >> off;
  if (x == 0) {
    fst = BITTAPE_INT_BIT - off;
  } else {
    fst = bitutil_ctz(x);

    // Finish the search if the number of read bits is less than the remaining bits in the word.
    if (fst < BITTAPE_INT_BIT - off) {
      if (pos->bh_get + fst > bit->bt_len) {
        fst = bit->bt_len;
      }

      pos->bh_get += fst;
      *out = fst;
      return true;
    }
  }

  // Need to take into account the edge case where there aren't enough bits, or where the 

  // Compute the last used word.
  max = bit->bt_max / BITTAPE_INT_BIT;

  // Compute the number of words that are made up only from zeros.
  idx += 1;
  mid = 0;
  while (idx < max - 1 && bit->bt_buf[idx] == 0) {
    mid += BITTAPE_INT_BIT;
    idx += 1;
  }

  // Compute the leading set of zeros in the last word.
  lst = bitutil_ctz(bit->bt_buf[idx]);

  // Add all parts together.
  cnt = fst + mid + lst;
  if (pos->bh_get + cnt > bit->bt_len) {
    cnt = bit->bt_len;
  }

  // Advance the reading head accordingly.
  pos->bh_get += cnt;

  // Return the number of zeros.
  *out = cnt;
  return true;
}

/// Read a sequence of set bits and retrieve their count.
bool bittape_get1(
  const struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
        BITTAPE_INT    *const restrict out) // Number of bits.
{
  BITTAPE_INT cnt; // Number of zeros.
  BITTAPE_INT idx; // Index of the reading word.
  BITTAPE_INT off; // Offset within the word.
  BITTAPE_INT fst; // First word.
  BITTAPE_INT mid; // Middle words.
  BITTAPE_INT lst; // Last word.
  BITTAPE_INT max; // Index of the last written word.
  BITTAPE_INT x;

  // Ensure that the tape has requested number of bits available.
  if (pos->bh_get >= bit->bt_len) {
    return false;
  }

  // Start with zero set bits.
  cnt = 0;

  // Compute the current reading word index.
  idx = pos->bh_get / BITTAPE_INT_BIT;

  // Compute the offset within the word.
  off = pos->bh_get % BITTAPE_INT_BIT;

  // Compute the trailing set of zeros in the current word by shifting it all the way to the left
  // and counting the leading zeros.
  x = ~(bit->bt_buf[idx]) >> off;
  if (x == 0) {
    fst = BITTAPE_INT_BIT - off;
  } else {
    fst = bitutil_ctz(x);

    // Finish the search if the number of read bits is less than the remaining bits in the word.
    if (fst < BITTAPE_INT_BIT - off) {
      if (pos->bh_get + fst > bit->bt_len) {
        fst = bit->bt_len;
      }

      pos->bh_get += fst;
      *out = fst;
      return true;
    }
  }

  // Need to take into account the edge case where there aren't enough bits, or where the 

  // Compute the last used word.
  max = bit->bt_max / BITTAPE_INT_BIT;

  // Compute the number of words that are made up only from set bits.
  idx += 1;
  mid = 0;
  while (idx < max - 1 && bit->bt_buf[idx] == BITTAPE_INT_MAX) {
    mid += BITTAPE_INT_BIT;
    idx += 1;
  }

  // Compute the leading set of zeros in the last word.
  lst = bitutil_ctz(~(bit->bt_buf[idx]));

  // Add all parts together.
  cnt = fst + mid + lst;
  if (pos->bh_get + cnt > bit->bt_len) {
    cnt = bit->bt_len;
  }

  // Advance the reading head accordingly.
  pos->bh_get += cnt;

  // Return the number of set bits.
  *out = cnt;
  return true;
}

/// Append a sequence of unset bits.
bool bittape_put0(
        struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    val) // Number of zeros.
{
  BITTAPE_INT cnt;
  BITTAPE_INT idx;
  bool        ret;

  // Early exit for when the requested number is zero.
  if (val == 0) {
    return true;
  }

  // Verify that space is available upfront to prevent partial writes.
  if (pos->bh_put + val > bit->bt_max) {
    return false;
  }

  // Determine the number of full blocks where all bits are zeros.
  cnt = val / BITTAPE_INT_BIT;
  idx = 0;
  while (idx < cnt) {
    ret = bittape_put(bit, pos, BITTAPE_INT_BIT, 0);
    if (ret == false) {
      return false;
    }

    // Advance to the next block.
    idx += 1;
  }

  // Append any remaining zero bits.
  cnt = val % BITTAPE_INT_BIT;
  ret = bittape_put(bit, pos, cnt, 0);
  if (ret == false) {
    return false;
  }
  
  return true;
}

/// Append a sequence of set bits.
bool bittape_put1(
        struct bittape *const restrict bit, // Bit tape.
        struct bithead *const restrict pos, // Reading and writing heads.
  const BITTAPE_INT                    val) // Number of zeros.
{
  BITTAPE_INT cnt;
  BITTAPE_INT idx;
  bool        ret;

  // Early exit for when the requested number is zero.
  if (val == 0) {
    return true;
  }

  // Verify that space is available upfront to prevent partial writes.
  if (pos->bh_put + val > bit->bt_max) {
    return false;
  }

  // Determine the number of full blocks where all bits are zeros.
  cnt = val / BITTAPE_INT_BIT;
  idx = 0;
  while (idx < cnt) {
    ret = bittape_put(bit, pos, BITTAPE_INT_BIT, BITTAPE_INT_MAX);
    if (ret == false) {
      return false;
    }

    // Advance to the next block.
    idx += 1;
  }

  // Append any remaining zero bits.
  cnt = val % BITTAPE_INT_BIT;
  ret = bittape_put(bit, pos, cnt, BITTAPE_INT_MAX);
  if (ret == false) {
    return false;
  }
  
  return true;
}

/// Compute the index of the n-th unset bit.
bool bittape_sel0(
  const struct bittape *const restrict bit, // Bit tape.
  const BITTAPE_INT                    pos, // Number of the set bit.
        BITTAPE_INT    *const restrict out) // Index of the set bit.
{
  BITTAPE_INT cnt; // Overall population count.
  BITTAPE_INT pop; // Population count.
  BITTAPE_INT idx; // Block index.
  BITTAPE_INT lst; // Last word selection.
  BITTAPE_INT msk; // Bit mask for the last word.
  BITTAPE_INT off; // Offset within the word.

  // Identify the index of the last used word.
  lst = bit->bt_len / BITTAPE_INT_BIT;

  // Compute the population counts of all the blocks up to the last one.
  cnt = 0;
  idx = 0;
  while (idx < (lst + 1)) {
    pop = bitutil_pop(~(bit->bt_buf[idx]));
    
    // Terminate early when the population count is above the expected selection.
    if (cnt + pop >= (pos + 1)) {
      break;
    } else {
      cnt += pop;
    }

    // Advance to the next block.
    idx += 1;
  }

  // Terminate the process if the population count is less than the expected index.
  if (cnt + pop < (pos + 1)) {
    return false;
  }

  // Count the remaining set bits in the last word.
  off = bit->bt_len % BITTAPE_INT_BIT;
  if (idx == lst && off != BITTAPE_INT_BIT - 1) {
    msk = BITTAPE_INT_MAX >> (BITTAPE_INT_BIT - off);
  } else {
    msk = BITTAPE_INT_MAX;
  }
  *out = (idx * BITTAPE_INT_BIT) + bitutil_sel((~(bit->bt_buf[idx])) & msk, pos - cnt);
  return true;
}

/// Compute the index of the n-th one bit.
bool bittape_sel1(
  const struct bittape *const restrict bit, // Bit tape.
  const BITTAPE_INT                    pos, // Number of the set bit.
        BITTAPE_INT    *const restrict out) // Index of the set bit.
{
  BITTAPE_INT cnt; // Overall population count.
  BITTAPE_INT pop; // Population count.
  BITTAPE_INT idx; // Block index.
  BITTAPE_INT lst; // Last word selection.

  // Identify the index of the last used word.
  lst = bit->bt_len / BITTAPE_INT_BIT;

  // Compute the population counts of all the blocks up to the last one.
  cnt = 0;
  idx = 0;
  while (idx < (lst + 1)) {
    pop = bitutil_pop(bit->bt_buf[idx]);
    
    // Terminate early when the population count is above the expected selection.
    if (cnt + pop >= (pos + 1)) {
      break;
    } else {
      cnt += pop;
    }

    // Advance to the next block.
    idx += 1;
  }

  // Terminate the process if the population count is less than the expected index.
  if (cnt + pop < (pos + 1)) {
    return false;
  }

  // Count the remaining set bits in the last word.
  *out = (idx * BITTAPE_INT_BIT) + bitutil_sel(bit->bt_buf[idx], pos - cnt);

  return true;
}

/// Unset the n-th bit on the tape.
bool bittape_set0(
        struct bittape *const bit, // Bit tape.
  const BITTAPE_INT           pos) // Position.
{
  // Ensure that the write is within the tape bounds.
  if (pos >= bit->bt_max) {
    return false;
  }

  // Unset the single bit.
  bit->bt_buf[pos / BITTAPE_INT_BIT] &= ~((BITTAPE_INT)1 << (pos % BITTAPE_INT_BIT));
  return true;
}

/// Set the n-th bit on the tape.
bool bittape_set1(
        struct bittape *const bit, // Bit tape.
  const BITTAPE_INT           pos) // Position.
{
  // Ensure that the write is within the tape bounds.
  if (pos >= bit->bt_max) {
    return false;
  }

  // Set the single bit.
  bit->bt_buf[pos / BITTAPE_INT_BIT] |= ((BITTAPE_INT)1 << (pos % BITTAPE_INT_BIT));
  return true;
}

/// Unset all bits on the tape.
void bittape_nul(
  struct bittape *const bit) // Bit tape.
{
  BITTAPE_INT idx; // Array index.

  // Unset all bits in every word.
  idx = 0;
  while (idx < (bit->bt_max / BITTAPE_INT_BIT + 1)) {
    bit->bt_buf[idx] = 0;
    idx             += 1;
  }
}

/// Obtain the population of the tape.
void bittape_pop(
  const struct bittape *const restrict bit, // Bit tape.
        BITTAPE_INT    *const restrict out) // Population.
{
  BITTAPE_INT idx; // Array index.
  BITTAPE_INT pop; // Population count.

  // Iterate through all words and sum the partial population counts.
  pop = 0;
  idx = 0;
  while (idx < (bit->bt_max / BITTAPE_INT_BIT + 1)) {
    pop += bitutil_pop(bit->bt_buf[idx]);
    idx += 1;
  }

  *out = pop;
}

/// Add set bits from a second tape.
bool bittape_add(
        struct bittape *const restrict bit, // Bit tape.
  const struct bittape *const restrict add) // Bit tape to add.
{
  BITTAPE_INT idx; // Array index.

  // Ensure that the number of bits in both tapes is equal.
  if (bit->bt_max != add->bt_max) {
    return false;
  }

  // Traverse all but the last word of the tape and add the bits.
  idx = 0;
  while (idx < (bit->bt_max / BITTAPE_INT_BIT + 1)) {
    bit->bt_buf[idx] |= add->bt_buf[idx];
    idx              += 1;
  }

  return true;
}

/// Count the number of similarities: set bits at the same positions.
bool bittape_sim(
  const struct bittape *const restrict bit1, // First bit tape.
  const struct bittape *const restrict bit2, // Second bit tape
        BITTAPE_INT    *const restrict out)  // Number of similarities.
{
  BITTAPE_INT idx; // Array index.
  BITTAPE_INT sim; // Number of similarities.
  BITTAPE_INT and; // Intersection of two words.

  // Ensure that the number of bits in both tapes is equal.
  if (bit1->bt_max != bit2->bt_max) {
    return false;
  }
  
  // Compute the number of similarities in each 
  sim = 0;
  idx = 0;
  while (idx < (bit1->bt_max / BITTAPE_INT_BIT + 1)) {
    and  = bit1->bt_buf[idx] & bit2->bt_buf[idx];
    sim += bitutil_pop(and);
    idx += 1;
  }

  // Report the final result.
  *out = sim;
  return true;
}

/// Reset the writing and reading head positions.
void bithead_nul(
  struct bithead *const pos) // Reading and writing heads.
{
  pos->bh_get = 0;
  pos->bh_put = 0;
}

/// Retrieve the writing and reading head positions.
void bithead_get(
  const struct bithead *const restrict pos, // Reading and writing heads.
        BITTAPE_INT    *const restrict get, // Reading position.
        BITTAPE_INT    *const restrict put) // Writing position.
{
  *get = pos->bh_get;
  *put = pos->bh_put;
}

/// Assign the writing and reading head positions.
void bithead_set(
        struct bithead *const pos, // Reading and writing heads.
  const BITTAPE_INT           get, // Reading position.
  const BITTAPE_INT           put) // Writing position.
{
  pos->bh_get = get;
  pos->bh_put = put;
}

/// Move the writing and reading heads forward.
void bithead_fwd(
        struct bithead *const pos, // Reading and writing heads.
  const BITTAPE_INT           get, // Reading delta.
  const BITTAPE_INT           put) // Writing delta.
{
  pos->bh_get += get;
  pos->bh_put += put;
}
