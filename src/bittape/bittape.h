// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef BITTAPE_H
#define BITTAPE_H

#include <stdbool.h>
#include <stdint.h>


#ifndef BITTAPE_INT_BIT
  #error "must define BITTAPE_INT_BIT to select integer width"
#endif

// Select the appropriate word type.
#if   BITTAPE_INT_BIT == 16
  #define BITTAPE_INT     uint16_t
  #define BITTAPE_INT_MAX UINT16_MAX

#elif BITTAPE_INT_BIT == 32
  #define BITTAPE_INT     uint32_t
  #define BITTAPE_INT_MAX UINT32_MAX

#elif BITTAPE_INT_BIT == 64
  #define BITTAPE_INT     uint64_t
  #define BITTAPE_INT_MAX UINT64_MAX

#else
  #define _str(x) #x
  #define str(x) _str(x)
  #error "BITTAPE_INT_BIT has unsupported value: " str(BITTAPE_INT_BIT) " [8, 16, 32, 64]"
#endif

/// Bit tape.
struct bittape {
  BITTAPE_INT  bt_max; // Maximal number of bits.
  BITTAPE_INT  bt_len; // Current used bits.
  BITTAPE_INT* bt_buf; // Bits storage.
};

/// Bit tape head.
struct bithead {
  BITTAPE_INT bh_get; // Reading position in bits.
  BITTAPE_INT bh_put; // Writing position in bits.
};

/// Initialise the bit tape and allocate necessary resources.
bool bittape_new(
        struct bittape *const bit,
  const BITTAPE_INT           cnt);

/// Free all resources held by the bit tape.
void bittape_del(
  struct bittape *const bit);

/// Trim unused resources held by the bit tape.
bool bittape_cut(
  struct bittape *const bit);

/// Append bits to the tape.
bool bittape_put(
        struct bittape *const restrict bit,
        struct bithead *const restrict pos,
  const BITTAPE_INT                    cnt,
  const BITTAPE_INT                    val);

/// Read bits from the tape.
bool bittape_get(
  const struct bittape *const restrict bit,
        struct bithead *const restrict pos,
  const BITTAPE_INT                    cnt,
        BITTAPE_INT    *const restrict val);

/// Retrieve the available bits for writing.
void bittape_avl(
  const struct bittape *const restrict bit,
        BITTAPE_INT    *const restrict out);

/// Execute a function for every bit on the tape bound by a common context.
void bittape_map(
  struct bittape* bit,
  bool          (*fun)(BITTAPE_INT, BITTAPE_INT, void*),
  void*           ctx);

/// Read a sequence of unset bits and retrieve their count.
bool bittape_get0(
  const struct bittape *const restrict bit,
        struct bithead *const restrict pos,
        BITTAPE_INT    *const restrict out);

/// Read a sequence of set bits and retrieve their count.
bool bittape_get1(
  const struct bittape *const restrict bit,
        struct bithead *const restrict pos,
        BITTAPE_INT    *const restrict out);

/// Append a sequence of unset bits.
bool bittape_put0(
        struct bittape *const restrict bit,
        struct bithead *const restrict pos,
  const BITTAPE_INT                    cnt);

/// Append a sequence of set bits.
bool bittape_put1(
        struct bittape *const restrict bit,
        struct bithead *const restrict pos,
  const BITTAPE_INT                    cnt);

/// Compute the index of the n-th unset bit.
bool bittape_sel0(
  const struct bittape *const restrict bit,
  const BITTAPE_INT                    pos,
        BITTAPE_INT    *const restrict out);

/// Compute the index of the n-th set bit.
bool bittape_sel1(
  const struct bittape *const restrict bit,
  const BITTAPE_INT                    pos,
        BITTAPE_INT    *const restrict out);

/// Unset the n-th bit on the tape.
bool bittape_set0(
        struct bittape *const bit,
  const BITTAPE_INT           pos);

/// Set the n-th bit on the tape.
bool bittape_set1(
        struct bittape *const bit,
  const BITTAPE_INT           pos);

/// Unset all bits on the tape.
void bittape_nul(
  struct bittape *const bit);

/// Obtain the population of the tape.
void bittape_pop(
  const struct bittape *const restrict bit,
        BITTAPE_INT    *const restrict out);

/// Add set bits from a second tape.
bool bittape_add(
        struct bittape *const restrict bit,
  const struct bittape *const restrict add);

/// Count the number of similarities: set bits at the same positions.
bool bittape_sim(
  const struct bittape *const restrict bit1,
  const struct bittape *const restrict bit2,
        BITTAPE_INT    *const restrict out);

/// Reset the writing and reading head positions.
void bithead_nul(
  struct bithead *const pos);

/// Retrieve the writing and reading head positions.
void bithead_get(
  const struct bithead *const restrict pos,
        BITTAPE_INT    *const restrict get,
        BITTAPE_INT    *const restrict put);

/// Assign the writing and reading head positions.
void bithead_set(
        struct bithead *const pos,
  const BITTAPE_INT           get,
  const BITTAPE_INT           put);

/// Move the writing and reading heads forward.
void bithead_fwd(
        struct bithead *const pos,
  const BITTAPE_INT           get,
  const BITTAPE_INT           put);

#endif
