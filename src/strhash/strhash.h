// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef STRHASH_H
#define STRHASH_H

#include <stdbool.h>
#include <stdint.h>


// IDEA: new function
//  add strhash_new that specifies the alphabet
//  the alphabet is just an array of characters, the numbering of them is implicit
//  this then generates the tables (or maybe they are implicit too?)
//  good to reduce the alphabet -> just numbers? just letters? just letters and an underscore?
//  worth exploring then how that changes the length of the greatest string

// Ensure that the bit width is selected.
#ifndef STRHASH_INT_BIT
  #error "must define STRHASH_INT_BIT to select the bit width"
#endif

// Select the appropriate integer type. The maximal string length appears to be roughly three times
// number of bytes, plus one.
#if   STRHASH_INT_BIT == 16
  #define STRHASH_INT      uint16_t
  #define STRHASH_LEN      4  // "aj5i"
  #define STRHASH_INT_BYTE 2
  #define STRHASH_INT_FMT  PRIu16
  #define STRHASH_POW_MAX  50653      

#elif STRHASH_INT_BIT == 32
  #define STRHASH_INT      uint32_t
  #define STRHASH_LEN      7  // "ax7x8wg"
  #define STRHASH_INT_BYTE 4
  #define STRHASH_INT_FMT  PRIu32
  #define STRHASH_POW_MAX  2565726409

#elif STRHASH_INT_BIT == 64
  #define STRHASH_INT      uint64_t
  #define STRHASH_LEN      13 // "b2yg2214il10l"
  #define STRHASH_INT_BYTE 8
  #define STRHASH_INT_FMT  PRIu64
  #define STRHASH_POW_MAX  6582952005840035281

#elif STRHASH_INT_BIT == 128
  #define STRHASH_INT      __uint128_t
  #define STRHASH_LEN      25 // "g4s3wsz1ir1v7kfq6xlw5ylv6"
  #define STRHASH_INT_BYTE 16
  #define STRHASH_POW_MAX  43335257111193343900365036083324748961 

#else
  #error "invalid value for STRHASH_INT_BIT: " STRHASH_INT_BIT
#endif

bool strhash_put(STRHASH_INT* val, const char* str, const STRHASH_INT len);
bool strhash_get(char* str, const STRHASH_INT len, const STRHASH_INT val);

#endif
