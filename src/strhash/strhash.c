// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdio.h>

#include "strhash.h"


/// Compress a string into an integer representation using arithmetic coding. The permitted
/// characters are only 'a-z0-9_'.
bool strhash_put(
        STRHASH_INT* val, // Integer representation.
  const char*        str, // String to compress.
  const STRHASH_INT  len) // Length of the string.
{
  // The following table is a translation between characters and alphabet indices. It is offset by
  // one on purpose, so that an invalid character would map to the value zero.
  const uint8_t tab[256] = {
    ['a'] = 1,  ['b'] = 2,  ['c'] = 3,  ['d'] = 4,  ['e'] = 5,
    ['f'] = 6,  ['g'] = 7,  ['h'] = 8,  ['i'] = 9,  ['j'] = 10,
    ['k'] = 11, ['l'] = 12, ['m'] = 13, ['n'] = 14, ['o'] = 15,
    ['p'] = 16, ['q'] = 17, ['r'] = 18, ['s'] = 19, ['t'] = 20,
    ['u'] = 21, ['v'] = 22, ['w'] = 23, ['x'] = 24, ['y'] = 25,
    ['z'] = 26, ['0'] = 27, ['1'] = 28, ['2'] = 29, ['3'] = 30,
    ['4'] = 31, ['5'] = 32, ['6'] = 33, ['7'] = 34, ['8'] = 35,
    ['9'] = 36, ['_'] = 37
  };
  STRHASH_INT res; // Result.
  STRHASH_INT idx; // Character index.
  STRHASH_INT mul; // Base multiplier.
  STRHASH_INT add; // 
  uint8_t     chr; // Character.
  
  // Ensure maximal string length is observed.
  if (len > STRHASH_LEN) {
    return false;
  }

  // The result starts as zero (might end up being zero in case of string "a").
  res = 0;

  // Start at the first multiplier base: 37^0 = 1.
  mul = 1;

  // Traverse the string in reverse.
  idx = len;
  while (idx > 0) {

    // Skip any trailing NUL characters.
    if (str[idx - 1] == '\0') {
      idx -= 1;
      continue;
    }

    // Resolve the character index.
    chr = tab[(uint8_t)str[idx - 1]];

    // Verify that the character is a member of the accepted alphabet.
    if (chr == 0) {
      return false;
    }

    // The value to be added to the result.
    add = mul * (STRHASH_INT)chr;

    // Verify the potential overflow.
    if (res + add < res) {
      return false;
    }

    // Add the character value to the result.
    res += add;

    // Advance to the next base multiplier.
    mul *= 37;

    // Advance to the next character.
    idx -= 1;
  }

  *val = res;
  return true;
}

/// Expand an integer into its string representation using arithmetic coding.
bool strhash_get(
        char*        str, // Decompressed string.
  const STRHASH_INT  len, // Maximal length of the string.
  const STRHASH_INT  val) // Integer representation.
{
  // Translation table where the 
  const char tab[38] = {
    '?', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
    'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u',
    'v', 'w', 'x', 'y', 'z', '0', '1',
    '2', '3', '4', '5', '6', '7', '8',
    '9', '_'
  };
  STRHASH_INT idx; // String index.
  STRHASH_INT cpy; // Working copy of the integer representation.
  STRHASH_INT end;
  STRHASH_INT quo;
  char        tmp; // Temporary space for swapping.

  if (len > STRHASH_LEN) {
    return false;
  }

  // Start the string building at index zero.
  idx = 0;

  // Make a copy of the input value.
  cpy = val;

  while (cpy > 0) {
    // Determine the polynomial constant for this base.
    quo = cpy % 37;
    if (quo == 0) {
      quo = 37;
    }

    // Append the appropriate character to the string.
    str[idx] = tab[quo];
    idx += 1;

    if (idx > len) {
      return false;
    }

    // Advance to another base.
    cpy -= quo;
    cpy /= 37;
  }

  // Reverse the string.
  end = idx;
  idx = 0;
  while (idx < end / 2) {
    tmp                = str[idx];
    str[idx]           = str[end - 1 - idx];
    str[end - 1 - idx] = tmp;
    idx               += 1;
  }

  return true;
}
