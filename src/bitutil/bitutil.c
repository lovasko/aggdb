// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.
//
#if defined(__x86__) && defined(BITUTIL_ACC)
  #include <x86gprintrin.h>
#endif
  

#include "bitutil/bitutil.h"


#ifndef bitutil_ctz 
/// Count the number of trailing unset bits of a word.
BITUTIL_INT bitutil_ctz(
  const BITUTIL_INT inp) // Word.
{ 
  BITUTIL_INT idx; // Bit index.
  BITUTIL_INT cnt; // Number of zeros.

  // Iterate the bits, starting with the least significant. Terminate once a set bit is reached.
  idx = 0;
  cnt = 0;
  while (idx < BITUTIL_INT_BIT) {
    if (((inp >> idx) & 1) == 0) {
      cnt += 1;
    } else {
      break;
    }

    idx += 1;
  }

  return cnt;
}
#endif

#ifndef bitutil_clz
/// Count the number of leading unset bits of a word.
BITUTIL_INT bitutil_clz(
  const BITUTIL_INT inp) // Word.
{
  BITUTIL_INT idx; // Bit index.
  BITUTIL_INT cnt; // Number of zeros.

  // Iterate the bits, starting with the least significant. Terminate once a set bit is reached.
  idx = 0;
  cnt = 0;
  while (idx < BITUTIL_INT_BIT) {
    if (((inp << idx) & ((BITUTIL_INT)1 << (BITUTIL_INT_BIT - 1))) == 0) {
      cnt += 1;
    } else {
      break;
    }

    idx += 1;
  }

  return cnt;
}
#endif

#ifndef bitutil_pop
/// Compute the number of set bits in a word (population count).
BITUTIL_INT bitutil_pop(
  const BITUTIL_INT inp) // Word.
{
  BITUTIL_INT idx; // Bit index.
  BITUTIL_INT cnt; // Population count.

  cnt = 0;
  idx = 0;
  while (idx < BITUTIL_INT_BIT) {
    if (inp >> idx & 1) {
      cnt += 1;
    }

    idx += 1;
  }

  return cnt;
}
#endif

/// Compute the one-based index of the n-th set bit within a single word.
BITUTIL_INT bitutil_sel(
  const BITUTIL_INT inp, // Word.
  const BITUTIL_INT nth) // Number of the set bit.
{
  #if defined(__x86__) && defined(BITUTIL_ACC)
    #if   BITUTIL_INT_BIT == 32
      return _tzcnt_u32(_pdep_u32((uint32_t)1 << nth, inp));
    #elif BITUTIL_INT_BIT == 64
      return _tzcnt_u64(_pdep_u64((uint64_t)1 << nth, inp));
    #else
      #error "acceleration not supported"
    #endif
  #else
    BITUTIL_INT cnt; // Set bit count.
    BITUTIL_INT idx; // Bit index.
    BITUTIL_INT val; // Word.

    // Create a mutable copy of the input word.
    val = inp;

    // One by one scan the bits of the byte and count the set bits.
    cnt = 0;
    idx = 0;
    while (idx < BITUTIL_INT_BIT) {
      // Extract and process the first bit.
      cnt += val & 1;

      // Check for the intended condition.
      if (cnt == (nth + 1)) {
        return idx;
      }

      // Advance to the next bit.
      val >>= 1;
      idx  += 1;
    }

    // This should not be reached.
    return 0;
  #endif
}
