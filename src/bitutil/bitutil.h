// Copyright (c) 2022 Daniel Lovasko
// All Rights Reserved. Proprietary and confidential.

#include <stdint.h>


#ifndef BITUTIL_H
#define BITUTIL_H

// Ensure that the type sizes are selected by the user.
#ifndef BITUTIL_INT_BIT
  #error "must define BITUTIL_INT_BIT to set unsigned integer width"
#endif

#ifdef BITUTIL_INT
  #error "the BITUTIL_INT type must be set by this header"
#endif

// Define the appropriate types based on the selection.
#if   BITUTIL_INT_BIT == 16
  #define BITUTIL_INT uint16_t

#elif BITUTIL_INT_BIT == 32
  #define BITUTIL_INT uint32_t

#elif BITUTIL_INT_BIT == 64
  #define BITUTIL_INT uint64_t

#else
  #error "invalid value for BITUTIL_INT_BIT: '" BITUTIL_INT_BIT "' [16, 32, 64]"
#endif

// Attempt to select the appropriate builtin for the selected type to count trailing zeros.
#ifdef BITUTIL_ACC
  #if   BITUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_ctz)
    #define bitutil_ctz __builtin_ctz
  #elif BITUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_ctzl)
    #define bitutil_ctz __builtin_ctzl
  #elif BITUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_ctzll)
    #define bitutil_ctz __builtin_ctzll
  #endif
#endif

// Attempt to select the appropriate builtin for the selected type to count leading zeros.
#ifdef BITUTIL_ACC
  #if   BITUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_clz)
    #define bitutil_clz __builtin_clz
  #elif BITUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_clzl)
    #define bitutil_clz __builtin_clzl
  #elif BITUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_clzll)
    #define bitutil_clz __builtin_clzll
  #endif
#endif

// Attempt to select the appropriate builtin for the selected type to count set bits.
#ifdef BITUTIL_ACC
  #if   BITUTIL_INT_BIT == __INT_WIDTH__       && __has_builtin(__builtin_popcount)
    #define bitutil_bit_pop __builtin_popcount
  #elif BITUTIL_INT_BIT == __LONG_WIDTH__      && __has_builtin(__builtin_popcountl)
    #define bitutil_bit_pop __builtin_popcountl
  #elif BITUTIL_INT_BIT == __LONG_LONG_WIDTH__ && __has_builtin(__builtin_popcountll)
    #define bitutil_bit_pop __builtin_popcountll
  #endif
#endif

#ifndef bitutil_ctz
  /// Count the number of trailing unset bits of a word.
  BITUTIL_INT bitutil_ctz(
    const BITUTIL_INT inp);
#endif

#ifndef bitutil_clz
  /// Count the number of leading unset bits of a word.
  BITUTIL_INT bitutil_clz(
    const BITUTIL_INT inp);
#endif

#ifndef bitutil_pop
  /// Compute the number of set bits in a word (population count).
  BITUTIL_INT bitutil_pop(
    const BITUTIL_INT inp);
#endif

/// Compute the one-based index of the n-th set bit within a single word.
BITUTIL_INT bitutil_sel(
  const BITUTIL_INT inp,
  const BITUTIL_INT nth);

#endif
