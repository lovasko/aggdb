// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/un.h>

#include <netinet/in.h>

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include <errno.h>

#include <pthread.h>

#include "strhash/strhash.h"


/// Outgoing message to the client. (16 bytes)
struct cl_out {
  char     co_mtr[12];
  uint32_t co_val;
};

/// Incoming message from the client. (20 bytes)
struct cl_inp {
  uint16_t ci_ver;     // Version.
  uint8_t  ci_cmd;     // Command type.
  char     ci_mtr[12]; // Metric.
  uint32_t ci_arg;    // Argument.
};

/// Outgoing message to the database. (24 bytes)
struct db_out {
  uint8_t  do_cmd : 2;  // Command.
  uint64_t do_mtr : 60; // Metric.
  uint32_t do_arg;      // Command argument.
  uint32_t do_sec;      // UNIX Epoch time in seconds.
  uint64_t do_uid;      // Unique ID of the client (IPv4 address & port number).
};

/// Incoming message from the database. (20 bytes)
struct db_inp {
  uint64_t di_mtr; // Metric.
  uint64_t di_uid; // Unique ID of the client (IPv4 address & port number).
  uint32_t di_val; // Aggregate value.
};

/// Client thread context.
struct cl_ctx {
  _Atomic bool*     cc_end; // Termination flag.
  _Atomic uint32_t* cc_cli; // Information requests.
          int       cc_cls; // UDP socket.
          int       cc_dbs; // UNIX domain socket.
};

/// Database thread context.
struct db_ctx {
  _Atomic bool*     dc_end; // Termination flag.
  _Atomic uint32_t* dc_dbi; // Information requests.
          int       dc_dbs; // UNIX domain socket.
          int       dc_cls; // UDP socket.
};

/// Signal thread context.
struct sg_ctx {
  _Atomic bool*     sc_end; // Termination flag.
  _Atomic uint32_t* sc_cli; // Information requests.
  _Atomic uint32_t* sc_dbi; // Information requests.
};

/// Unique ID.
struct ip_uid {
  uint32_t iu_adr; // IPv4 address.
  uint32_t iu_prt; // UDP port.
};

/// Initialise a new UDP socket to communicate with the clients.
static bool cl_new(
  int* out) // Created socket.
{
  int                ret; // Return value.
  struct sockaddr_in adr; // Address.
  struct timeval     rto; // Receive timeout.

  // Create a new UDP socket.
  ret = socket(AF_INET, SOCK_DGRAM, 0);
  if (ret == -1) {
    printf("error: unable to create a socket\n");
    return false;
  }
  *out = ret;

  // Set a timeout on data receipt.
  rto.tv_sec  = 0;
  rto.tv_usec = 500000;
  ret = setsockopt(*out, SOL_SOCKET, SO_RCVTIMEO, &rto, sizeof(rto));
  if (ret == -1) {
    printf("error: unable to set receive timeout\n");
    return false;
  }

  // Set the local address.
  (void)memset(&adr, 0, sizeof(adr));
  adr.sin_family      = AF_INET;
  adr.sin_addr.s_addr = INADDR_ANY;
  adr.sin_port        = htons(10015);

  // Assign the local address to the socket.
  ret = bind(*out, (const struct sockaddr*)&adr, sizeof(adr));
  if (ret == -1) {
    printf("error: unable to bind the socket\n");
    return false;
  }

  return true;
}

/// Initialise a new UNIX domain socket to communicate with the database.
static bool db_new(
  int* out) // Created socket.
{
  int                ret; // Return value.
  struct sockaddr_un adr; // UNIX domain address.
  struct timeval     rto; // Receive timeout.

  // Create a new UNIX domain socket.
  ret = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if (ret == -1) {
    printf("error: unable to create a UNIX domain socket\n");
    return false;
  }
  *out = ret;

  // Set a timeout on data receipt.
  rto.tv_sec  = 0;
  rto.tv_usec = 500000;
  ret = setsockopt(*out, SOL_SOCKET, SO_RCVTIMEO, &rto, sizeof(rto));
  if (ret == -1) {
    printf("error: unable to set receive timeout\n");
    return false;
  }

  // Set the local address.
  adr.sun_family = AF_UNIX;
  snprintf(adr.sun_path, sizeof(adr.sun_path),
    "/tmp/aggdb_udp_%" PRIiMAX, (intmax_t)getpid());

  // Assign a local address to the socket.
  ret = bind(*out, (struct sockaddr*)&adr, sizeof(adr));
  if (ret == -1) {
    printf("error: unable to bind to local path\n");
    return false;
  }
  
  // Connect to the database socket.
  snprintf(adr.sun_path, sizeof(adr.sun_path), "/tmp/aggdb_srv");
  ret = connect(*out, (const struct sockaddr*)&adr, sizeof(adr)); 
  if (ret == -1) {
    printf("error: unable to connect to the database socket\n");
    return false;
  }
  
  return true;
}

#define DB_ERR_SND 0
#define DB_ERR_RCV 1

/// Receive data packets from the database and forward them to the respective clients.
static void* db_run(
  void* arg) // Thread arguments.
{
  bool               end;    // Termination decision.
  uint64_t           ctr;    // Message counter.
  struct sockaddr_in adr;    // Client address and port.
  struct db_ctx*     ctx;    // Thread context.
  struct db_inp      dbi;    // Input from database.
  struct cl_out      clo;    // Output to client.
  uint32_t           err[2]; // Error counters.
  uint32_t           inf;    // Information requests.
  int                ret;    // Return value.
  union {
    uint64_t         fst;
    struct ip_uid    snd;
  } uid;

  // Unpack the thread arguments.
  ctx = (struct db_ctx*)arg;

  // Reset the counters.
  ctr = 0;
  err[DB_ERR_SND] = 0;
  err[DB_ERR_RCV] = 0;
  
  // Start the main thread loop.
  while (true) {
    // Check whether an exit is in order.
    end = atomic_load(ctx->dc_end);
    if (end == true || err[DB_ERR_SND] + err[DB_ERR_RCV] > 100) {
      atomic_store(ctx->dc_end, true);
      break;
    }

    // Check whether information is required.
    inf = atomic_load(ctx->dc_dbi);
    if (inf > 0) {
      printf("db/ctr=%" PRIu64 " err[snd]=%" PRIu32 " err[rcv]=%" PRIu32 "\n",
        ctr, err[DB_ERR_SND], err[DB_ERR_RCV]);
      atomic_fetch_sub(ctx->dc_dbi, 1);
    }

    // Read a message from the database.
    ret = recvfrom(ctx->dc_dbs, &dbi, sizeof(dbi), 0, NULL, NULL);
    if (ret == -1) {
      if (errno != EAGAIN) {
        err[DB_ERR_RCV] += 1;
      }
      continue;
    }

    // Resolve the intended recipient.
    uid.fst = dbi.di_uid;
    memset(&adr, 0, sizeof(adr));
    adr.sin_family      = AF_INET;
    adr.sin_addr.s_addr = uid.snd.iu_adr;
    adr.sin_port        = uid.snd.iu_prt;

    // Copy data to the response.
    (void)strhash_get(clo.co_mtr, 12, dbi.di_mtr);
    clo.co_val = dbi.di_val;
    
    // Issue the response to the client.
    ret = sendto(ctx->dc_cls, &clo, sizeof(clo), 0, (struct sockaddr*)&adr, sizeof(adr));
    if (ret == -1) {
      if (errno != EAGAIN) {
        err[DB_ERR_SND] += 1;
      }
      continue;
    }

    // Record a successful message passing.
    ctr += 1;
  }

  return NULL;
}

#define CL_ERR_RCV 0
#define CL_ERR_SND 1
#define CL_ERR_FMT 2

/// Receive data packets from clients and forward them to the database.
static void* cl_run(
  void* arg) // Thread arguments.
{
  uint32_t           err[3]; // Number of errors.
  uint32_t           inf;    // Information request.
  bool               end;    // Retrieved termination decision.
  socklen_t          len;    // Length of the incoming address.
  struct sockaddr_in adr;    // Address of the incoming message.
  struct cl_ctx*     ctx;    // Thread context.
  struct timespec    now;    // Current time.
  uint64_t           ctr;    // Message counter.
  struct cl_inp      cli;    // Message incoming from the client.
  struct db_out      dbo;    // Message outgoing to the database.
  uint64_t           mtr;    // Metric.
  int                ret;    // Return value.
  union {                    // Unique ID.
    uint64_t         fst;    // Retrievable value.
    struct ip_uid    snd;    // Writable value.
  } uid;

  // Unpack the thread arguments
  ctx = (struct cl_ctx*)arg;

  // Reset the counters.
  ctr = 0;
  err[CL_ERR_RCV] = 0;
  err[CL_ERR_SND] = 0;
  err[CL_ERR_FMT] = 0;

  // Start the main thread loop.
  while (true) {
    // Check whether an exit is in order.
    end = atomic_load(ctx->cc_end);
    if (end == true || err[CL_ERR_RCV] + err[CL_ERR_SND] > 100) {
      atomic_store(ctx->cc_end, true);
      break;
    }

    // Check whether information is required.
    inf = atomic_load(ctx->cc_cli);
    if (inf > 0) {
      printf("cl/ctr=%" PRIu64 
             " err[snd]=%" PRIu32
             " err[rcv]=%" PRIu32
             " err[fmt]=%" PRIu32 "\n",
        ctr, err[CL_ERR_SND], err[CL_ERR_RCV], err[CL_ERR_FMT]);
      atomic_fetch_sub(ctx->cc_cli, 1);
    }

    // Read a single datagram from the client.
    ret = recvfrom(ctx->cc_cls, &cli, sizeof(cli), 0, (struct sockaddr*)&adr, &len);
    if (ret == -1) {
      if (errno != EAGAIN) {
        err[CL_ERR_RCV] += 1;
      }
      continue;
    }

    // Verify that the magic string, version and command are valid.
    if (cli.ci_ver != 0xADB1 || cli.ci_cmd > 4) {
      err[CL_ERR_FMT] += 1;
      continue;
    }

    // Unpack the metric name.
    ret = strhash_put(&mtr, cli.ci_mtr, 12);
    if (ret == (int)false) {
      err[CL_ERR_FMT] += 1;
      continue;
    }
    dbo.do_mtr = mtr;

    // Repack the values for the database.
    dbo.do_cmd = cli.ci_cmd;
    dbo.do_arg = cli.ci_arg;
    
    // Add the IPv4 address and port of the sender to the message.
    uid.snd.iu_adr = adr.sin_addr.s_addr;
    uid.snd.iu_prt = adr.sin_port;
    dbo.do_uid     = uid.fst;
    
    // Add a timestamp of the message receipt.
    (void)clock_gettime(CLOCK_REALTIME_COARSE, &now);
    dbo.do_sec = (uint32_t)now.tv_sec;
    
    // Write the message for the database.
    ret = sendto(ctx->cc_dbs, &dbo, sizeof(dbo), 0, NULL, 0);
    if (ret == -1) {
      err[CL_ERR_SND] += 1;
      continue;
    }

    // Record a successful message passing.
    ctr += 1;
  }

  return NULL;
}

/// Signal thread. An endless loop of handling incoming signals.
static void* sg_run(
  void* arg) // Thread arguments.
{
  sigset_t        set; // Set of signals to receive.
  struct sg_ctx*  ctx; // Thread context.
  int             ret; // Return value.
  bool            end; // Termination decision.
  uint32_t        err; // Error counter.
  struct timespec wto; // Wait timeout.

  // Unpack the thread arguments.
  ctx = (struct sg_ctx*)arg;

  // Create a signal mask and allow its delivery.
  sigfillset(&set);
  sigdelset(&set, SIGTERM);
  sigdelset(&set, SIGINT);
  sigdelset(&set, SIGUSR1);
  ret = pthread_sigmask(SIG_SETMASK, &set, NULL);
  if (ret != 0) {
    printf("error: unable to block signal delivery\n");
    atomic_store(ctx->sc_end, true);
    return NULL;
  }

  // Set a 0.5 second timeout for the wait operation.
  wto.tv_sec  = 0;
  wto.tv_nsec = 5000000000;

  // Create a negation of the blocking set.
  sigemptyset(&set);
  sigaddset(&set, SIGTERM);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR1);

  // Start the main thread loop.
  err = 0;
  while (true) {
    // Check whether an exit is in order.
    end = atomic_load(ctx->sc_end);
    if (end == true || err > 100) {
      atomic_store(ctx->sc_end, true);
      break;
    }

    // Retrieve a signal signal from the queue.
    ret = sigtimedwait(&set, NULL, &wto);
    if (ret == -1) {
      if (errno != EAGAIN) {
        err += 1;
      }
      continue;
    }

    // Handle the termination by setting the `end` flag for all threads.
    if (ret == SIGTERM || ret == SIGINT) {
      atomic_store(ctx->sc_end, true);
      break;
    }

    // Force threads to report their statistics to the standard output stream.
    if (ret == SIGUSR1) {
      atomic_fetch_add(ctx->sc_dbi, 1);
      atomic_fetch_add(ctx->sc_cli, 1);
      printf("sg/err=%" PRIu32 "\n", err);
    }
  }

  return NULL;
}

// Environment variables:
//   AGGDB_UDP_PORT
//   AGGDB_SRV_SOCK

int main(void)
{
  _Atomic bool          end; // Program termination flag.
  _Atomic uint32_t      dbi; // Database thread information flag.
  _Atomic uint32_t      cli; // Client thread information flag.
          struct cl_ctx clc; // Client thread arguments.
          struct db_ctx dbc; // Database thread arguments.
          struct sg_ctx sgc; // Signal thread arguments.
          pthread_t     clt; // Client thread handle.
          pthread_t     dbt; // Database thread handle.
          pthread_t     sgt; // Signal thread handle.
          int           cls; // Client UDP socket.
          int           dbs; // Database UNIX domain socket.
          int           ret; // Return value.
          sigset_t      set; // Signal set.

  // Block receipt of all signals. All other spawned threads will inherit this mask.
  sigfillset(&set);
  ret = pthread_sigmask(SIG_SETMASK, &set, NULL);
  if (ret != 0) {
    printf("error: unable to block all signals\n");
    return EXIT_FAILURE;
  }

  // Create the client UDP socket.
  ret = (int)cl_new(&cls);
  if (ret == (int)false) {
    return EXIT_FAILURE;
  }
  
  // Create the database UNIX domain socket.
  ret = (int)db_new(&dbs);
  if (ret == (int)false) {
    return EXIT_FAILURE;
  } 

  // Start the signal-handling thread.
  sgc.sc_end = &end;
  sgc.sc_dbi = &dbi;
  sgc.sc_cli = &cli;
  ret = pthread_create(&sgt, NULL, sg_run, &sgc);
  if (ret != 0) {
    printf("error: unable to start the signal thread\n");
    return EXIT_FAILURE;
  }

  // Start the client thread.
  clc.cc_end = &end;
  clc.cc_cli = &cli;
  clc.cc_cls = cls;
  clc.cc_dbs = dbs;
  ret = pthread_create(&clt, NULL, cl_run, &clc);
  if (ret != 0) {
    printf("error: unable to start the client thread\n");
    return EXIT_FAILURE;
  }
  
  // Start the database thread.
  dbc.dc_end = &end;
  dbc.dc_dbi = &dbi;
  dbc.dc_dbs = dbs;
  dbc.dc_cls = cls;
  ret = pthread_create(&dbt, NULL, db_run, &dbc);
  if (ret != 0) {
    printf("error: unable to start the database thread\n");
    return EXIT_FAILURE;
  }
  
  // Wait for the database thread to finish.
  ret = pthread_join(dbt, NULL);
  if (ret != 0) {
    printf("error: unable to join the database thread\n");
    return EXIT_FAILURE;
  }

  // Wait for the client thread to finish.
  ret = pthread_join(clt, NULL);
  if (ret != 0) {
    printf("error: unable to join the client thread\n");
    return EXIT_FAILURE;
  }

  // Wait for the signal thread to finish.
  ret = pthread_join(sgt, NULL);
  if (ret != 0) {
    printf("error: unable to join the signal thread\n");
    return EXIT_FAILURE;
  }

  // Terminate the database connection.
  (void)close(dbs);
  
  return EXIT_SUCCESS;
}
