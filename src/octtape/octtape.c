// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#include <stdlib.h>

#include "octtape/octtape.h"


/// Initialise the octet tape.
bool octtape_new(
        struct octtape* oct, // Octet tape.
  const OCTTAPE_INT     max) // Maximal number of octets.
{
  // Allocate underlying block of memory.
  oct->ot_oct = calloc(max, 1);
  if (oct->ot_oct == NULL) {
    return false;
  }

  // Set the state variables.
  oct->ot_max = max;
  oct->ot_len = 0;

  return true;
}

/// Read octets from the tape at the reading head position.
bool octtape_get(
  const struct octtape *const restrict oct, // Octet tape.
        struct octhead *const restrict pos, // Reading head position.
  const OCTTAPE_INT                    cnt, // Number of octets to read.
        uint8_t*                       out) // Output octets.
{
  // Ensure that the tape has sufficient data for the requested octets.
  if (pos->oh_get + cnt > oct->ot_len) {
    return false;
  }

  // Copy the data from the tape to the output destination.
  (void)memcpy(out, &oct->ot_oct[pos->oh_get], cnt);
  
  // Advance the reading head.
  pos->oh_get += cnt;

  return true;
}

/// Write octets to the tape at the writing head position.
bool octtape_put(
        struct octtape *const restrict oct, // Octet tape.
        struct octhead *const restrict pos, // Writing head position.
  const OCTTAPE_INT                    cnt, // Number of octet to write.
  const uint8_t*                       inp) // Input octets.
{
  // Ensure that the tape has sufficient space for the added octets.
  if (pos->oh_put + cnt > oct->ot_max) {
    return false;
  }

  // Copy the data from the input source to the tape.
  (void)memcpy(&oct->ot_oct[pos->oh_put], inp, cnt);
  
  // Update the watermark of the written data.
  if (pos->oh_put + cnt > oct->ot_len) {
    oct->ot_len = pos->oh_put + cnt; 
  }

  // Advance the writing head.
  pos->oh_put += cnt;
  
  return true;
}

/// Free resources held by the bit tape.
void octtape_del(
  struct octtape* oct) // Octet tape.
{
  free(oct->ot_oct);
}
