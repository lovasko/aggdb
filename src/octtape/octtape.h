// Copyright (c) 2022 Daniel Lovasko
// All rights reserved. Proprietary and confidential.

#ifndef OCTTAPE_H
#define OCTTAPE_H

#include <stdint.h>
#include <stdbool.h>


// Ensure that an integer width is selected.
#ifndef OCTTAPE_INT_BIT
  #error "must define OCTTAPE_INT_BIT to select integer width"
#endif

// Select the appropriate word type.
#if   OCTTAPE_INT_BIT == 16
  #define OCTTAPE_INT     uint16_t
  #define OCTTAPE_INT_MAX UINT16_MAX

#elif OCTTAPE_INT_BIT == 32
  #define OCTTAPE_INT     uint32_t
  #define OCTTAPE_INT_MAX UINT32_MAX

#elif OCTTAPE_INT_BIT == 64
  #define OCTTAPE_INT     uint64_t
  #define OCTTAPE_INT_MAX UINT64_MAX

#else
  #define _str(x) #x
  #define str(x) _str(x)
  #error "OCTTAPE_INT_BIT has unsupported value: " str(OCTTAPE_INT_BIT) " [16, 32, 64]"
#endif

// Octet tape.
struct octtape {
  uint8_t*    ot_oct; // Backing storage.
  OCTTAPE_INT ot_len; // Current number of 
  OCTTAPE_INT ot_max; // Maximal number 
};

// Octet tape head.
struct octhead {
  OCTTAPE_INT ot_get; // Reading head position.
  OCTTAPE_INT ot_put; // Writing head position.
};

bool octtape_new(struct octtape* oct, const OCTTAPE_INT max);
bool octtape_get(const struct octtape* oct, struct octhead* pos, uint8_t* out);
bool octtape_put(struct octtape* oct, struct octhead* pos, uint8_t* inp);
void octtape_del(struct octtape* oct);

#endif
